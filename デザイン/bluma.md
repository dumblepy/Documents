Bluma
===
[戻る](../README.md)

[公式ドキュメント](https://bulma.io/documentation/)

## インストール
```
<!-- Bluma 本体-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
<!-- アイコン用 -->
<script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
```

## 基本形
```
<body>
  <section class="section">
    // 内容
  </section>
</body>
```
