Supabase
オープンソースの Firebase

Deno
JavaScript と TypeScript のためのモダンなランタイム

GitLab
オープンソースの GitHub

Traefik
クラウドネイティブソリューション
会社サイト: https://traefik.io/

オープンソース版の Traefik にはない、複数ノードの高可用性、セキュリティ強化、プライオリティサポートなどの機能を提供している

Ruff
Python 静的型チェッカー

調達ニュース: https://astral.sh/blog/announcing-astral-the-company-behind-ruff

Prisma
TypeScript の OMR
会社サイト: https://www.prisma.io/about

調達ニュース
$4.5M https://www.prisma.io/blog/prisma-raises-4-5m-to-build-the-graphql-data-layer-for-all-databases-663484df0f60
$12M https://www.prisma.io/blog/prisma-raises-series-a-saks1zr7kip6
$40M https://www.prisma.io/blog/series-b-announcement-v8t12ksi6x

ソースコードを公開したソフトウェアで収益を得ている会社
https://zenn.dev/username/articles/2021-11-29-3fe9ccd53b5395d5ad5b
