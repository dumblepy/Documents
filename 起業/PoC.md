PoC
===
[戻る](../README.md)


- 冒険型経営
  - 予測不可能を前提に考える
  - シェークスピアは5幕から成る
  - 起承転結、PDCAは4つの構成だけど、5展開する流れがあるのではないか
  - 上場企業がやる、年度予算を月次に分けて計画を作るのは組織統制経営であり、スタートアップはそれをするべきではない

- 事業立ち上げ6つの壁テスト
  - 顧客候補
  - 切実な課題
  - 解決仮説
  - 商品使用→価格
  - 初期顧客テスト
    - 自ら50件くらいやれ
    - ファンの詳細を記述
  - 市場拡販
    - ヒット兆候は？
    - 販促どうする？
    - 市場規模はある？
    - 競合は？
  - 資源調達
  - 供給体制
  - 投資回収


```
ペルソナ（Notion）
↓
カスタマージャーニー
↓
スプリント
↓
リーンキャンバス（Notion）
↓
ジャベリンボード（Notion）
↓
ユーザーストーリーマップ（Miro）
↓
MPV定義（Miro）
↓
プロダクトバックログ（Miro）
↓
ワイヤーフレーム（Whmsical）
```

## リーンキャンバス
個人的には  
「課題」「解決策」「顧客」  
の並びを「それってどんな道具？」がテキスト形式の問いとしてカバーしてる時点で問題のほとんどは解決してて  
「圧倒的優位性」  
ってのが「おもちゃが道具になっていく過程における北極星」なので、これはこれで別の段落にテキストで書けばいいな、と  
さらにいうと個人的には「ファイナンシング市況とファイナンシングのコネ」がリーンキャンバスにはないし、「めっちゃ金払いのいい太客へ『おもちゃ』を届ける最短ルート」についての言及も足りてないなーと  
つまりキャンバス型である必然性が弱い  
https://twitter.com/shogochiai/status/1412271144980283394

### 課題

### ソリューション

### 独自の価値提案

### 圧倒的な優位性

### 顧客セグメント

### 主要指標
- アクイジション（新規）
- アクティベーション（活性化）
- リテンション（定着）
- リファラル（口コミ）
- チャネル（顧客流入の接点）
![](https://ferret.akamaized.net/images/5b28e11bfafbd80624000001/original.png?1529405722)

### コスト構造

### 収益の流れ
誰から（なぜそのソリューションに払ってくれるのか）、どんな形式で、いくら貰うのか（なぜその金額に払ってくれるのか）


## ペルソナ
|名前|年齢|出身|学歴|ITリテラシー|思考のクセ|情報の得方|どんな人|
|---|---|---|---|---|---|---|---|

## ジャベリンボード

## ユーザーストーリーマップ
横軸：ユーザーの体験  
縦軸：機能の深堀り

スライス：リリースの単位。機能毎に切る。

![](https://camo.qiitausercontent.com/bf00af24103c27993056b1600b1cda3d700e13ee/68747470733a2f2f71696974612d696d6167652d73746f72652e73332e616d617a6f6e6177732e636f6d2f302f31323739302f34656463336636392d333864392d373335612d313632312d6332326539626233316534302e706e67)


## インタビュー
### 接点作り
自分の知ってる人の中からその状況に一番近い人に最初に聞く  
その人から同じような課題を抱えている人を紹介してもらって芋づる式に広げていく

### ユーザーの行動事実に注目する
質問攻めにするのではない。  
課題は何ですか？ではなく、〜する時にどうしていますか？と、実際にユーザーがどういう行動をしているかを聞く。周辺情報を掘りまくる。

事象の有無→頻度→一番最近のエピソード

### 既存の代替手段を聞いてみる
今その課題について行動を起こしているか？  

行動を起こすほど解決したい課題があり、代替手段でどれだけ負担が掛かってるかを判断する

- 最初の挨拶
  - 本音で自分のことや、周辺環境のことを話してもらうために安心してもらう
- 基本情報
  - 仮設の顧客ターゲットなのかを判断するための情報を聞く
- 深堀り
  - 課題の周辺の質問から、本人の口から課題が出てくるかを確認
- サービス説明
  - 独自の価値、ソリューションがお金を払ってまで使いたいと思えるか確認
- 終わりの挨拶
  - 更に協力してもらう、サービス開始時に顧客になってもらえるように

[ユーザーインタビューですべき「5つの質問」](https://www.qualtrics.com/m/assets/jp/wp-content/uploads/2021/07/customer-journey-mapping.pdf)

## MVPの決め方
![](https://cdn-ak.f.st-hatena.com/images/fotolife/i/i2key/20161204/20161204224435.png)

![](https://ssaits.jp/promapedia/wp-content/uploads/2020/05/moscow.png)

## PdM
[プロダクトづくりに関わる全員が知っておきたい「豊かな仮説」の立て方とは？](https://codezine.jp/article/detail/14065)

[エンジニアに伝えたい！プロダクトマネージャーの頭の中 - プロダクトをもう一段階成長させる仮説の立て方](https://note.com/tably/n/na148a5bcdeab)

[プロダクトマネジメントクライテリア - プロダクトをつくるチームのチェックリスト](https://productmanagement-criteria.com)

[プロダクトマネージャーという仕事](https://note.com/akihirohara/n/n6e995f645e49)

## PMF
```
PMFしたと判断できるKPI

-  アクティブユーザー率25%以上
-  バイラル率0.5以上
-  Dau/Mau率50%以上
-  コホートカーブが水平に
-  60%以上がオーガニック獲得
-  少数だが強烈にハマるユーザー層の出現
-  Day1, 7, 30の利用回数が60回、30回、15回

↑　どれか一つでもあれば◎

(by Andrew Chen)
```
https://twitter.com/kubotamas/status/1366765876582649856

[PMF(プロダクトマーケットフィット)の大切さと実例、はかり方、見つけ方: 基礎編](https://note.com/kenichiro_hara/n/nde3bf0c242b1)

[PMFのはかり方、見つけ方: やや実践編](https://note.com/kenichiro_hara/n/nec3b6d791039)

[SaaSプライシングの教科書【決定版】](https://note.com/bppinc_pricing/n/n3256cbdcabbc)
