# 組織

[戻る](../README.md)

# IT 業界全般

[IT 業界来て辛かったこと](https://twitter.com/lawliteqed/status/1345691775071965185)

[プログラマの生き様](https://np-complete.gitbook.io/c89-the-way-of-programmer-life/)

[デスマーチが起きる理由 - ３つの指標](https://gist.github.com/voluntas/9c1d9d51e86a853fed6889f743a12145)

[Twitter で医師を拾ってきて Google のソフトウェアエンジニアにするだけの簡単なお仕事](https://nuc.hatenadiary.org/entry/2021/03/31)

# プロジェクト

## [スケールする組織を支えるドキュメンテーションの技術を”GitLab Handbook”から学ぶ](https://note.com/takahiroanno/n/n62b962e021d6)

- ドキュメント文化は健全な組織のスケールのために必要
  - 組織に情報が流通し、透明性を確保できるようになる。
  - 情報を流通させるためにいちいち口頭の説明がいらないから、メンバーの数が増えた時でもスケールしやすくなる。
  - 過去の結論にアクセス可能になるので、議論を積み上げていき、意思決定のクオリティを高めることにもつながる。

しかし  
**少数のメンテナの気力によってのみ駆動されているため、次第に情報が古くなってゆき、形骸化してしまうことが多い**

### Wiki がダメな理由

- ページの複数の部分や複数のページにまたがるような提案をすることができない
- 作業者とレビュワーの役割を分けることができない
- 提案を取り込むべきか取りこまないべきかの議論もできない
- そのため、ハンドブックはリファクタリング不能な状態に陥る

→ 　 Git リポジトリにする。

[コードレビューの目的と考え方](https://osak.hatenablog.jp/entry/code-review-objectives-and-howto)

# 設計

[MVC とはなにか](https://note.com/tenjuu99/n/n0232ccd1089d)

[プログラマが知るべき 97 のこと](https://ja.wikisource.org/wiki/%E3%83%97%E3%83%AD%E3%82%B0%E3%83%A9%E3%83%9E%E3%81%8C%E7%9F%A5%E3%82%8B%E3%81%B9%E3%81%8D97%E3%81%AE%E3%81%93%E3%81%A8)

[ビジネスロジックオブジェクトを中心にアプリケーションを組み立てる](https://twitter.com/masuda220/status/1321700797042716672)

[オブジェクト指向とは何か](https://jp.quora.com/%E3%82%AA%E3%83%96%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E6%8C%87%E5%90%91%E3%81%A8%E3%81%AF%E4%BD%95%E3%81%A7%E3%81%99%E3%81%8B)

[アジャイル開発で向上するのは「投資対収益率」](https://twitter.com/little_hand_s/status/1327741803588587520)

[良いコードとは何か - エンジニア新卒研修 ](https://note.com/cyberz_cto/n/n26f535d6c575)

# 個人の成長

[いまのあなたはどのレベル？ ビジネスパーソンの「成長の 9 段階」の登り方](https://studyhacker.net/grow-9-step)

![](https://cdn-ak.f.st-hatena.com/images/fotolife/s/sh_manami_yamazaki/20201002/20201002101214.jpg)

![](https://www.shijo24.com/images/ラーニングピラミッドshijo24.com.png)

# エンジニア組織

[日本 CTO 協会「2 つの DX」とデジタル時代の経営ガイドライン](https://cto-a.github.io/dxcriteria/)

[日本 CTO 協会 DX Criteria](https://www.notion.so/DX-Criteria-v202104-aef4f94969504f3f82ab94c06291245a)

[【図解】改善と変革を推進するならどれから手をつけるか 〜 個人・チーム・組織](https://www.servantworks.co.jp/2020/insight-of-individual-team-organization/)

[CTO から見た，なぜスタートアップ初期のソフトウェア設計は壊れがちなのか](https://speakerdeck.com/memory1994/why-the-application-design-is-breaking-sometimes-at-a-startup-company)

[組織規模と CTO の求められる役割の変化に関する雑記](https://note.com/y_matsuwitter/n/n9825615c53bc)

[なぜ PrAha Inc.のエンジニアは設計を学ぶのか](https://praha-inc.hatenablog.com/entry/2020/08/22/184729)

[「DMM をつくり変える」　 29 歳 CTO のエンジニア魂　: NIKKEI STYLE](https://style.nikkei.com/article/DGXMZO41448310Z10C19A2000000?channel=DF180320167086)

[IT 事業は「サービス」と「ソフトウェア」に分類でき、その分類によって DDD を適用すべきかが決まるのでは、という考察](https://zenn.dev/meijin/articles/5cb73354486ec0eb54b3)

[スタートアップで CTO が PdM/UI デザイナー/エンジニアを兼任してわかったこと](https://note.com/suzu_prog/n/n8c034e5d46ec)

[技術的負債とステークホルダと説明責任と](https://speakerdeck.com/toricls/the-debt)

[新米スクラムマスターにお勧めの本](https://yattom.hatenablog.com/entries/2017/04/09)

[スクラムガイド](https://scrumguides.org/docs/scrumguide/v2020/2020-Scrum-Guide-Japanese.pdf)

[シリコンバレーの「何が」凄いのか](https://www.slideshare.net/atsnakada/ss-80520040)

[マネージャーが中間管理職からリーダーになるための守破離](https://note.com/ktknd/n/n95285d6b15d0)

[Google re:work マネジメント](https://rework.withgoogle.com/jp/subjects/managers/)

[レイトステージスタートアップに CTO として 参画しての 10 ヶ月](https://note.com/sotarok/n/n2e8a8840d866)

[LayerX の QA への取り組み〜アイスクリームの誘惑に負けるな〜](https://tech.layerx.co.jp/entry/how-to-qa-in-early-stage)

[開発爆速化を支える経営会議や週次定例の方法論 〜LayerX の透明性への取り組みについて〜](https://tech.layerx.co.jp/entry/2021/05/06/101058)

[Four Keys 〜自分たちの開発レベルを定量化してイケてる DevOps チームになろう〜](https://engineer.recruit-lifestyle.co.jp/techblog/2021-03-31-four-keys/)

[スクエアエニックスゲーム開発プロジェクトマネジメント講座](http://www.jp.square-enix.com/tech/openconference/library/2011/dldata/PM/PM.pdf)

[アーキテクチャ変更を決断するとき CTO は何を考えているか？ 技術的意思決定を経営陣に説明する実践的 Tips](https://tech.fastdoctor.jp/entry/2023/04/17/163239)

[アジャイル開発は本当に必要なのか、何を解決するのか](https://speakerdeck.com/yuukiyo/is-agile-development-really-necessary-and-what-does-it-solve)

# スタートアップ

[読書メモ 『1 兆ドルコーチ』](https://qiita.com/sekiyaeiji/items/6c78408b9cbf24f5adb5)

[リサーチする際の主要リソース](https://twitter.com/juju_gaishi/status/1346059653075980289)

[MVP の作り方 🔨 とにかく雑に作る「手作業型 MVP」のススメ](https://speakerdeck.com/tumada/mvp-falsezuo-rifang-tonikakuza-nizuo-ru-shou-zuo-ye-xing-mvp-falsesusume)

[PMF に到るまでのステージ別指針集](https://speakerdeck.com/tumada/pmf-nidao-rumadefalsesutezibie-zhi-zhen-ji)

[解像度を高める by 馬田さん](https://review.foundx.jp/entry/high-resolution-slide)

[起業の羅針盤 ～起業家が事業フェーズ毎に直面する課題と解決策～〈→0〉起業前夜編](https://kigyolog.com/article.php?id=332)

[「プロデューサーがしてること」について](https://note.com/aokiu/n/n838fdc4e0528)

[新規事業立ち上げのアンチパターン](https://comemo.nikkei.com/n/n17b2fa051e52)

# データドリブン

[事業を成長させるデータ基盤を作るには](https://speakerdeck.com/yuzutas0/20200715)

[ZOZOTOWN の事業を支える BigQuery の話](https://speakerdeck.com/shiozaki/bigquery-behind-zozotown)

[データマネジメントなき経営は、破綻する。](https://speakerdeck.com/yuzutas0/20200419)

[あと 10 年で“営業職”が不要になるって本当？『営業はいらない』三戸政和氏 ×『無敗営業』高橋浩一氏](https://logmi.jp/business/articles/323515)
