[Cloud Buildガイド](https://cloud.google.com/build/docs/configuring-builds/create-basic-configuration?hl=ja)
[cloudbuild.yamlで使える環境変数一覧](https://cloud.google.com/build/docs/configuring-builds/substitute-variable-values)

## Compute Engineにコンテナをデプロイする

```yaml
steps:
  # コンテナイメージをビルド
  - name: 'gcr.io/cloud-builders/docker'
    args:
      [
        'build',
        '-t',
        '${_GCR_HOSTNAME}/$PROJECT_ID/${_SERVICE_NAME}:${SHORT_SHA}',
        '--file',
        './docker/app/Dockerfile.production',
        '.',
      ]

  # コンテナイメージをコンテナレジストリにpush
  - name: 'gcr.io/cloud-builders/docker'
    args: ['push', '${_GCR_HOSTNAME}/$PROJECT_ID/${_SERVICE_NAME}:${SHORT_SHA}']

  # Conpute Engineを削除
  - name: 'gcr.io/google.com/cloudsdktool/cloud-sdk'
    entrypoint: 'gcloud'
    args: [
      'compute',
      'instances',
      'delete',
      '${_SERVICE_NAME}',
      '--zone=${_DEPLOY_REGION}',
      '-q'
    ]

    # 初回のみCompute Engineを作る
    # https://cloud.google.com/sdk/gcloud/reference/compute/instances/create-with-container
    # ポートの公開
    # https://cloud.google.com/compute/docs/containers/configuring-options-to-run-containers?hl=ja#publishing_container_ports
    - name: 'gcr.io/google.com/cloudsdktool/cloud-sdk'
      entrypoint: 'gcloud'
      args:
        [
          'compute',
          'instances',
          'create-with-container',
          '${_SERVICE_NAME}',
          '--zone=${_DEPLOY_REGION}',
          '--container-image=${_GCR_HOSTNAME}/$PROJECT_ID/${_SERVICE_NAME}:${SHORT_SHA}',
          '--machine-type=e2-micro',
          '--network-tier=STANDARD', # 予約した静的IPのタイプに合わせる
          '--address=${_IP_ADDRESS}', # 予約した静的IP
          '--tags=allow-proxy-8080', # ポートの公開を参照
        ]

  # コンテナをアップデート
  # https://cloud.google.com/sdk/gcloud/reference/compute/instances/update-container
  - name: 'gcr.io/google.com/cloudsdktool/cloud-sdk'
    entrypoint: 'gcloud'
    args:
      [
        'compute',
        'instances',
        'update-container',
        '${_SERVICE_NAME}',
        '--container-image=${_GCR_HOSTNAME}/$PROJECT_ID/${_SERVICE_NAME}:${SHORT_SHA}',
        '--zone=${_DEPLOY_REGION}',
      ]

timeout: 3600s
substitutions:
  _DEPLOY_REGION: asia-northeast1-b
  _GCR_HOSTNAME: asia.gcr.io
  _SERVICE_NAME: _SERVICE_NAME
  _IP_ADDRESS: _IP_ADDRESS
images:
  - '${_GCR_HOSTNAME}/$PROJECT_ID/${_SERVICE_NAME}:${SHORT_SHA}'
options:
  logging: CLOUD_LOGGING_ONLY
```

- _SERVICE_NAMEと_IP_ADDRESSはCloudBuildの環境変数から設定する
