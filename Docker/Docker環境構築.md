Docker環境構築
===
[戻る](../README.md)

## Dockerのインストール
### Windows
Docker for Windows インストール（ローカルマシンがMacの場合for Mac）  
https://docs.docker.com/docker-for-windows/install/  

Stable channel を選び、ダウンロードし実行。  

セキュリティソフトで445ポート許可  
DockerがCドライブの接続に使うポートらしいので開けておく。  

### Linux
https://docs.docker.com/engine/install/ubuntu/
```
sudo apt remove docker docker-engine docker.io containerd runc
sudo apt update
sudo apt install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu jammy stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# Cannot connect to the Docker daemon の対応
sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
sudo service docker restart
```

[[対処法 WSL2] Docker エラー：Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running](https://qiita.com/Yutto_924/items/b52efb6da9e9f5905a51)


## dockerをsudo無しで実行できるようにする
[Ubuntu 16.04 で root ユーザー以外でも docker コマンドを使えるようにする](https://qiita.com/1000k/items/03a17c49471de881d5c0)
```
# docker グループにユーザーを追加
sudo gpasswd -a $(whoami) docker

# docker.sock にグループ書き込み権限を付与
sudo chgrp docker /var/run/docker.sock

# Docker daemon を再起動します。
sudo service docker restart

#再ログイン
su [ユーザー名]

#確認
docker info
```
その後OS再起動しないと、suコマンドを入力しないとdockerが使えないため、再起動をする


## dockerでmariadbを構築
[DockerでさくっとPostgreSQL/MySQL/MariaDBを触る方法](https://qiita.com/ryysud/items/7414162f012ea29b4e1a)  
[Docker公式イメージのMySQLで文字コードを指定する](https://qiita.com/neko-neko/items/de8ea13bbad32140de87)
```
sudo apt install -y mariadb-client
docker pull mariadb
docker run --name mariadb -e MYSQL_ROOT_PASSWORD=Password! -p 3306:3306 -d mariadb:latest --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
mysql -h 127.0.0.1 -uroot -pPassword!
```

---
---
ローカルの作業ディレクトリにGitクローン  


※コンテナ生成中にDockerのウインドウが表示されると思いますが、「Share it」を選んでください。  
※コンテナ生成とnpm installに時間がかかります。  

docker-compose実行
```
$ cd laradock
$ docker-compose up -d --build nginx mysql redis beanstalkd phpmyadmin php-fpm workspace
```

コンテナの再起動
```
$ docker-compose restart nginx mysql redis beanstalkd phpmyadmin php-fpm workspace

もしくは

//コンテナの停止
$ docker-compose stop
↓
$ docker-compose start nginx mysql redis beanstalkd phpmyadmin php-fpm workspace
```

コンテナにログイン
```
docker-compose exec workspace bash
```

npmとcomposerをインストール
```
npm install
composer install
```

exitコマンドでDocker内から抜ける
```
exit
```

コンテナを作り直したい時
```
//コンテナを停止
$ docker-compose stop

//コンテナを削除
$ docker-compose down --rmi all

//stopまたはkillできないプロセスの削除
$ docker ps
↑コンテナIDが表示される
$ docker rm --force <コンテナID>

//コンテナを再ビルド
$ docker-compose up -d --build nginx mysql redis beanstalkd phpmyadmin php-fpm workspace
```

コンテナ再起動
```
docker-compose restart
```
