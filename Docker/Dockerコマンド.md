Dockerコマンド
===
[戻る](../README.md)

## イメージをダウンロード
```
docker pull [イメージ名]
docker pull ubuntu
```

## イメージを元にコンテナを立てる
ubuntuのイメージをdockerのrepoから取得し、コンテナとして起動したあとに/bin/bashに接続することを意味する
```
docker run -it ubuntu /bin/bash
```

### Ubuntu
```
docker pull ubuntu
docker run -it ubuntu /bin/bash
docker run --name ubuntu-masonite ubuntu /bin/bash -it -v ../masonite-blog
```

### Python
```
docker pull python:3.6
docker run -it --name pytest python:3.6 /bin/bash

#再起動
docker exec -it pytest /bin/bash
```

### Mariadb
```
sudo apt install -y mariadb-client
docker pull mariadb
docker run --name mariadb -e MYSQL_ROOT_PASSWORD=Password! -p 3306:3306 -d mariadb:latest --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
mysql -h 127.0.0.1 -uroot -pPassword!
```

### postgresql
```
docker run --name pgsql -e POSTGRES_USER=root -e POSTGRES_PASSWORD=Password! -p 5432:5432 -d postgres:latest
psql -h localhost -U root
```

## Dockerfileを元にコンテナを作る
```
docker build . -t {name}
docker run -it -p:9000:9000 {name}
```

## 出力を省略しない
`--progress=plain` を付ける
```sh
docker build . -t {name} --progress=plain
```

## コンテナから抜ける
```
exit
```

## コンテナの確認
```
docker ps
docker ps -a
```

## コンテナの削除
```
#削除したいコンテナのidを確認
docker ps

docker rm 8bc339798fb7  
```

## 止まっているコンテナを全て削除
```
docker rm `docker ps -a -q`
```

## イメージの確認
```
docker images
```

## イメージの削除
```
#削除したいイメージのidを確認
docker images

docker rmi 3a00bcd77179
```

## イメージを全削除
```
docker rmi `docker images`
```

docker-compose コマンド
===

## ビルド
```
git clone
docker-compose build
```
## 起動
```
docker-compose up

#バックグラウンドで
docker-compose up -d
```

## コンテナにログイン
```
docker compose exec app bash
```

## CloudRunをローカルで動かす
makefile
```makefile
region=asia-northeast1
project={project}
repogitory={repository}
image={image}

container-stg=`docker ps -a | grep "${image}" | awk '{print $$1}'`

stg-build:
	docker rm -f $(container-stg) || :
	docker build --tag=${image} --file="./docker/stg/Dockerfile" .
	docker run \
		--detach \
		--name="${image}" \
		--publish=8080:8080 \
		--volume="${PWD}/src/credentials.json:/root/credentials.json" \
		--tty \
	${image}
```
