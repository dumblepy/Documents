Cloud Run
===

- 開発環境はdokcer-composeで
- デプロイ環境はDockerfile単体で動くようにする

Makefile
```makefile
region=asia-northeast1
project=
repogitory=
image=

login:
	gcloud auth login
	gcloud config set core/project ${project}
	gcloud config list

dev:
	docker-compose -f docker-compose.dev.yml build
	docker-compose -f docker-compose.dev.yml up -d

container-stg=`docker ps -a | grep "${image}" | awk '{print $$1}'`

dev-build:
	docker rm -f $(container-stg) || :
	docker build --tag=${image} --file="./docker/stg/Dockerfile" .
	docker run --detach --name="${image}" --publish=8080:8080 --tty ${image}

stg:
	gcloud auth configure-docker ${region}-docker.pkg.dev
	docker build --tag=${image} --file="./docker/stg/Dockerfile" .
	docker tag ${image} ${region}-docker.pkg.dev/${project}/${repogitory}/${image}
	docker push ${region}-docker.pkg.dev/${project}/${repogitory}/${image}
```

Dockerfile
```dockerfile
FROM node:14.18.1-alpine3.14
RUN echo http://dl-cdn.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories
RUN apk update && \
    apk upgrade --no-cache && \
    apk add --no-cache \
        git \
        yarn
WORKDIR /app
COPY ./ /app
WORKDIR /app/src
RUN yarn install
EXPOSE 8080
CMD ["yarn", "stg"]
```

```sh
.
├── README.md
├── docker
│   ├── dev
│   │   └── Dockerfile
│   ├── prd
│   │   └── Dockerfile
│   └── stg
│       └── Dockerfile
├── docker-compose.dev.yml
├── makefile
├── permission.sh
└── src
    ├── app
    │   └── controllers
    ├── index.ts
    ├── node_modules
    ├── package.json
    ├── tsconfig.json
    └── yarn.lock
```