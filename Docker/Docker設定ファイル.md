# Docker 設定ファイル

[Dockerfile のベストプラクティス Top 20](https://sysdig.jp/blog/dockerfile-best-practices/)

ターミナルを省略せずにビルド
```
docker compose --progress=plain build
```

## compose.yaml

```yml
services:
```

## OS 依存ライブラリ更新

### alpine

```nginx
RUN echo http://dl-cdn.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories
RUN apk update && \
    apk upgrade --no-cache
RUN apk add --no-cache \
      xxx
```

### debian ubuntu

```
# prevent timezone dialogue
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update  && \
    apt upgrade -y && \
    apt install -y --fix-missing \
      xxx
```

## https-portal

https-portal はただのポートフォワードとして使う  
https-portal => Nginx => Application

```yml
https-portal:
  image: steveltn/https-portal:1
  ports:
    - "80:80"
    - "443:443"
  tty: true
  restart: always
  environment:
    # STAGE: production
    # DOMAINS: 'domain.com -> http://web'
    STAGE: local
    DOMAINS: "localhost -> http://web"
    SERVER_NAMES_HASH_BUCKET_SIZE: 128 # 長いドメイン対応
  volumes:
    - ./docker/ssl_certs:/var/lib/https-portal # 鍵ファイルをホストとマウントする
  depends_on:
    - web
```

## Nginx

```yml
web:
  image: nginx:1.21.0-alpine
  tty: true
  environment:
    TZ: "Asia/Tokyo"
  depends_on:
    - app
  volumes:
    - ./docker/nginx/app.conf:/etc/nginx/conf.d/default.conf
```

PHP の時の default.conf

```nginx
server {
    listen 80;
    server_name localhost;
    index index.php index.html;
    root /var/www/html/public;
    client_max_body_size 50M;
    charset utf-8;

    # auth_basic "認証情報を入力してください";
    # auth_basic_user_file /etc/nginx/.htpasswd;

    location / {
      try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass laravel:9000; # コンテナ名:ポート
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
}
```

Web アプリ

```nginx
server {
  listen 80 default_server;
  # Cloud Runの時
  # listen       8080;
  # listen  [::]:8080;

  # 拡張子があるもの=静的ファイル
  location ~^(.*\..*) {
    alias /application/public;
  }

  location / {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_redirect off;
    proxy_pass  http://service:8000;
  }
```

SPA

```nginx
server {
    listen       8080;
    listen  [::]:8080;
    server_name  _;

    location / {
        root   /var/www;
        index  index.html;
        try_files $uri /index.html; # SPAのリロードに対応
    }
}
```

Basic 認証

```nginx
server {
    listen       8080;
    listen  [::]:8080;
    server_name  _;

    location / {
        root   /var/www;
        index  index.html;
        try_files $uri /index.html;
        auth_basic	"Restricted";
       	auth_basic_user_file	/etc/nginx/.htpasswd;
    }
}
```

## Python

compose.yaml

```yml
python:
  build: ./docker/python
  tty: true
  volumes:
    - .:/application
  depends_on:
    - db
```

dockerfile

```dockerfile
FROM python:3.10.3-bullseye

RUN apt update && \
    apt upgrade -y

RUN curl -sSL https://install.python-poetry.org | python3 -
ENV PATH $PATH:/root/.local/share/pypoetry/venv/bin
RUN poetry config virtualenvs.in-project true

WORKDIR /application
ENV PATH $PATH:/root/.local/bin
COPY . .
RUN mkdir -p .venv
WORKDIR /application/src
RUN poetry run poetry install
```

gnicorn.py

```py
import multiprocessing

bind = 'unix:/var/run/sock/django.sock'
bind = '0.0.0.0:8000'

# Worker Processes
# workers = 1
# workers = multiprocessing.cpu_count() * 2 + 1
workers = multiprocessing.cpu_count()
worker_class = 'sync'
threads = 1
reload = True

# Logging
accesslog = '/var/log/django/access.log'
errorlog = '/var/log/django/error.log'
loglevel = 'error'
capture_output = True
```

起動

```sh
poetry run gunicorn project.wsgi -c gunicorn.py --reload
```

## Jupyter Lab

compose.yaml

```yaml
version: "3"
services:
  asset-management:
    build:
      context: .
      dockerfile: ./docker/asset-managemtn/Dockerfile
    tty: true
    volumes:
      - .:/application
    ports:
      - 8888:8888
    command: >
      bash -c "
        poetry run jupyter-lab \
          --allow-root \
          --autoreload \
          --ip 0.0.0.0 \
          --no-browser \
          --NotebookApp.token='' \
      "
```

Dockerfile

```dockerfile
FROM python:3.10.3-bullseye

RUN apt update && \
    apt upgrade -y

RUN curl -sSL https://install.python-poetry.org | python3 -
ENV PATH $PATH:/root/.local/share/pypoetry/venv/bin
RUN poetry config virtualenvs.in-project true

WORKDIR /application
ENV PATH $PATH:/root/.local/bin
COPY . .
RUN mkdir -p .venv
WORKDIR /application/src
RUN poetry run poetry install
```

```makefile
run:
	compose build && compose up -d

stop:
	compose stop
```

```
├── docker
│   └── asset-managemtn
│        └── Dockerfile
├── compose.yaml
├── makefile
├── permission.sh
└── src
    ├── README.rst
    ├── app
    │   ├── Untitled.ipynb
    │   └── __init__.py
    ├── poetry.lock
    ├── pyproject.toml
    └── tests
        ├── __init__.py
        └── test_src.py
```

## Nim

compose.yaml

```yml
nim:
  build: docker/app
  tty: true
  ports: # ホストのポート:コンテナのポート
    - 9000:5000
  volumes:
    - ./src:/application
```

dockerfile

- slim: Nimコンパイラのみ
- regular: Nimコンパイラとnimbleパッケージマネージャ
- onbuild: Nimbleパッケージ用のDockerfile

```dockerfile
FROM nimlang/nim:2.0.8-ubuntu-regular

# prevent timezone dialogue
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
        make \
        vim \
        curl \
        git \
        libsass-dev \
        sqlite3 \
        libpq-dev \
        libmariadb-dev
RUN apt autoremove -y

WORKDIR /application
```

```dockerfile
FROM ubuntu:24.04

# prevent timezone dialogue
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update; apt install -y libgnutls30 && \
    apt update --fix-missing && \
    apt upgrade -y
RUN apt install -y --fix-missing \
        gcc \
        xz-utils \
        ca-certificates \
        vim \
        curl \
        git \
        libsass-dev \
        sqlite3 \
        libpq-dev \
        libmariadb-dev
RUN apt autoremove -y

# ===== Nim =====
WORKDIR /root
RUN curl -O https://nim-lang.org/choosenim/init.sh
RUN init.sh -y
RUN rm init.sh
ENV PATH /root/.nimble/bin:$PATH

WORKDIR /application
```


## Nim & Wasm

compose.yaml

```yml
services:
  app:
    build:
      context: .
      dockerfile: ./Dockerfile
    tty: true
    volumes:
      - .:/application
    ports:
      - 9000:9000
```

Dockerfile

```dockerfile
FROM ubuntu:24.04

# prevent timezone dialogue
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
        git \
        curl \
        gcc \
        g++ \
        xz-utils \
        ca-certificates \
        libpcre3-dev \
        unzip \
        make \
        clang \
        cmake
RUN apt autoremove -y

# nodejs
WORKDIR /root
# https://nodejs.org/en/download/prebuilt-binaries
ARG NODE_VERSION=22.13.1
RUN curl -OL https://nodejs.org/dist/${NODE_VERSION}/node-${NODE_VERSION}-linux-x64.tar.xz
RUN tar -xvf node-${NODE_VERSION}-linux-x64.tar.xz
RUN rm node-${NODE_VERSION}-linux-x64.tar.xz
RUN mv node-${NODE_VERSION}-linux-x64 .node
ENV PATH /root/.node/bin:$PATH

# pnpm
RUN curl -fsSL https://get.pnpm.io/install.sh | bash -s -- -y

# python
RUN apt install -y python3

# emcc
WORKDIR /root
RUN git clone https://github.com/emscripten-core/emsdk.git /root/.emsdk
WORKDIR /root/.emsdk
RUN ./emsdk install latest
RUN ./emsdk activate latest
RUN echo 'source "/root/.emsdk/emsdk_env.sh"' >> $HOME/.bashrc

# ninja
WORKDIR /root
RUN curl -OL https://github.com/ninja-build/ninja/releases/download/v1.12.1/ninja-linux.zip
RUN unzip ninja-linux.zip
RUN rm ninja-linux.zip
RUN mv ninja /usr/local/bin

# webt
WORKDIR /root
RUN git clone --recursive https://github.com/WebAssembly/wabt .wabt
WORKDIR /root/.wabt
RUN git submodule update --init
RUN make
RUN make install

# nim
WORKDIR /root
RUN curl -O https://nim-lang.org/choosenim/init.sh
RUN init.sh -y
RUN rm init.sh
ENV PATH /root/.nimble/bin:$PATH

WORKDIR /application
```

## Rust

```dockerfile
FROM ubuntu:24.04

# prevent timezone dialogue
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt upgrade -y && \
    apt install -y --fix-missing \
        gcc \
        xz-utils \
        ca-certificates \
        vim \
        curl \
        git \
        libssl-dev \
        pkg-config \
RUN apt autoremove -y

RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

WORKDIR /application
```

## NodeJS

compose.yaml

```yml
app:
  build:
    context: .
    dockerfile: Dockerfile
  tty: true
  ports:
    - "3000:3000"
  volumes:
    - ../:/application
```

Dockerfile

```dockerfile
FROM ubuntu:24.04

RUN apt update && apt upgrade -y
RUN apt install -y \
      curl \
      xz-utils \
      git
RUN apt autoremove -y

WORKDIR /root
# nodejs
# https://nodejs.org/en/download/prebuilt-binaries
ARG NODE_VERSION=22.13.1
RUN curl -OL https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz
RUN tar -xvf node-v${NODE_VERSION}-linux-x64.tar.xz
RUN rm node-v${NODE_VERSION}-linux-x64.tar.xz
RUN mv node-v${NODE_VERSION}-linux-x64 .node
ENV PATH $PATH:/root/.node/bin
# pnpm
RUN curl -fsSL https://get.pnpm.io/install.sh | bash -s -- -y

RUN git config --global --add safe.directory /application
WORKDIR /application
```

### Preact Tailwind DaisyUI
```sh
cd /application
pnpm create preact

>> Project directory:
src
```

package.json
```json
"scripts": {
  "dev": "vite --mode develop --host 0.0.0.0 --port 3000",
  "build:s": "vite build --mode staging --outDir staging-dist",
  "build:p": "vite build --mode production --outDir production-dist",
  "preview:s": "vite preview --host 0.0.0.0 --port 3000 --outDir staging-dist",
  "preview:p": "vite preview --host 0.0.0.0 --port 3000 --outDir production-dist",
  "staging": "pnpm run build:s && pnpm run preview:s",
  "production": "pnpm run build:p && pnpm run preview:p",
}
```

```sh
pnpm add -D tailwindcss @tailwindcss/vite @tailwindcss/typography autoprefixer daisyui
```

src/vite.config.ts
```ts
import { defineConfig } from 'vite';
import preact from '@preact/preset-vite';
import tailwindcss from '@tailwindcss/vite';

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [
		preact({  
			prerender: {
				enabled: true,
				renderTarget: '#app',
			},
		}),
		tailwindcss(),
	],
});
```

src/style.css
```css
@import "tailwindcss";
@plugin "daisyui";
```

### Hash Routing

```sh
pnpm add wouter
```
Header.tsx
```tsx
import { Link } from "wouter"

export const Header = () => {
  return (
    <div class="navbar bg-base-100 shadow-sm">
      <div class="flex-1">
        <Link href="/" class="btn btn-ghost text-xl">main</Link>
        <Link href="#page1" class="btn btn-ghost text-xl">page1</Link>
        <Link href="#page2" class="btn btn-ghost text-xl">page2</Link>
      </div>
    </div>
  )
}
```

index.tsx
```tsx
import { Router, Route, Switch } from "wouter";
import { useHashLocation } from "wouter/use-hash-location";

export function App() {
	return (
		<>
			<Router hook={useHashLocation} base="/">
				<Header />
				<Switch>
					<Route path="/" component={HomePage} />
					<Route path="page1" component={HomePage} />
					<Route path="page2" component={HomePage} />
					<Route component={Page404} />
				</Switch>
			</Router>
		</>
	);
}
```

## Solidity

compose.yaml

```yml
app:
  build:
    context: .
    dockerfile: Dockerfile
  tty: true
  environment:
    TZ: Asia/Tokyo
  ports:
    - 8545:8545
    - 3000:3000
  volumes:
    - .:/root/project
```

Dockerfile

```dockerfile
FROM ubuntu:24.04

# prevent timezone dialogue
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y
RUN apt install -y \
  git \
  curl \
  xz-utils \
  ca-certificates
RUN apt autoremove -y

# nodejs
WORKDIR /root

# https://nodejs.org/en/download/prebuilt-binaries
ARG NODE_VERSION=22.13.1
RUN curl -OL https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz
RUN tar -xvf node-v${NODE_VERSION}-linux-x64.tar.xz
RUN rm node-v${NODE_VERSION}-linux-x64.tar.xz
RUN mv node-v${NODE_VERSION}-linux-x64 .node
ENV PATH $PATH:/root/.node/bin

# pnpm
RUN curl -fsSL https://get.pnpm.io/install.sh | bash -s -- -y

# foundry
RUN curl -L https://foundry.paradigm.xyz | bash
ENV PATH=/root/.foundry/bin/:$PATH
RUN foundryup

RUN git config --global --add safe.directory /application
WORKDIR /application
```

### 初期ディレクトリ作成
#### foundryでプロジェクトを作成する
```sh
forge init .
```

#### フォーマッターの設定
foundry.tomlに追加
```
[fmt]
line_length = 120
tab_width = 2
```

#### OpenZeppelinをインストール
https://docs.openzeppelin.com/contracts/5.x/#foundry_git

```sh
forge install OpenZeppelin/openzeppelin-contracts
forge install OpenZeppelin/openzeppelin-contracts-upgradeable
touch remappings.txt
```

remappings.txtに追記
```
@openzeppelin/contracts/=lib/openzeppelin-contracts-upgradeable/lib/openzeppelin-contracts/contracts/
@openzeppelin/contracts-upgradeable/=lib/openzeppelin-contracts-upgradeable/contracts/
```

#### hardhatを追加する
[Adding Hardhat to a Foundry project](https://hardhat.org/hardhat-runner/docs/advanced/hardhat-and-foundry#adding-hardhat-to-a-foundry-project)

```sh
pnpm init
pnpm add hardhat
pnpm hardhat init
> Create an empty hardhat.config.js
mv hardhat.config.js hardhat.config.ts
```

#### Typescriptを有効にする
[Using TypeScript](https://hardhat.org/hardhat-runner/docs/guides/typescript#using-typescript)

```sh
pnpm add -D ts-node typescript chai@4 @types/node @types/mocha @types/chai@4 @nomicfoundation/hardhat-toolbox @nomicfoundation/hardhat-foundry
```

hardhat.config.ts
```ts
import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import "@nomicfoundation/hardhat-foundry";

const config: HardhatUserConfig = {
  solidity: "0.8.24",
};

export default config;
```

tsconfig.json
```ts
{
  "compilerOptions": {
    "target": "es2020",
    "module": "commonjs",
    "esModuleInterop": true,
    "forceConsistentCasingInFileNames": true,
    "strict": true,
    "skipLibCheck": true,
    "resolveJsonModule": true
  }
}
```

- 単体テストは`test/Utest`にSolidityで書く
  - `forge test`
- 結合テストは`test/ITest`ディレクトリにTSで書く
  - `pnpm hardhat test`

#### Viemを使う
[Using Viem](https://hardhat.org/hardhat-runner/docs/advanced/using-viem)

```sh
pnpm add viem
pnpm add -D @nomicfoundation/hardhat-viem
```

hardhat.config.tsに追加
```ts
import "@nomicfoundation/hardhat-viem";
```

## ICP

compose.yaml
```yaml
services:
  icp:
    build:
      context: .
      dockerfile: docker/icp/develop.Dockerfile
    tty: true
    volumes:
      - .:/application
    ports:
      - 4943:4943
```

dockerfile

```dockerfile
FROM ubuntu:24.04

# prevent timezone dialogue
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
      git \
      curl \
      xz-utils \
      ca-certificates \
      libunwind-dev \
      build-essential
# libunwind-dev: /root/.cache/dfinity/versions/0.24.3/canister_sandbox: error while loading shared libraries: libunwind.so.8: cannot open shared object file: No such file or directory
# build-essential: gcc, g++, make
RUN apt autoremove -y

WORKDIR /root

# icp
# https://github.com/dfinity/sdk/releases/latest
ARG DFX_VERSION=0.25.0
RUN curl -OL https://internetcomputer.org/install.sh
RUN chmod +x install.sh
ARG DFXVM_INIT_YES=yes
RUN DFXVM_INIT_YES=$DFXVM_INIT_YES DFX_VERSION=$DFX_VERSION ./install.sh

# nodejs
# https://nodejs.org/en/download/prebuilt-binaries
ARG NODE_VERSION=20.18.3
RUN curl -OL https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz
RUN tar -xvf node-v${NODE_VERSION}-linux-x64.tar.xz
RUN rm node-v${NODE_VERSION}-linux-x64.tar.xz
RUN mv node-v${NODE_VERSION}-linux-x64 .node
ENV PATH $PATH:/root/.node/bin

# pnpm
RUN curl -fsSL https://get.pnpm.io/install.sh | bash -
ENV PATH $PATH:/root/.local/share/pnpm

RUN git config --global --add safe.directory /application
WORKDIR /application
```

## SUI

compose.yaml

```yaml
version: "3"
services:
  app:
    build: ./docker/app
    tty: true
    volumes:
      - .:/application
```

dockerfile

```dockerfile
FROM ubuntu:24.04

# prevent timezone dialogue
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
        gcc \
        xz-utils \
        ca-certificates \
        vim \
        curl \
        git \
        libssl-dev \
        pkg-config \
        libssl-dev \
        libclang-dev \
        libpq-dev \
        build-essential
RUN apt autoremove -y

# rust
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
# sui
WORKDIR /root/.sui
RUN curl -L https://github.com/MystenLabs/sui/releases/download/sui-v1.0.0/sui -o /root/.sui/sui
RUN chmod 555 /root/.sui/sui
ENV PATH $PATH:/root/.sui

WORKDIR /application
```

## PHP

### 開発環境

compose.yaml

```yml
php:
  build:
    context: .
    dockerfile: ./docker/dev/app/Dockerfile
  tty: true
  volumes:
    - ./src/app:/var/www
  depends_on:
    - db
  ports:
    - 8000:8000
```

Dockerfile

```dockerfile
FROM php:8.0-fpm
COPY ./docker/php/php.ini /usr/local/etc/php/

RUN apt-get update \
  && apt-get install -y zlib1g-dev mariadb-client vim libzip-dev \
  && docker-php-ext-install zip pdo_mysql

#Composer install
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer

ENV COMPOSER_ALLOW_SUPERUSER 1

ENV COMPOSER_HOME /composer

ENV PATH $PATH:/composer/vendor/bin


WORKDIR /var/www

RUN composer global require "laravel/installer"
RUN composer require --dev beyondcode/laravel-dump-server
```

起動

```sh
# デバッグサーバー
php artisan dump-server
# ブラウザから接続用
php artisan serve --host 0.0.0.0
```

### デプロイ環境

compose.yaml

```yml
app:
  build:
    context: .
    dockerfile: ./docker/dev/app/Dockerfile
  tty: true
  restart: always
  volumes:
    - ./src/app:/var/www
  depends_on:
    - db
```

php.ini

```ini
[Date]
date.timezone = "Asia/Tokyo"
[mbstring]
mbstring.language = "Japanese"

opcache.jit_buffer_size=64M
```

php-fpm.conf

```conf
listen = 127.0.0.1:9000
```

## dotnet

compose.yaml

```yml
dotnet:
  build: ./docker/dotnet
  tty: true
  ports:
    - 9000:5000
  volumes:
    - .:/application
  depends_on:
    - db
```

Dockerfile

```Dockerfile
FROM mcr.microsoft.com/dotnet/sdk:6.0.400-1-jammy-amd64

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
        gcc \
        xz-utils \
        ca-certificates \
        vim \
        curl \
        wget \
        git \
        sqlite3 \
        libpq-dev \
        libmariadb-dev \
        libsass-dev

WORKDIR /application
```

## mysql

compose.yaml

```yml
db:
  image: mariadb:latest #mariadbが準備されているimageを指定
  environment:
    - MYSQL_ROOT_PASSWORD=Rg4p5Qz(aZPZ #rootのパスワード
    - MYSQL_DATABASE=database #データベースの作成
    - MYSQL_USER=root #ユーザの作成
    - MYSQL_PASSWORD=Password #ユーザのパスワード
  volumes:
    - ./database/mysql:/var/lib/mysql # DBの永続化
    - ./initdb:/docker-entrypoint-initdb.d # 初期データ
  ports:
    - "3306:3306"
```

## pgsql

compose.yaml

```yml
db:
  image: postgres:alpine #pgsqlが準備されているimageを指定
  tty: true
  environment:
    POSTGRES_DB: database
    POSTGRES_USER: root
    POSTGRES_PASSWORD: Password
    TZ: Asia/Tokyo
  volumes:
    - ./database/pgsql/:/var/lib/postgresql/data # DBの永続化
    - ./initdb:/docker-entrypoint-initdb.d # 初期データ
  ports:
    - "5432:5432"
```

## phpmyadmin

```yml
pgadmin:
  image: dpage/pgadmin4:6.3
  tty: true
  environment:
    PGADMIN_DEFAULT_EMAIL: admin@mail.com
    PGADMIN_DEFAULT_PASSWORD: password
  ports:
    - 8001:80
  depends_on:
    - db
```

## schemaspy

docker/dev/schema/Dockerfile

```dockerfile
FROM schemaspy/schemaspy:snapshot
USER root
```

compose.yml

```yml
schema:
  build: ./docker/dev/schema
  volumes:
    - ./docker/dev/schema/output:/output:rw
    - ./docker/dev/schema/schemaspy.properties:/schemaspy.properties:ro
  depends_on:
    - db
  # compose upするたびにドキュメント作成されるのが嫌な人は入れる
  # entrypoint: /bin/sh
  # command: ["-configFile", "/schemaspy.properties"]
```

docker/dev/schema/schemaspy.properties

```properties
# type of database. mysql/pgsql. Run with -dbhelp for details
# https://github.com/schemaspy/schemaspy/issues/719#issuecomment-671058655
schemaspy.t=pgsql11
schemaspy.host=db
schemaspy.port=5432
schemaspy.db=database
schemaspy.u=user
schemaspy.p=password
# output dir to save generated files
schemaspy.o=/output/html
# db scheme for which generate diagrams
schemaspy.s=publics
```

### schemaspy 配信用 Nginx

```yml
web: # schemaspyで生成された静的ファイル配信用
  image: nginx:1.21.6-alpine
  tty: true
  ports:
    - "8001:8001"
  volumes:
    - ./docker/dev/web/schema.conf:/etc/nginx/conf.d/schema.conf
    - ./docker/dev/schema/output:/var/www
  depends_on:
    - schema
```

docker/web/schema.conf

```nginx
server {
    listen       8001;
    listen  [::]:8001;
    server_name  localhost;

    location / {
        root   /var/www;
        index  index.html index.htm;
    }
}
```

## adminer

Sqite、MySQL、Postgres 対応の DB の中身を見れるツール

compose.yaml

```yml
adminer:
  image: adminer:4.8.1-standalone
  tty: true
  ports:
    - 8002:8080
  depends_on:
    - db
```

## rman

dockerfile

```dockerfile
FROM ubuntu:20.04

COPY ./docker/app/apt.conf /etc/apt/apt.conf
RUN apt update && \
    apt upgrade -y && \
    apt install -y \
    wget \
    make \
    gcc \
    libpq-dev \
    postgresql-server-dev-all \
    postgresql-common \
    zlib1g-dev \
    libselinux-dev \
    libpam-dev \
    libedit-dev \
    libssl-dev \
    libkrb5-dev

ENV PGDATA /pg_data
ENV BACKUP_PATH /backup
WORKDIR /pgdata/global/pg_control
WORKDIR /backup

WORKDIR /root
# https://github.com/ossc-db/pg_rman/releases
RUN wget https://github.com/ossc-db/pg_rman/releases/download/V1.3.9/pg_rman-1.3.9-pg10.tar.gz --no-check-certificate
RUN tar zxvf pg_rman-1.3.9-pg10.tar.gz && \
    rm pg_rman-1.3.9-pg10.tar.gz
WORKDIR /root/pg_rman-1.3.9-pg10
RUN make && make install
RUN ln -s /root/pg_rman-1.3.9-pg10/pg_rman /usr/local/bin/pg_rman
WORKDIR /home
COPY ./docker/db/backup.sh /home
```

compose.yaml

```yml
rman:
  build:
    context: . # Dockerfileがあるディレクトリへのパス
    dockerfile: ./docker/rman/Dockerfile # Dockerfile名
  tty: true # バックグラウンドで実行しっぱなしにする
  env_file: .env # .envファイルへの相対パス
  environment: # コンテナ内で有効になる環境変数の設定
    TZ: "Asia/Tokyo"
  volumes:
    - ./docker/db/pgdata:/pg_data
    - ./docker/db/backup:/backup
    - ./docker/db/bakup.sh:/home/bakup.sh
```

起動 sh

```sh
pg_rman backup -h rdb -p 5432 -d $DB_NAME \
  -U $DB_USER -W $DB_PASSWORD \
  --backup-mode=full --compress-data --progress
```
