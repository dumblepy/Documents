heroku
===
[戻る](./README.md)

## 環境構築
### アカウント作成
[URL](https://signup.heroku.com)

### インストール
```
wget -qO- https://cli-assets.heroku.com/install-ubuntu.sh | sh
heroku login
heroku keys:add
heroku create
```

以下が表示された
```
Creating app... done, ⬢ agile-tundra-86953
https://agile-tundra-86953.herokuapp.com/ | https://git.heroku.com/agile-tundra-86953.git
```

### デプロイ
プロジェクトのルートディレクトリ(python manage.py runserverやrails sする場所)からpushしないといけない
```
git push heroku master
heroku open
```
ブラウザでデプロイされた環境が開かれる

### プロジェクトを削除する時
settingsタブの一番下のDelete Appから