Ruby環境構築
===
[戻る](../README.md)

Gitをインストール
```
apt install -y git
```

ネイティブにコンパイルする際のライブラリをインストール
```
apt install -y gcc make openssl libssl-dev zlib1g-dev libbz2-dev libreadline-dev libpq-dev libxml2 libxml2-dev liblzma-dev zlib1g-dev libffi-dev ruby-dev patch build-essential
```

Mysqlをインストール
```Linux
apt install -y mariadb-server mariadb-client
```

## anyenv内にrbenvを構築
rbenvをインストール　　
```
anyenv install rbenv  
exec $SHELL -l  
```

rbenvがインストールできるバージョン一覧を表示  
```
rbenv install -l
```

数が多すぎて画面に収まらない場合はファイル出力
```
bash rbenv install -l > rubyV.txt
vi rubyV.txt
```

rubyをインストール
```
rbenv install 2.4.4
```

インストールされたRubyの一覧を表示
```
rbenv versions
```

バージョンを設定
```
#グローバル
rbenv globa x.x.x

#ローカル
rbenv local x.x.x
```

確認
```
rbenv versions
> * 2.4.4 (set by /root/.anyenv/envs/rbenv/version)

which ruby
> /root/.anyenv/envs/rbenv/shims/ruby

ruby -v
> ruby 2.4.4p296 (2018-03-28 revision 63013) [x86_64-linux]
```