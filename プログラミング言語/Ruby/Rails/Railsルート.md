ルート
===
[戻る](../../README.md)

- /config/route.rb

## デフォルト
コマンド
```
bundle exec rails generate controller home top
```
 ↓
```
Rails.application.routes.draw do
  get 'home/top'
end
```
"/home/top"に来た時に、home_controllerのtopメソッドを呼ぶ 

以下の書き方でも同じ
```
get '/home/top', to: 'home#top'
get '/home/top' => 'home#top'
```

## URLパラメーター
/post/1  
/post/2  
```
get '/post/:id', to: 'posts#show'
```
```
def show
end
```




---
```
Rails.application.routes.draw do
    # "/"に来た時applicationコントローラーのhelloメソッドを返す
    root 'application#hello'
    get '/', to: 'application#hello'
end
```
---
```
get 'static_pages/home'
```
/app/controllers/static_pages_controller.rbのhomeメソッドを探す  
見つかった時→homeメソッドを実行  
見つからなかった時→/app/views/static_pages/home.html.erbを探しに行く  


## HTTPメソッド
GET・POST・PATCH・DELETEが使える

