コントローラー
===
[戻る](../../README.md)

## コントローラーを作るコマンド
```
bundle exec rails g controller home top
```
/test/controllers/home_controller_test.rb  
/app/helpers/home_helper.rb  
/app/controllers/home_controller.rb  
/app/views/home/top.html.erb  
が作られる  

消したい場合は
```
rails destroy controller home top
```

## デフォルト
コントローラと同じ名前のビューフォルダから、アクションと同じ名前のHTMLファイルを探してブラウザに返す
```
class HomeController < ApplicationController
  def top
  end
end
```
/app/views/home/top.html.erb が呼ばれる  

## ビューに変数を渡す
コントローラーの中で宣言する変数名には必ず@をつける
```
def index
  @posts = [
    "今日からProgateでRailsの勉強するよー！",
    "投稿一覧ページ作成中！"
  ]
end
```

## リクエストパラメータを取得
### URLパラメータ
/posts/1
```
get "posts/:id" => "posts#show"
```
```
def show
  @id = params[:id]
end
```

### get

### post
```
<%= form_tag("/posts/#{@post.id}/update") do %>
  <textarea name="content"><%= @post.content %></textarea>
  <input type="submit" value="保存">
<% end %>
```
```
def update
  content = params[:content]
end
```

## リダイレクト
```
def create
  redirect_to("/posts/index")
end
```

## ルーティング
ルートがroot 'application#hello'の時
```
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    render html: 'hello world'
  end
end
```

## ORM
モデルから取得
```
#全件取得
posts = Post.all

#並び替え
posts = Post.all.order(created_at: desc)

#一件取得
post = Post.find(1)         #idの値で検索
post = Post.find_by(id: 1)  #指定したカラムと値

#外部キーから取得
posts = Post.where(user_id: 1)

#更新
post = Post.find_by(id: 1)
post.content = "新しい内容"
post.save

#削除
post = Post.find_by(id: 1)
post.destroy
```
[find、find_by、whereの違い](https://qiita.com/tsuchinoko_run/items/f3926caaec461cfa1ca3)

### 並び替え
created_atカラムでdesc(降順)にする
```
def index
  @posts = Post.all.order(created_at: :desc)
end
```

## フラッシュ 成功時メッセージ
```
if `post.save
  flash[:notice] = "投稿を編集しました"
end
```

## デバッグ
コントローラーにdebuggerを入れると、コンソールからその時点での変数の状態を確認できる
```
def show
  @user = User.find(params[:id])
  debugger
end
```

## セッション
### ログイン
```
session[:キー] = 値
session[:use_id] = @user.id
```

### ログアウト
```
session[:use_id] = nil
```

## 継承元クラス
子クラスで使うグローバルな変数
```
class ApplicationController < ActionController::Base
  before_action :set_current_user
  #アクションを呼び出す前にset_current_userメソッドを呼び出す

  def set_current_user
    @current_user = User.find(session[user_id])
  end
end
```


---
## コントローラーを作るコマンド
```
craft controller App
```
/app/http/controllers/AppController.pyが作られる


## リクエストパラメータを取得
/app?arg=abcde
```
def index(self, Request):
    arg =  Request.input('arg')
    print(arg)
```
```
def index(self):
    arg =  request().input('arg')
    print(arg)
```

## ビューを返す
```
def index(self, View):
    return View('index')
```
```
def index(self):
    return view('index')
```

## ビューに変数を渡す
```
def index(self, View):
    arg = 'aaaaa'
    return View('index', {'arg': arg})
```
ビュー
```
<p>{{arg}}</p>
```

## Jsonを返す
```
def index(self):
    val = 12345
    return {'key': val}
```