テスト
===
[戻る](../../README.md)

```
rails generate controller StaticPages home
```
をすると、/test/controllers/static_pages_controller_test.rbが作られる

```
require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  test "should get home" do
    get static_pages_home_url
    assert_response :success
  end
end
```
「static_pages.homeにgetでアクセスした時、200が返ってくれば良い」

## ループ


## テストの実行
```
bundle exec rails test
```

## 自動テスト
Guardを使う  

Gemfile
```
group :test do
  gem 'guard',                    '2.13.0'
  gem 'guard-minitest',           '2.4.4'
end
```

初期化してGuardfileを生成
```
bundle exec guard init
```

<details><summary>Guardfile</summary><div>

```
# Uncomment the following line on the cloud IDE. (Ignore the subsequent warning.)
# notification :libnotify

# Defines the matching rules for Guard.
guard :minitest, spring: "bin/rails test", all_on_start: false do
  watch(%r{^test/(.*)/?(.*)_test\.rb$})
  watch('test/test_helper.rb') { 'test' }
  watch('config/routes.rb')    { integration_tests }
  watch(%r{^app/models/(.*?)\.rb$}) do |matches|
    "test/models/#{matches[1]}_test.rb"
  end
  watch(%r{^app/controllers/(.*?)_controller\.rb$}) do |matches|
    resource_tests(matches[1])
  end
  watch(%r{^app/views/([^/]*?)/.*\.html\.erb$}) do |matches|
    ["test/controllers/#{matches[1]}_controller_test.rb"] +
    integration_tests(matches[1])
  end
  watch(%r{^app/helpers/(.*?)_helper\.rb$}) do |matches|
    integration_tests(matches[1])
  end
  watch('app/views/layouts/application.html.erb') do
    'test/integration/site_layout_test.rb'
  end
  watch('app/helpers/sessions_helper.rb') do
    integration_tests << 'test/helpers/sessions_helper_test.rb'
  end
  watch('app/controllers/sessions_controller.rb') do
    ['test/controllers/sessions_controller_test.rb',
     'test/integration/users_login_test.rb']
  end
  watch('app/controllers/account_activations_controller.rb') do
    'test/integration/users_signup_test.rb'
  end
  watch(%r{app/views/users/*}) do
    resource_tests('users') +
    ['test/integration/microposts_interface_test.rb']
  end
end

# Returns the integration tests corresponding to the given resource.
def integration_tests(resource = :all)
  if resource == :all
    Dir["test/integration/*"]
  else
    Dir["test/integration/#{resource}_*.rb"]
  end
end

# Returns the controller tests corresponding to the given resource.
def controller_test(resource)
  "test/controllers/#{resource}_controller_test.rb"
end

# Returns all tests for the given resource.
def resource_tests(resource)
  integration_tests(resource) << controller_test(resource)
end
```
</div></details>

実行
```
bundle exec guard
```

## 統合テスト
テストを作成
```
bundle exec rails generate integration_test site_layout
```
/test/integration/site_layout_test.rb  
が作られるので、その中に記述する
```
require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
  end
end
```

テスト実行
```
bundle exec rails test:integration
```

## メソッド一覧
### assert_response  
返ってくるレスポンスが何か
```
assert_response :success
```

|:success|:redirect|:missing|:error|
|---|---|---|---|
|200|300-399|404|500-509|

### assert_select  
HTMLの要素が存在するか否か
```
assert_select "title", "Home | Ruby on Rails Tutorial Sample App"
```
```
<title>Home | Ruby on Rails Tutorial Sample App</title>
```
が期待される

Rspec
===
起動コマンド
```
rspec spec/models/*
```

桁指定数値の桁あふれ対応
```
sequence(:closed_card_number) { |n| "#{n}".rjust(16, '9') }
```
