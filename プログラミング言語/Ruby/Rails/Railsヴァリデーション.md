ヴァリデーション
===

[戻る](../../README.md)

## モデル
```
validates :検証するカラム名, {検証内容}
validates :content, {presence: true} #contentカラムに入力必須
```
カンマで区切って複数指定
```
validates :content, {
  presence: true,         #入力必須
  length: {maximum: 140}  #長さ140字
}
```

### 一覧
[Railsバリデーションまとめ](https://qiita.com/shunhikita/items/772b81a1cc066e67930e)
|変数|引数|役割|
|---|---|---|
|presence|true/false|入力必須|
|length|{maximum: n}/{minimum: n}|長さ|
|uniqueness|true/false|値がユニークかどうか|

## コントローラー
```
if @post.save
  #保存できた場合
  flash[:notiuce] = "投稿を編集しました"
  redirect_to("/posts/index")
else
  #失敗した場合
  render("posts/#{@post.id}/edit")
end
```
*redirect_toのURLの頭には「/」をつける。renderのURLには頭に「/」をつけない*

## ビュー
### 成功時
/app/views/layouts/application.html.erb
```
<% if flash[:notice] %>
  <%= flash[:notice] %>
<% end %>
```

### 失敗時
```
<% @post.errors.full_messages.each do |message| %>
  <%= message %>
<%end%>
```

## 日本語化
[Railsのバリデーションエラーのメッセージの日本語化](https://qiita.com/Ushinji/items/242bfba84df7a5a67d5b)

Gemfile
```
gem 'rails-i18n'
```
gemインストール
```
bundle install --path vendor/bundle
```
/app/views/layouts/application.html.erb
```
require_relative 'boot'
require 'rails/all'

Bundler.require(*Rails.groups)

module Tweet
  class Application < Rails::Application
    generated Rails version.
    config.load_defaults 5.2
    config.i18n.default_locale = :ja  #追加
  end
end
```

---

コントローラー内で発生したエラー
===

## コントローラー
@error_messageを自作する
```
def login
  email = params[:email]
  password = params[:password]
  @user = User.find_by(email: email, password: password)

  if @user
    redirect_to("/users/index")
  else
    @error_message = "メールアドレスまたはパスワードが間違っています"

    render("users/login_form")
  end
end
```

## ビュー
```
<% if @error_message %>
  <%= @error_message %>
<% end %>
```