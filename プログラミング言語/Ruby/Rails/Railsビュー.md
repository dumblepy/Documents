ビュー
===
[戻る](../../README.md)

## 共通のレイアウトをまとめる
共通のレイアウトは/app/views/layouts/application.html.erbにまとめる
```
<!DOCTYPE html>
<html>
  <head>
    <title>Tweet</title>
    <%= csrf_meta_tags %>
    <%= csp_meta_tag %>

    <%= stylesheet_link_tag    'application', media: 'all', 'data-turbolinks-track': 'reload' %>
    <%= javascript_include_tag 'application', 'data-turbolinks-track': 'reload' %>
  </head>

  <body>
    <!-- ここにヘッダーを書いてください -->
    <!-- ヘッダー部分始まり -->
    <header>
      <div class="header-logo">
        <a href="/">TweetApp</a>
      </div>
      <ul class="header-menus">
        <li>
          <a href="/about">TweetAppとは</a>
        </li>
      </ul>
    </header>
    <!-- ヘッダー部分終わり -->
    <!-- 各ビューファイルは以下のyieldに代入され、application.html.erbの一部となる -->
    <%= yield %>
  </body>
</html>
```

/app/views/home/top.html.erb  
yieldの中に表示される
```
<div class="main top-main">
  <div class="top-message">
    <h2>つぶやきで、世界はつながる</h2>
    <p>今の気持ちをつぶやいてみよう！</p>
  </div>
</div>
```

## 渡された変数を表示する  
コントローラー
```
@arg = "abcde"
```
ビュー
```
<%= "argの中身は#{@arg}" %>
```

## 他のhtmlの埋め込み
```
<%= render 'form', user: @user %>
```
_form.html.erbを埋め込んでuserに@userを渡している


## CSRF
```
<form method="POST">
    <input type="text" name="arg"/>
    <button type="submit">ボタン</button>
    <%= csrf_meta_tags %>
</form>
```

## HTMLエレメントの代わりに使うもの
### link_to
aタグはlink_toメソッドを使う
```
<a href="/about">About</a>
```
```
<%= link_to("About", "/about") %>
```
postの場合は第三引数で指定
```
<%= link_to("削除", "/posts/#{@post.id}/destroy", {method: "post"}) %>
```

### form_tag
formタグはform_tagメソッドを使う
```
<form action="/posts/create" method="post">
  <textarea></textarea>
  <button type="submit">投稿</button>
</form>
```
```
<%= form_tag("/posts/create") do %>
  <textarea></textarea>
  <button type="submit">投稿</button>
<%= end %>
```

### 画像を送信する時
multipart: trueを設定する
```
<%= form_tag("/posts/create", {multipart: true}) do %>
  <input type="file" name="image">
  <button type="submit">投稿</button>
<%= end %>
```