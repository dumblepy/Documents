Rails環境構築
===
[戻る](../../README.md)

## railsをインストール
### グローバルにrailsインストールする時
```
sudo gem install rails
rails new rails_project -path vendor/bunlde
```

### ローカルにrailsをインストールする時
```
sudo gem install bundler
mkdir project
cd project
bundle init
```
Gemfile編集  
Gemfileのrailsについているコメントアウトを消す

## プロジェクト作成
### herokuに上げる時
```
bundle install --path vendor/bundle --jobs=4 --without production

#Gemfileの上書きはnoにする
bundle exec rails new .
```

### ローカル環境
```
bundle install --path vendor/bundle --jobs=4
yes | bundle exec rails new .
```

## 環境確認
```
bundle exec rails c

Rails.env
=> "development"
```
環境を変えるには、Linuxコマンドで
```
export RAILS_ENV=production
```
