モデル
===
[戻る](../../README.md)

## モデルを追加
頭文字大文字の単数形でテーブル名を指定する  
```
bundle exec rails g model Post content:text
```
/app/models/post.rb が作られる

マイグレーション実行  
```
bundle exec rails db:migrate
```

## モデルを変更
マイグレーションを作成する
```
bundle exec rails g migration add_image_name_to_users
```
マイグレーションファイルを編集  
/db/migrate/20180929075050_add_image_name_to_users.rb
```
class AddImageNameToUsers < ActiveRecord::Migration[5.2]
  def change
    #add_column :テーブル名, :カラム名, :データ型
    add_column :users, :image_name, :string
  end
end
```
変更を反映
```
bundle exec rails db:migrate
```

|変数|役割|
|---|---|
|add_column|カラムを追加|
|remove_column|カラムを削除



---
## モデルを追加

usersテーブル

|カラム名|型|
|---|---|
|id|int|
|name|string|
|email|string|

頭文字大文字の単数形でテーブル名を指定する
```
bundle exec rails g model User name:string email:string
```
/app/models/user.rb  
/test/models/user_test.rb  
/db/migrate/20180920180515_create_users.rb  
/test/fixtures/users.yml  
が新規作成される

マイグレーションファイル
```
class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email

      t.timestamps
    end
  end
end
```

### マイグレーション
```
bundle exec rails db:migrate
```
→テーブルが追加される

#### マイグレーションを取り消したい時
```
bundle exec rails db:rollback
```

最初の状態に戻すには
```
bundle exec rails db:migrate VERSION=0
```

### リセット
「rails s」と「rails console」を閉じてから
```
bundle exec rails db:migrate:reset
```

## モデルのオプション操作
### 文字数制限
```
class Micropost < ApplicationRecord
    validates :content, length: { maximum: 140 }
  end
```
Micropostsテーブルのcontentカラムの長さを140字に制限する

### 外部キー
```
class Micropost < ApplicationRecord
  belongs_to :user
end
```
```
class User < ApplicationRecord
  has_many :microposts
end
```

### 入力必須
presence: trueを指定する
```
class Micropost < ApplicationRecord
  validates :content, length: { maximum: 140 },
                      presence: true
end
```

users

|カラム名|型|外部キー
|---|---|---|
|id|int promary|---|
|name|string|---|
|email|string|---|

microposts

|カラム名|型|外部キー
|---|---|---|
|id|int promary|---|
|content|string|---|
|user_id|int|users.id|

## ActiveRecord(ORM)

## テスト実行
```
bundle exec rails test:models
```