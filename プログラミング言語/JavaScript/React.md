React
===
[戻る](../README.md)

## 環境構築
### インストール
create-react-appをインストール
```
npm install --global create-react-app
```

プロジェクト作成
```
create-reac-app xxx --use-npm
```

オプションのモジュールをインストール
```
npm install -S axios @material-ui/core @material-ui/icons react-cookie react-meta-tags react-router-dom
```

### コマンド
テスト実行
```
npm start
```

ビルド
```
npm run build
```

### 雛形
```
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withStore } from '../../common/store';

import { withRouter } from 'react-router';

class SomeClass extends React.Component {
  state = {

  }

  render(){
    const { classes } = this.props;

    return(
      <p>sample</p>
    );
  }
}

const styles = {

}

export default withStyles(styles)(withRouter(withStore(SomeClass)));
```

```
import {Component} from 'react';

class SomeClass extends Component {
  state = {

  }

  render(){
    const { classes } = this.props;

    return(
      <p>sample</p>
    );
  }
}

export default SomeClass;
```

### reactとmasoniteの連携
```
/build/index.html
↓
/resources/templates/masonite-blog/index.html
```
その他のbuild以下のファイルは全て  
/storage/masonite-blog/  
に移動  

```
#!/bin/bash

# index.html
rm -fr ../../../templates/masonite-blog/*
mv -f build/index.html ../../../templates/masonite-blog/

# static
rm -fr ../../../../storage/masonite-blog/*
mv -f build/* ../../../../storage/masonite-blog/

#service workerのindex.htmlをreactに置換
sed -i -e "s/index.html/react/g" ../../../../storage/masonite-blog/service-worker.js
```
registerServiceWorker.js
```
const swUrl = `${process.env.PUBLIC_URL}/service-worker.js`;
↓
const swUrl = `static/service-worker.js`;
```

storage.py
```
STATICFILES = {
    #masonite-blog
    'storage/masonite-blog/static/': 'static/',
    'storage/masonite-blog/': 'static/',
}
```


## Tips
### ビルド先を指定する方法
[create-react-appでbuild先のパスを変更する](https://qiita.com/yakimeron/items/7a4f8d9e70a4a2b1b96b)  
[Dan Abramov 氏は create-react-app の issues で以下のように発言してます。](https://github.com/facebook/create-react-app/issues/1354#issuecomment-299956790)  
buildフォルダ以下のファイルをpackage.jsonから相対パスで指定した先に移している
```
#package.json

"scripts": {
  "build": "react-scripts build && rm -fr ../../../templates/masonite-blog/* && mv -f build/* /../../templates/masonite-blog/",
},
```
```
#package.json

"scripts": {
  "build": "react-scripts build && ./build.sh",
},
```

### 開発環境と本番環境で定数を切り替える方法
定義
```
#CONST.js

const DEV = {
  num: 1,
  char: 'aaa',
}

const PROD = {
  num: 2,
  char: 'bbb',
}

let CONST = null;
if(process.env.NODE_ENV === 'development'){
  CONST = DEV;
}else if(process.env.NODE_ENV === 'production'){
  CONST = PROD;
}

export default CONST;
```
使用時
```
import CONST from './CONST';

console.log(CONST.num);
console.log(CONST.char);
```

### 関数を一箇所にまとめて定義する方法
定義
```
#Util.js

import React from 'react';

export default class Util extends React.Component{
  static method1 = (arg1, arg2) => {
    return arg1 + arg2;
  }
}
```
使用時
```
import Util from './Util'

let result = Util.method1(1,2);
```

### 関数に引数を渡す方法
```
handler=(e)=>{
  let arg1 = e.currentTarget.getAttribute('data-arg');
  let arg2 = e.currentTarget.dataset.arg;
}

render(){
  return(
    <div onClick={this.handler} data-arg="aaa"/>
  );
}
```

```
handler=(key, value)=>{
  console.log(key);
  console.log(value);
}

render(){
  return(
    <div onClick={this.handler.bind(this, arg)}/>
  );
}
```

### ReactでのCSS in JSの使い方
```
import React from 'react';
import { withStyles } from '@material-ui/core/styles';

class Sample extends React.Component{
  render(){
    const {classes} = this.props;
    return(
      <div className={classes.sample}/>
    );
  }
}

const styles = {
  sample: {
    color: '#f66',
    width: '100vw'
  }
}

export default withStyles(styles)(Sample);
```

## エラー
no space…の時
```
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```
nodeが使える容量を増やしてあげる

## Redux
インストール
```
yarn add redux
```