NodeJS、npm環境構築
===
[戻る](../README.md)

## anyenv内にndenvを構築
ndenvをインストール　　
```
anyenv install ndenv  
exec $SHELL -l  
```

ndenvがインストールできるバージョン一覧を表示  
```
ndenv install -l
```

数が多すぎて画面に収まらない場合はファイル出力
```
bash ndenv install -l > nodeV.txt
vi nodeV.txt
```

nodeをインストール
```
ndenv install v10.0.8
```

インストールされたnodeの一覧を表示
```
ndenv versions
```

バージョンを設定
```
#グローバル
ndenv global x.x.x

#ローカル
ndenv local x.x.x
```

## yarnをインストール
npmを使ってグローバルにyarnをインストール
```
npm install -g yarn

# 確認
yarn --version
```

yarnでインストールするモジュール類にパスを通す
```
vi ~/.bash_profile

# ↓を追加
export PATH="$HOME/.yarn/bin:$PATH"
```