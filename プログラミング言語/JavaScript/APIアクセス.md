APIアクセス
===

[戻る](../README.md)

## APIアクセス
const.js
```js
const DEV = {
  mode: 'dev',
  APIHOST: 'http://localhost:8000'
}

const PROD = {
  mode: 'prod',
  APIHOST: window.location.origin
}

let CONST = null
if (process.env.NODE_ENV === 'development') {
  CONST = DEV
} else if (process.env.NODE_ENV === 'production') {
  CONST = PROD
}

export default CONST
```

util.js
```js
import axios from 'axios'
import CONST from './const'

export default class Util {
  static getAPI=(url) => {
    url = CONST.APIHOST + url
    return axios
      .get(url)
      .then(response => {
        return response
      })
  }

  static postAPI=(url, params) => {
    url = CONST.APIHOST + url
    return axios
      .post(url, params)
      .then(response => {
        return response
      })
  }

  static putAPI=(url, params) => {
    url = CONST.APIHOST + url
    return axios
      .put(url, params)
      .then(response => {
        return response
      })
  }

  static deleteAPI=(url) => {
    url = CONST.APIHOST + url
    return axios
      .delete(url)
      .then(response => {
        return response
      })
  }

  // ==========================================================================

  static getApiWithLogin=(url) => {
    let headers = {
      'X-Login-Id': sessionStorage.getItem('id'),
      'X-Login-Token': sessionStorage.getItem('token')
    }
    url = CONST.APIHOST + url
    return axios
      .get(url, { headers: headers, data: {} })
      .then(response => {
        return response
      })
      .catch(err => {
        if (typeof err.response !== 'undefined') {
          this.loginFale(err.response)
        }
        return err.response
      })
  }

  static postApiWithLogin=(url, params) => {
    let headers = {
      'X-Login-Id': sessionStorage.getItem('id'),
      'X-Login-Token': sessionStorage.getItem('token')
    }
    url = CONST.APIHOST + url
    return axios
      .post(url, params, { headers: headers })
      .then(response => {
        return response
      })
      .catch(err => {
        if (typeof err.response !== 'undefined') {
          this.loginFale(err.response)
        }
        return err.response
      })
  }

  static putApiWithLogin=(url, params) => {
    let headers = {
      'X-Login-Id': sessionStorage.getItem('id'),
      'X-Login-Token': sessionStorage.getItem('token')
    }
    url = CONST.APIHOST + url
    return axios
      .put(url, params, { headers: headers })
      .then(response => {
        return response
      })
      .catch(err => {
        if (typeof err.response !== 'undefined') {
          this.loginFale(err.response)
        }
        return err.response
      })
  }

  static deleteApiWithLogin=(url) => {
    let headers = {
      'X-Login-Id': sessionStorage.getItem('id'),
      'X-Login-Token': sessionStorage.getItem('token')
    }
    url = CONST.APIHOST + url
    return axios
      .delete(url, { headers: headers, data: {} })
      .then(response => {
        return response
      })
      .catch(err => {
        if (typeof err.response !== 'undefined') {
          this.loginFale(err.response)
        }
        return err.response
      })
  }

  static loginFale=(response) => {
    if (response.status === 403) {
      sessionStorage.removeItem('id')
      sessionStorage.removeItem('token')
      window.location.href = '/'
    }
  }
}
```

API.js
```js
import Util from './util'

export default class API {
  static getIndex = () => {
    const url = '/api/sample/'
    return Util.getAPI(url)
  }

  static getShow = (id) => {
    const url = `/api/sample/${id}/`
    return Util.getAPI(url)
  }

  static create = (params) => {
    const url = '/api/sample/create/'
    return Util.postAPI(url, params)
  }

  static update = (params) => {
    const url = `/api/sample/${params.id}/update/`
    return Util.putAPI(url, params)
  }

  static delete = (id) => {
    const url = `/api/sample/${id}/delete/`
    return Util.deleteAPI(url)
  }
}
```