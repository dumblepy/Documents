ReactをJqueryの代わりに使う
===
[戻る](../README.md)

```html
<!DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8" />
    <title>HTML 5 complete</title>
    <script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <!-- <script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script> -->
    <!-- router -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react-router-dom/5.1.2/react-router-dom.min.js"></script>
    <!-- babel -->
    <script src="https://unpkg.com/babel-standalone@latest/babel.min.js" crossorigin="anonymous"></script>
    <!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>

      <![endif]-->
  </head>

  <body>
    <main></main>
    <script type="text/babel">
      const {useState} = React
      const {BrowserRouter, Switch, Route, Link } = ReactRouterDOM

      function Header(){
        return <div>
          <Link to="/sample/react" style={{marginRight: '10px'}}>react</Link>
          <Link to="/sample/react/page1" style={{marginRight: '10px'}}>page1</Link>
          <Link to="/sample/react/page2" style={{marginRight: '10px'}}>page2</Link>
        </div>
      }

      function Index(){
        let [count, setCount] = useState(0)
        
        return <div>
          <h1>index</h1>
          <button onClick={function(){setCount(count+1)}}>add</button>
          <p>{count}</p>
        </div>
      }

      function Page1(){
        return <h1>page1</h1>
      }

      function Page2(){
        return <h1>page2</h1>
      }

      function App(){
        return <div>
          <a href="/">back</a>
          <BrowserRouter>
            <Header/>
            <Switch>
              <Route exact path='/sample/react' children={<Index/>} />
              <Route exact path='/sample/react/page1' children={<Page1/>} />
              <Route exact path='/sample/react/page2' children={<Page2/>} />
            </Switch>
          </BrowserRouter>
        </div>
      }

      ReactDOM.render(<App/>,document.querySelector('main'))
    </script>
  </body>

  </html>
  ```