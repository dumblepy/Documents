JavaScript Tips
===
[戻る](../README.md)

## セッションストレージ
### 登録
```
window.sessionStorage.key = value;
```
### 読み込み
```
let value = window.sessionStorage.getItem('key');
```
### 削除
```
window.sessionStorage.removeItem('key');
```
