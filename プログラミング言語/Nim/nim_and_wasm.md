Nim and wasm
===

必要なライブラリ
```sh
nimble install jsbind
```

package.json
```json
{
  "name": "wasm-nim",
  "version": "1.0.0",
  "main": "index.js",
  "license": "MIT",
  "scripts": {
    "dev": "ts-node-dev index.ts"
  },
  "dependencies": {
    "@types/express": "^4.17.13",
    "@types/node": "^17.0.23",
    "express": "^4.17.3",
    "express-promise-router": "^4.1.1",
    "node-fetch": "^3.2.3",
    "ts-node-dev": "^1.1.8",
    "typescript": "^4.6.3"
  }
}
```

## Nim
```nim
import jsbind/emscripten

proc multiply*(x, y: int32): int32 {.EMSCRIPTEN_KEEPALIVE.} =
  return x * y
```

nim.cfg
```sh
@if emscripten:
  cc = clang
  clang.exe = "emcc"
  clang.linkerexe = "emcc"
  clang.options.linker = ""
  cpu = "i386"
  noMain = "on"
  warning[GcMem]= off
  passC = "-s WASM=1 -s EXPORTED_RUNTIME_METHODS=\"['cwrap']\" -s -O3"
  passL = "-s WASM=1 -s EXPORTED_RUNTIME_METHODS=\"['cwrap']\" -s -O3"
@end
```
コンパイル
```sh
nim c -d:release -d:emscripten --gc:arc -o:wasm_module.js wasm_module.nim
```

## TS
```ts
const wasmModule = require('./wasm_module')
wasmModule._multiply(a, b)
```

