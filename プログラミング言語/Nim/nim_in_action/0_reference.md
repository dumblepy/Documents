## Nim Reference
### 文法
```nim
const x = 5 # コンパイル時定数
let y = “Hello” # 不変変数
var z = [1, 2, 3] # 可変変数
```
```nim
proc name(param: int): ReturnType = body
method name(param: float): ReturnType = body
iterator items(list: seq[int]): int = body
template name(param: typed) = body
macro name(param: string):untyped = body
```
```nim
if x > 5:
  body
elif y == "Hello":
  body
else:
  body

for item in list:
  body

while x == 5:
  if y.len > 0:
    break
  else:
    continue

case x
of 5:
  body
of 1, 2, 3: body
of 6..30:
  body

for i in 0..<len(list):
  body

try:
  raise err
except Exception as exc:
  echo(exc.msg)
finally: discard
```

### Input/Output
```nim
echo(x, 42, "text")
stdout.write("text")
stderr.write("error")
stdin.readLine()
readFile("file.txt")
writeFile("file.txt", "contents")
open("file.txt", fmAppend)
```

### 型定義
```nim
type
  MyType = object
    field: int

type
  Colors = enum
    Red, Green,
    Blue, Purple

type
  MyRef = ref object
    field*: string
```