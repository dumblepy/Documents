Parallelism
===

> **本章の内容**
> - 並列処理の重要性を探る
> - 並行処理と並列処理を比較する
> - Nim のスレッドについて知る
> - 正規表現などを使った高度なデータ解析を使った高度なデータ解析
> - 大規模データセットの解析の並列化

すべてのコンピュータ・プログラムは1つまたは複数の計算を行い、これらの計算は通常、順次実行されます。つまり、現在の計算が終了してから次の計算を開始することになります。例えば、(2 + 2) x 4 という単純な計算を考えてみましょう。この場合、まず足し算を計算して 4 を出し、次に掛け算をして 16 を出します。この例では、計算が順次実行されます。

並行処理とは、複数の計算を、他の計算の完了を待たずに進めることです。このような計算形態は、第3章で開発したチャットアプリケーションのようなI/Oアプリケーションなど、多くの場面で役に立ちます。順次実行される場合、このようなアプリケーションは、入力または出力操作の完了を待って時間を浪費します。同時実行により、この時間を別のタスクに使うことができ、アプリケーションの実行時間を劇的に短縮することができます。第3章で並行処理について学びましたが、この章では並列処理と呼ばれる関連した概念について学びます。

Nim には、futures や await といった非同期 I/O 機能、新しいスレッドを生成する spawn など、並行処理や並列処理を行うための機能が多数組み込まれています。第3章では、これらの機能のいくつかをすでに見てきました。

Nim の並列処理はまだ発展途上であり、本章で説明する機能は変更されたり、より堅牢な機能に置き換わったりする可能性があることを意味します。しかし、Nim における並列処理の核となる概念は変わらないはずで、本章で学ぶことは他のプログラミング言語にも応用できるはずです。

Nim の並列処理機能を紹介するだけでなく、この章では簡単なパーサーの実装を通して、パーサーを作成するための様々な方法を紹介する予定です。この章の終わりには、パーサを最適化することで を最適化し、複数の CPU コアで並列実行できるようにします。

## 6.1 Concurrency vs. parallelism
現在では、ほとんどのOSがマルチタスク（一定時間内に複数のタスクを実行する機能）をサポートしています。タスクは通常プロセスと呼ばれ、実行されるコンピュータ・プログラムのインスタンスである。各CPUは一度に1つのプロセスしか実行しませんが、マルチタスクにより、OSはプロセスの実行終了を待つことなく、CPU上で現在実行中のプロセスを変更することができます。図6.1にマルチタスクOSで2つのプロセスを同時に実行する様子を示します。

CPUは非常に高速なので、プロセスAを1ナノ秒、プロセスBを2ナノ秒、プロセスAをさらに1ナノ秒実行することができます[^1]。CPUは一度に一つの命令しか実行できないのに、複数のプロセスが同時に実行されているように見える。このように、見かけ上 このように、複数のプロセスが同時に実行されているように見えることを「同時実行」という。

 [^1]: ここでは単純に考えるため、コンテキストスイッチにかかる時間は無視することにする。

![6.1](./img/6.1.jpg)

近年、マルチコアCPUが普及しています。これは、複数の命令を同時に実行できる独立したユニットを2つ以上組み合わせたCPUです。

このため，マルチタスクOSでは，2つ以上のプロセスを同時に並列に実行することができます．

図6.2は、デュアルコアCPUで2つのプロセスを並列に実行する様子を示している。

シングルコアCPUとは異なり、デュアルコアCPUでは、実際に2つのプロセスを同時に実行することができます。このような実行は並列処理と呼ばれ、複数の物理CPU上か、インテルのハイパースレッディング（HT）テクノロジーのような同時マルチスレッディング（SMT）テクノロジーによってのみ実現可能です。並行処理と並列処理は一見似ているようですが、同じものではありません。

プロセスに加えて、OSはスレッドの実行も管理します。スレッドはプロセスの構成要素であり、同じプロセス内に複数存在することができる。プロセスと同じように同時または並行して実行できるが、プロセスとは異なり、スレッド同士でメモリなどの資源を共有します。

マルチコアCPUの能力を最大限に活用するためには、CPUに負荷のかかる計算を並列化する必要があります。そのためには，複数のプロセスを利用することもできますが，大量のデータを共有するような計算には，スレッドがより適しています．

第3章で紹介した非同期待ち行列は厳密には並列処理です．

非同期コードは常に単一のスレッドで実行されるため、並列処理ではありません。つまり、マルチコアCPUの能力をフルに発揮することはできません。

![6.2](./img/6.2.jpg)

> **PARALELLEL ASYNC AWSIT**  
> Nim の将来のバージョンでは、並列に動作する非同期 await が搭載される可能性が非常に高いです。

非同期Awaitとは異なり、spawnは並列であり、マルチコアCPUで実行することで恩恵を受けるCPU負荷の高い計算のために特別に設計されています。

> **PARALELLISM IN OTHER PROGRAMMING LANGUAGE**  
> PythonやRubyなど一部のプログラミング言語では、インタープリタにグローバルロックがかかっているため、スレッドレベルの並列処理をサポートしません。このため、スレッドを使用するアプリケーションでは、マルチコアCPUの能力を十分に発揮することができません。この制限を回避する方法はありますが、スレッドほど柔軟ではないプロセスを使用する必要があります。

## 6.2 Using threads in Nim
さて、並行処理と並列処理の違いを学んだところで、Nim でスレッドを使う方法を学ぶ準備ができたと思います。

Nim にはスレッドを扱うためのモジュールが2つあります。threads モジュール (http://nim-lang.org/docs/threads.html) はスレッドを手動で作成する機能を公開します。この方法で作成されたスレッドは指定された手続きを即座に実行し、その手続きの実行時間の間だけ実行されます。threadpoolモジュール(http://nim-lang.org/docs/threadpool.html)もあり、これはスレッドプールを実装しています。これは、指定された手続きをスレッドプールのタスクキューに追加する spawn を公開します。しかし、プロシージャをspawnする行為は、それがすぐに別のスレッドで実行されることを意味しない。スレッドの生成はスレッドプールによって完全に管理される。

この後の節で、2つの異なるスレッドモジュールについて全て説明します。この後の節で、2つの異なるスレッドモジュールについてすべて説明しますので、今紹介した新しい用語に圧倒されないでください。

### 6.2.1 The threads module and GC safety
このセクションでは、threadsモジュールについて見ていきます。しかし、その前に Nim でスレッドがどのように動作するかを説明しておく必要があります。特に、Nim におけるガベージコレクタセーフティ ( GC safety ) とは何か知っておく必要があります。Nim と他の多くのプログラミング言語におけるスレッドの動作方法には、非常に重要な違いがあります。Nim の各スレッドはそれぞれ独立したメモリヒープを持っています。スレッド間のメモリ共有は制限されているので、レースコンディションを防ぎ、効率を向上させるのに役立ちます。

また、各スレッドが独自のガベージコレクタを持つことで、効率も向上しています。メモリを共有するスレッドの他の実装では、ガベージコレクタがそのビジネスを行う間、すべてのスレッドを一時停止する必要があります。これは、アプリケーションに問題のある一時停止を追加することができます。

このスレッドモデルが実際にどのように機能するか見てみましょう。次のリスト は、コンパイルできないコードサンプルを示しています。

**Listing 6.1 Mutating a global variable using a Thread**
```nim
# data という名前の新しい変更可能なグローバル変数を定義し、
# テキスト "Hello World" をそれに代入する。
var data = "Hello World"

# 新しいスレッドで実行される新しいプロシージャを定義します。
# このことを示すために {.thread.} プラグマを使用しなければなりません。
proc showData() {.thread.} =
  # データ変数の値を表示する。
  echo(data)

# 新しいスレッドを格納するための変数を定義する。
# 汎用パラメータは、スレッドプロシージャが受け取るパラメータの種類を意味します。
# この場合、voidはプロシージャがパラメータを取らないことを意味します。
var thread: Thread[void]
# createThreadプロシージャは、指定されたプロシージャを新しいスレッドで実行します。
createThread[void](thread, showData)
# スレッドが終了するのを待つ
joinThread(thread)
```

> **THE THREADS MODEL**  
> threads モジュールは暗黙のうちにインポートされる system モジュールの一部なので、明示的にインポートする必要はありません。

この例は、Nim の GC 安全機構で何が禁止されているかを示しています。この例がコンパイルできるように修正する方法は、後で説明します。

リスト6.1のコードをlisting01.nimとして保存し、`nim c --threads:on listing01.nim` を実行してコンパイルしてください。スレッドサポートを有効にするには、`--threads:on` フラグが必要です。このようなエラーが表示されるはずです。

```
listing01.nim(3, 6) Error: 'showData' is not GC-safe as it accesses
➥ 'data' which is a global using GC'ed memory
```

このエラーは、問題をかなりよく表しています。グローバル変数dataはメインスレッドで作成されたので、メインスレッドのメモリに属している。showData スレッドは他のスレッドのメモリにアクセスできませんし、アクセスしようとしても、コンパイラによって GC safe であるとは見なされません。コンパイラは GC safe でないスレッドの実行を拒否します。

プロシージャは、それがガベージコレクションされたメモリを含む任意のグローバル変数にアクセスしない限り、コンパイラによってGC安全であるとみなされます。代入やある種の突然変異もアクセスとしてカウントされ、許可されません。ガベージコレクションメモリには、次のような種類の変数があります。

- string
- seq[T]
- ref T
- クロージャのイテレータとプロシージャ、およびそれらを含む型。

GCセーフなスレッド間メモリ共有の方法は他にもあります。例えば、showData へのパラメータの1つとして、データの内容を渡すことができます。次のリストは、スレッドにパラメータとしてデータを渡す方法を示しています。リスト 6.2 と 6.1 の違いは、太字で示されています。

**Listing 6.2 Passing data to a thread safely**
```nim
var data = "Hello World"

# プロシージャ定義に文字列型の引数が指定されている。
proc showData(param: string) {.thread.} =
  # グローバル変数のデータではなく、
  # プロシージャの引数がechoに渡されます。
  echo(param)

# voidは、showDataプロシージャが受け取るパラメータの種類を示すために
# stringに置き換えられました。
var thread: Thread[string]
# グローバル変数dataは createThread プロシージャに渡され、
# このプロシージャはそれを showData に渡します。
createThread[string](thread, showData, data)
joinThread(thread)
```

リスト6.2のコードをlisting2.nimとして保存し、`nim c --threads:on listing2.nim` を使ってコンパイルしてください。コンパイルは成功し、プログラムを実行すると "Hello World" と表示されるはずです。

createThreadプロシージャは、作成するスレッドに1つの変数しか渡すことができません。スレッドに複数の別々のデータを渡すには、データを保持する新しい型を定義する必要があります。次のリストは、これを行う方法を示しています。

**Listing 6.3 Passing multiple values to a thread**
```nim
type
  ThreadData = tuple[param: string, param2: int]

var data = "Hello World"

proc showData(data: ThreadData) {.thread.} =
  echo(data.param, data.param2)

var thread: Thread[ThreadData]
createThread[ThreadData](thread, showData, (param: data, param2: 10))
joinThread(thread)
```

#### EXECUTING THREADS
前のリストで作成されたスレッドは、あまり効果がありません。これらのスレッドの実行を調べ、2つのスレッドが同時に作成され、数行のテキストを表示するように指示されたときに何が起こるかを見てみましょう。次の例では つの整数の列が表示されます。

**Listing 6.4 Executing multiple threads**
```nim
var data = "Hello World"

proc countData(param: string) {.thread.} =
  # 0 から param 引数の長さから 1 を引いた値まで反復処理する。
  for i in 0 .. <param.len:
    # 改行文字を表示せず、現在の反復カウンターを表示する。
    stdout.write($i)
  # 次の行へ移動します。
  echo()

# 今回は、配列に格納された2つのスレッドがあります。
var threads: array[2, Thread[string]]
# スレッドを作成し、threads 配列の要素の 1 つに割り当てる。
createThread[string](threads[0], countData, data)
createThread[string](threads[1], countData, data)
# すべてのスレッドが終了するのを待ちます。
joinThreads(threads)
```

リスト6.4のコードをlisting3.nimとして保存し、コンパイルして実行します。リスト6.5は、ほとんどの場合において出力がどのように見えるかを示しており、リスト6.6はその代わりにどのような出力が見えるかを示しています。

**Listing 6.5 First possible output when the code in listing 6.4 is executed**
```
001122334455667788991010
```

**Listing 6.6 Second possible output when the code in listing 6.4 is executed**
```
012345678910
012345678910
```

スレッドの実行は、使用するOSとコンピュータに完全に依存します。私のマシンでは、リスト 6.5 の出力は 2 つのスレッドが 2 つの CPU コアで並行して実行された結果だと思われます。一方、リスト 6.6 の出力は、最初のスレッドが 2 番目のスレッドの開始前に終了した結果だと思われます。お使いのシステムでは異なる結果が得られるかもしれません。図6.3は、最初の結果と2番目の結果の両方の実行がどのように見えるかを示しています。

![6.3](./img/6.3.jpg)

threads モジュールを使って作成されたスレッドは、かなりリソースを消費します。

メモリを大量に消費するので、大量に作成するのは非効率的です。

アプリケーションが使用するスレッドを完全に制御する必要がある場合は有用ですが、ほとんどの使用例では threadpool モジュールの方が優れています。では、どのように threadpoolモジュールがどのように動作するかを見てみましょう。

### 6.2.2 Using thread pools
複数のスレッドを使用する主な目的は、コードを並列化することです。CPUに負荷のかかる計算では、できるだけ多くのCPUパワーを使用する必要があり、これにはマルチコアCPUのすべてのコアのパワーを使用することも含まれます。

1 つのスレッドが使用できる CPU コアは 1 つです。すべてのコアのパワーを使うには、1つのコアに1つのスレッドを作成すればよいのです。しかし、最大の問題は、そのスレッドがすべてビジー状態であることを確認することです。100個のタスクがあったとして、それがすべて同じ時間で完了するわけではありませんから、それをスレッドに分散させるのは簡単なことではありません。

あるいは、1つのタスクにつき1つのスレッドを作成することもできます。しかし、これはそれ自身の問題を引き起こします。なぜなら、スレッドの作成には非常にコストがかかるからです。多数のスレッドがあると OSのオーバーヘッドにより、多くのメモリを消費します。

#### WHAT IS A THREAD POOL ?
threadpool モジュールは、複数のスレッドへのタスクの分散を管理する抽象化機能を実装しています。スレッド自体もスレッドプールによって管理されます。

spawn コマンドは手続き呼び出しの形でタスクをスレッドプールに追加し、スレッドプールが管理するスレッドの一つでタスクを実行することを可能にします。スレッドプールは、タスクがすべてのスレッドをビジー状態にして、CPUのパワーを最大限に活用することを保証します。図6.4は、スレッドプールがどのようにタスクを管理するかを示しています。

![6.4](./img/6.4.jpg)

#### USING SPAWN
spawnプロシージャは、ほとんどの場合、プロシージャ呼び出しである式を受け付けます。

spawnは呼び出されたプロシージャの戻り値を保持するFlowVar[T]型の値を返します。これは、スレッドが値を返すことができないthreadsモジュールと比較した場合の利点です。

次のリストは、リスト6.4のコードのspawnに相当するものです。

**Listing 6.7 Executing multiple threads using spawn**
```nim
# spawn を使用するには、
# threadpool モジュールを明示的にインポートする必要があります。
import threadpool
var data = "Hello World"

# spawnに渡されるプロシージャには{.thread.}プラグマは必要ない。
proc countData(param: string) =
  for i in 0 .. <param.len:
    stdout.write($i)
  echo()

# プロシージャを起動するための構文は、
# createThreadを使用するよりもはるかに簡単です。
spawn countData(data)
spawn countData(data)

# syncプロシージャは、
# 生成されたすべてのプロシージャが終了するのを待ちます。
sync()
```

リスト6.7のコードをlisting4.nimとして保存し、コンパイルして実行してください。このとき、`--threads:on` フラグを指定する必要があることを覚えておいてください。出力は、リスト6.5と6.6に示した出力とほぼ同じになるはずです。`spawn` を使って実行される手続きは、`GC safe` でなければなりません。

#### RETRIEVING RETURN VALUES FROM THE F LOW V AR TYPE
ここでは、spawnされたプロシージャから戻り値を取得する方法を示す例を見てみましょう。これは、`FlowVar[T]`型を扱うことになります。
`FlowVar[T]`は、第3章で使用した`Future[T]`型と同じようなコンテナと考えることができます。最初は、このコンテナの中には何も入っていません。生成されたプロシージャが別スレッドで実行されると、未来のいつかの値を返します。
そのとき、返された値は`FlowVar`コンテナに入れられます。
次のリストは、第3章にある`readLine`プロシージャを示したもので、whileループを使ってブロックせずにターミナルからテキストを読み込んでいます。

**Listing 6.8 Reading input from the terminal with spawn**
```nim
# threadpoolモジュールはspawnに必要です。
# osモジュールは sleepプロシージャを定義している。
import threadpool, os

# readLine手続きをスレッドプールに追加します。
# spawnは、lineFlowVar変数に代入されるFlowVar[string]型を返します。
let lineFlowVar = spawn stdin.readLine()
# lineFlowVarにreadLineが返す文字列が含まれるまでループします。
while not lineFlowVar.isReady:
  # プログラムの動作に関するステータスメッセージをいくつか表示します。
  echo("No input received.")
  echo("Will check again in 3 seconds.")
  # メインスレッドを3秒間サスペンドする。
  # sleepのパラメータはmsである。
  sleep(3000)

# ループが終了すると、lineFlowVar は ^演算子ですぐに読み出すことができる。
# この行は、readLineによって読み込まれた入力を表示する。
echo("Input received: ", ^lineFlowVar)
```

リスト6.8をlisting5.nimとして保存し、コンパイルして実行します。このアプリケーションは、あなたが端末に何か入力をするまで待ちます。このアプリケーションは、3秒ごとに入力があるかどうかをチェックします。

`FlowVar`型の使い方は簡単です。その中の値を `^`演算子で読み取ることができるが、この演算子は、呼び出された `FlowVar` が値を含むまで、それが使用されているスレッドをブロックします。`FlowVar`が値を含んでいるかどうかは、`isReady`プロシージャを使って確認することができます。リスト6.8は、`lineFlowVar`変数が値を含んでいるかどうかを3秒ごとに定期的にチェックします。

リスト6.8は、`FlowVar[T]`がどのように動作するかを示すためのものであることに留意してください。

このプログラムでは、3秒ごとに入力をチェックするだけなので、あまり実用的なものではありません。

この例では、メインスレッドで他に何も実行されていないので、メインスレッドで`readLine`を呼び出すのと同じです。より現実的な例としては、`sleep(3000)`ステートメントを、メインスレッドで何か役に立つ仕事をする他のプロシージャに置き換えることができます。例えば、アプリケーションのユーザーインターフェイスを描画したり、第3章のように非同期I/Oイベントループの`poll`プロシージャを呼び出したりすることができます。

### 6.2.3 Exceptions in threads
別スレッドでの例外の振る舞いは意外と知られていないかもしれません。あるスレッドが未処理の例外でクラッシュすると、アプリケーションも一緒にクラッシュします。FlowVarの値を読んでも読まなくても関係ない。

> **FUTURE VERSION**  
> この動作はNimの将来のバージョンで変更され、FlowVarの値を読み込まない限り例外が発生しないようになります。

次のリストは、この動作の様子を示しています。

**Listing 6.9 Exceptions in a spawned procedure**
```nim
import threadpool

proc crash(): string =
  raise newException(Exception, "Crash")

let lineFlowVar = spawn crash()
sync()
```

リスト6.9をlisting6.nimとして保存し、コンパイルして実行します。出力に、クラッシュプロシージャの raise 文を示すトレースバックが表示されるはずです。

> **THE RAISES PRAGMA**  
> raises プラグマは、スレッドがすべての例外を処理することを保証するために使用されます。これを利用するには、次のように `crash` プロシージャを定義します： `proc crash(): string {.raises: [].} = `... . これは、`crash`プロシージャが例外を発生させないことをマークします。この手続きで発生させることができる例外は、角括弧の中で指定します。

要約すると、生成されたプロシージャに引数を渡すことと、その結果を受け取ることの両方が簡単なため、`spawn`は比較的短い実行時間を持つタスクに向いていると言えます。そのようなタスクは通常、実行の最後に結果を生成するので、実行が停止するまで他のスレッドと通信する必要はありません。

定期的に他のスレッドと通信する必要がある長時間実行のタスクでは、代わりに`threads`モジュールで定義された`createThread`プロシージャを使用する必要があります。

## 6.3 Parsing data
Nim でのスレッドの使い方が分かったところで、実際にどのように使えるのか例を見てみましょう。このセクションの例では、パーサーに関連し、Nim の並行処理と並列処理機能を使った実用的な使用例を紹介します。

毎日、多くのデータが様々なソースから、様々な用途を想定して生成されています。コンピュータはこのデータを処理するための非常に便利なツールですが、そのデータを消費するためには、コンピュータがデータの保存形式を理解する必要があります。

パーサーは、データを入力として受け取り、そのデータからデータ構造を構築するソフトウェアコンポーネントである。入力データは通常、テキスト形式です。第3章では、JSON データ形式と、json モジュールを使って特定の情報を照会できるデータ構造にパースする方法について見てきました。

単純なデータ形式のカスタムパーサーを書かなければならない時がよくあります。Nim にはこのようなタスクに取り組むための方法がたくさんあります。

このセクションでは、Wikipedia のページビューデータ用のパーサを書く方法を紹介します。[^2] このデータは様々な用途に使えますが、このセクションでは英語版 Wikipedia で最も人気のあるページを見つけるアプリケーションを作成します。その過程で、以下のことを行います。

 [^2]: https://wikitech.wikimedia.org/wiki/Analytics/Data/Pagecounts-all-sites.


- ウィキペディアのページ数ファイルの構造と形式を学びましょう。
- ページ数のフォーマット用のパーサーを書くために、さまざまなテクニックを使う。
- 大きなファイルを便利な大きさのチャンクやフラグメントに分割して読むことができます。

> **WIKIPEDIA API**  
> Wikipedia は最近、生のページビューデータを補足し、英語版 Wikipedia で最も人気のあるページをより簡単に見つけることができる Pageview API (https://wikitech.wikimedia.org/wiki/Analytics/Pageview API ) を導入しました。もしあなたがWikipediaで最も人気のあるページを見つける必要のあるアプリケーションを書いているなら、代わりにAPIを使いたいと思うかもしれません。生のデータを手動で解析するのは効率的ではありませんが、この例は他のタスクにも適用できることがわかるでしょう。

このセクションの最後に、パーサーを並列化し、マルチコアCPUを搭載したシステムでより良いパフォーマンスを発揮させる方法も紹介します。

### 6.3.1 Understanding the Wikipedia page-counts format
ページ数の生データは、Wikipediaのこちら(https://dumps.wikimedia.org/other/pagecounts-all-sites/)からダウンロードすることができます。

このデータファイルは、特定の年や月に整理されています。例えば、2016年1月のページカウントデータは、https://dumps.wikimedia.org/other/pagecounts-all-sites/2016/2016-01/ で入手できます。

さらに、データは日ごと、時間ごとに細分化されています。先のURLの各ファイルは、1時間以内の訪問者を表しています。

ファイルサイズを小さくするため、すべてgzip圧縮されています。

以下のファイルをダウンロードし、解凍してください： https://dumps.wikimedia.org/other/pagecounts-all-sites/2016/2016-01/pagecounts-20160101-050000.gz

> **FOR WINDOWS USERS**  
> Windowsでは、7-Zipなどgzip形式のアーカイブを展開するためのアプリケーションをインストールする必要があります。

インターネットの速度により、ファイルのダウンロードに時間がかかる場合があります。抽出前は約92MB、抽出後は約428MBで、かなり大きなファイルです。

そのファイルをタイムリーに解析するためには、パーサーの効率化が必要です。

ファイルには、改行文字で区切られたテキスト行が入ります。各行のテキストは、スペースで区切られた以下の4つのフィールドで構成されています。

```
domain_code page_title count_views total_response_size
```
domain_codeには省略されたドメイン名が含まれます。たとえば、en.wikipedia.orgはenと省略されます。

page_title には、要求されたページのタイトルが含まれます (例: http://en.wikipedia.org/wiki/Dublin は Dublin)。

count_viewsには、1時間以内にそのページが閲覧された回数が含まれています。

最後に、total_response_sizeは、そのページへのリクエストによって転送されたバイト数である。

たとえば、次のような行を考えてみましょう
```
en Nim_(programming_language) 1 70231
```

これは、http://en.wikipedia.org/wiki/Nim_(programming_language) へのリクエストが1回あり、合計で70,231レスポンス・バイトを占めたことを意味します。

ダウンロードを依頼したファイルは、2016年1月の小さいファイルの1つです。これは、2016年1月1日午前4時（UTC ）から2016年1月1日午前5時（UTC ）までに訪問されたWikipediaページに関するデータが含まれています。

### 6.3.2 Parsing the Wikipedia page-counts format
ページ数の形式を解析する場合、さまざまなオプションがあります。

ここでは、正規表現、split手続き、parseutilsモジュールの3つの方法を使ってパーサーを実装する方法を紹介します。

#### PARSING USING REGULAR EXPRESSIONS
データを解析する一般的な方法として、正規表現（regexes）を使用する方法があります。正規表現は非常に人気があり、開発者は文字列を解析する必要がある場合、すぐに正規表現を使用するようになることがよくあります。

正規表現は決して、すべての解析の問題に対する魔法のような解決策ではありません。

たとえば、任意のHTMLを解析するための正規表現を書くことは事実上不可能です[^3] しかし、Wikipediaのページ数形式のような単純なデータ形式の解析には、正規表現がうまく機能します。

 [^3]: だからといって、人々が試みるのを止めることはできません: http://stackoverflow.com/questions/1732348/regex-match-open-tags-except-xhtml-self-contained-tags

> **LEARNING ABOUT REGULAR EXPRESSIONS**  
> 正規表現について深く説明することは、この章の範囲外です。もし、よく知らないのであれば、ネットで調べてみることをお勧めします。

Nim では、`re`モジュールにより正規表現がサポートされています。このモジュールは、文字列の解析と操作に正規表現を使うための手続きと型を定義しています。

> **WARNING: EXTERNAL DEPENDENCY**  
> re モジュールは不純物モジュールであり、外部の C ライブラリに依存していることを意味します。re の場合、C ライブラリは PCRE と呼ばれ、アプリケーションが正しく機能するためには、アプリケーションと一緒にインストールする必要があります。

まず、1行の解析に焦点を当てましょう。次のリストは、`re`モジュールを使ってそれを行う方法を示しています。

**Listing 6.10 Parsing data with the re module**
```nim
# reモジュールは、findプロシージャを定義しています。
import re

# 新しい正規表現パターンは、reコンストラクタで初期化されます。
let pattern = re"([^\s]+)\s([^\s]+)\s(\d+)\s(\d+)"

var line = "en Nim_(programming_language) 1 70231"
# この matches 配列は、line の部分文字列にマッチしたものを保持します。
var matches: array[4, string]
# findプロシージャは、正規表現中のサブグループで指定された
# 部分文字列にマッチするものを見つけるために使用されます。
# 部分文字列は、matches配列に格納されます。
let start = find(line, pattern, matches)
# 戻り値は，マッチした文字列の開始位置を示す。
# マッチしなかった場合は，-1が返される。
doAssert start == 0
# 最初のマッチンググループは "en "という部分文字列を捕らえ、
# 次に2番目のマッチンググループは "Nim_(programming_language) "を捕らえ、
# 以下同様である。
doAssert matches[0] == "en"
doAssert matches[1] == "Nim_(programming_language)"
doAssert matches[2] == "1"
doAssert matches[3] == "70231"
echo("Parsed successfully!")
```

> **WARNING: THE RE CONSTRUCTOR**  
> 正規表現の作成は、非常に手間がかかる作業です。同じ正規表現で複数の正規表現マッチを行う場合は、 `re`コンストラクタが返す値を必ず再利用するようにしましょう。

リスト6.10をlisting7.nimとして保存し、コンパイルして実行します。プログラムは正常にコンパイル・実行され、"Parsed successfully!"と表示されるはずです。

> **PCRE PROBLEMS**
`could not load: pcre.dll` のようなエラーでプログラムが終了する場合、PCREライブラリが不足しているため、インストールする必要があります。

正規表現で文字列を解析するコードは簡単です。正規表現の作り方を知っていれば、問題なく使えるはずです。`re`モジュールには、文字列を解析したり操作したりするための他のプロシージャも含まれています。例えば、`replace`プロシージャを使えば、マッチした部分文字列を置き換えることができます。詳しくは`re`モジュールのドキュメントを見てください(http://nim-lang.org/docs/re.html)

#### PARSING THE DATA MANUALLY USING SPLIT
また、さまざまな方法でデータを手動で解析することもできます。この方法には複数の利点がありますが、いくつかの欠点もあります。正規表現を使用する場合の最大の利点は、アプリケーションが PCRE ライブラリに依存しなくなることです。

また、手動でパースすることで、パース処理の制御が容易になります。場合によっては、最大のデメリットは、データを手動で解析するために多くのコードが必要になることです。

Wikipediaのページ数のような単純なデータ形式では、strutilsモジュールで定義されているsplit手続きを使うことができます。次のリストは、splitを使って `en Nim_(programming_language) 1 70231`をパースする方法を示しています。

**Listing 6.11 Parsing using split**
```nim
# strutilsモジュールでは、splitプロシージャを定義しています。
import strutils

var line = "en Nim_(programming_language) 1 70231"
# デフォルトでは、splitプロシージャは空白を見つけると文字列を分割します。
# 返されるシーケンスは @["en", "Nim_(programming_language)", "1", "70231"] となります。
var matches = line.split()
# 結果として得られるmatches変数の内容は、リスト6.10と同じです。
doAssert matches[0] == "en"
doAssert matches[1] == "Nim_(programming_language)"
doAssert matches[2] == "1"
doAssert matches[3] == "70231"
```

しかし、より複雑なデータ形式を扱う場合は、より柔軟性の高いソリューションが必要になります。文字列を解析する最も柔軟な方法は、whileループを使用して文字列内のすべての文字を繰り返し処理することです。この方法は非常に冗長ですが、HTMLのような複雑なデータ形式を解析する場合などには有効です。Nimのparseutilsモジュールはこのような方法でのパージングをより簡単にするための手続きを定義しています。

#### PARSING DATA MANUALLY USING PARSEUTILS
次のリストは、parseutilsモジュールを使用して、`en Nim_(programming_language) 1 70231`を解析する方法を示しています。

**Listing 6.12 Parsing using parseutils**
```nim
# parseUntil を定義している parseutils をインポートしています。
import parseutils

var line = "en Nim_(programming_language) 1 70231"

# 文字列中のプログラムの現在位置を記録するためのカウンタを定義する。
var i = 0
var domainCode = "" # 解析されたトークンが格納される文字列またはint型変数を定義します。
# line[i] == "" まで、lineの文字列から第2引数で指定された文字列にインデックスiから始まる文字をコピーする。
# 戻り値は取り込んだ文字数であり、iのインクリメントに使用される。
i.inc parseUntil(line, domainCode, {' '}, i)
i.inc # i をインクリメントすることで、ホワイトスペース文字をスキップする。
var pageTitle = ""  # 解析されたトークンが格納される文字列またはint型変数を定義します。
i.inc parseUntil(line, pageTitle, {' '}, i)
i.inc # i をインクリメントすることで、ホワイトスペース文字をスキップする。
var countViews = 0  # 解析されたトークンが格納される文字列またはint型変数を定義します。
# 行の文字列のインデックスiから始まるintをパースする。
# 解析されたintは、第2引数に格納される。戻り値は、取り込んだ文字数である。
i.inc parseInt(line, countViews, i)
i.inc # i をインクリメントすることで、ホワイトスペース文字をスキップする。
var totalSize = 0  # 解析されたトークンが格納される文字列またはint型変数を定義します。
# 行の文字列のインデックスiから始まるintをパースする。
# 解析されたintは、第2引数に格納される。戻り値は、取り込んだ文字数である。
i.inc parseInt(line, totalSize, i)

doAssert domainCode == "en"
doAssert pageTitle == "Nim_(programming_language)"
doAssert countViews == 1
doAssert totalSize == 70231
```

リスト6.12のコードは、前のリストよりはるかに複雑ですが、はるかに大きな柔軟性を可能にします。parseutilsモジュールには、パース処理に便利な他の手続きも多数定義されています。これらはほとんど、whileループの上の便利なラッパーです。例えば、`i.inc parseUntil(line, domainCode, {' '}, i)`に相当するコードは以下の通りです。

```nim
while line[i] != ' ':
  domainCode.add(line[i])
  i.inc
```
このパーサーの柔軟性により、コードは最後の2つのフィールドを1つのステップで解析して整数にすることができます。これは、最初にフィールドを分離してから整数をパースしなければならない代わりに、非効率的です。

まとめると、分割手続きは最もシンプルなアプローチですが、マッチを保持するためのシーケンスと新しい文字列を作成する必要があるため、`parseutils`より遅くなります。これに対して`parseutils`を使った解析コードでは、2つの文字列と 2つの整数を新たに作成するだけで、シーケンスの作成に関連するオーバーヘッドは発生しません。

正規表現の解析コードも`parseutils`よりも単純ですが、PCREへの依存に苦しみ、また`parseutils`のパーサーよりも遅くなります。

このため、`parseutils`のパーサーは少し複雑で冗長ですが、この使用例では最良の解決策となります。その速さは、pagecounts-20160101-050000ファイルの7,156,099行を解析するときに重宝されることでしょう。

### 6.3.3 Processing each line of a file efficiently
Wikipediaのページカウントのファイルは大きいです。各ファイルは約500MBで、約1000万行のデータを含んでいます。今回ダウンロードをお願いしたpagecounts-20160101-050000ファイルは 428 MB で、7,156,099 行のページカウントデータを含んでいます。

このファイルを効率的に解析するためには、ファイルを断片的に消費する必要があります。

ファイル全体をプログラムのメモリに読み込むと、少なくとも428MBのRAMを消費することになり、さまざまなオーバーヘッドが発生するため、実際の消費量ははるかに大きくなる可能性があります。

そのため、大きなファイルを読むときは、便利な大きさの小さな *断片（チャンク）* に分割するのがよい方法です。

#### USING AN ITERATOR TO READ A FILE IN FRAGMENTS
Nim は lines というイテレータを定義しており、ファイルの各行に対して反復処理を行ないます。このイテレータはファイルの全内容をプログラムのメモリにコピーする必要がないため、非常に効率的です。linesイテレータは、systemモジュールで定義されています。

次のリストは、linesイテレータを使用して、pagecounts-20160101-050000のファイルから行を読み取る方法を示しています。

**Listing 6.13 Iterating over each line in a file**
```nim
import os
proc readPageCounts(filename: string) =
  for line in filename.lines:
    echo(line)

when isMainModule:
  const file = "pagecounts-20160101-050000"
  let filename = getCurrentDir() / file
  readPageCounts(filename)
```
リスト6.13をsequential_counts.nimとして保存し、コンパイルして実行します。このプログラムは、ページカウントファイルの各行を表示するため、実行に1分程度かかります。Ctrl -Cで終了させることができます。実行中にメモリ使用量を確認することができますが、少ないはずです。

#### PARSING EACH LINE
これで、リスト6.12の解析コードをリスト6.13のコードに追加することができます。リスト6.14は、パーサーがリスト6.13にどのように統合されるかを示しており、変更点は太字で強調されています。

```nim
import os, parseutils
proc parse(
  line: string,
  domainCode,
  pageTitle: var string,
  countViews,
  totalSize: var int
) =
  var i = 0
  domainCode.setLen(0)
  i.inc parseUntil(line, domainCode, {' '}, i)
  i.inc
  pageTitle.setLen(0)
  i.inc parseUntil(line, pageTitle, {' '}, i)
  i.inc
  countViews = 0
  i.inc parseInt(line, countViews, i)
  i.inc
  totalSize = 0
  i.inc parseInt(line, totalSize, i)

proc readPageCounts(filename: string) =
  var domainCode = ""
  var pageTitle = ""
  var countViews = 0
  var totalSize = 0
  for line in filename.lines:
    parse(line, domainCode, pageTitle, countViews, totalSize)
    echo("Title: ", pageTitle)

when isMainModule:
  const file = "pagecounts-20160101-050000"
  let filename = getCurrentDir() / file
  readPageCounts(filename)
```

sequential_counts.nimのコードを、リスト6.14のコードに置き換えます。次のリストは、sequential_counts.nimからの出力の一部がどのように見えるかを示しています。

**Listing 6.15 The output of sequential_counts.nim**
```
...
Title: List_of_digital_terrestrial_television_channels_(UK)
Title: List_of_diglossic_regions
Title: List_of_dignitaries_at_the_state_funeral_of_John_F._Kennedy
Title: List_of_dimensionless_quantities
Title: List_of_diners
Title: List_of_dinosaur_genera
Title: List_of_dinosaur_specimens_with_nicknames
Title: List_of_dinosaurs
...
```

リスト6.14のコードでは、いくつかの最適化を施しています。まず、Nimアプリケーションの最大のスローダウンは、多くの場合、変数の割り当てと解放が多すぎることが原因です。解析手続きは解析されたトークンを返すこともできますが、そうすると繰り返しのたびに新しい文字列が割り当てられることになります。その代わり、この解析手続きは2つの文字列と2つのintへの変更可能な参照を受け取り、解析されたトークンで埋めます。この最適化なしでは解析に9.3秒かかるファイルが、最適化により7.8秒になりました。これは1.5秒の差です。

setLenの使用はもう一つの最適化です。これは文字列が再割り当てされることなく、再利用されることを保証するものです。解析手続きは少なくとも700万回実行されるので、小さな最適化が全体の実行速度に大きな利益をもたらします。

#### FINDING THE MOST POPULAR ARTICLE
解析コードが導入されたので、あとは英語版Wikipediaで最も人気のある記事を見つけるだけです。次のリストは完成した sequential_counts アプリケーションで、最新の変更点は太字で表示されています。

**Listing 6.16 The finished sequential_counts.nim**
```nim
import os, parseutils
proc parse(
  line: string,
  domainCode, pageTitle: var string,
  countViews, totalSize: var int
) =
  var i = 0
  domainCode.setLen(0)
  i.inc parseUntil(line, domainCode, {' '}, i)
  i.inc
  pageTitle.setLen(0)
  i.inc parseUntil(line, pageTitle, {' '}, i)
  i.inc
  countViews = 0
  i.inc parseInt(line, countViews, i)
  i.inc
  totalSize = 0
  i.inc parseInt(line, totalSize, i)

proc readPageCounts(filename: string) =
  var domainCode = ""
  var pageTitle = ""
  var countViews = 0
  var totalSize = 0
  var mostPopular = ("", "", 0, 0)
  for line in filename.lines:
    parse(line, domainCode, pageTitle, countViews, totalSize)
    if domainCode == "en" and countViews > mostPopular[2]:
      mostPopular = (domainCode, pageTitle, countViews, totalSize)
  echo("Most popular is: ", mostPopular)

when isMainModule:
  const file = "pagecounts-20160101-050000"
  let filename = getCurrentDir() / file
  readPageCounts(filename)
```

> **WARNING: RELEASE MODE**  
> Nim コンパイラに`-d:release`フラグを渡して、sequential_counts.nim をリリースモードでコンパイルしていることを確認してください。  
> このフラグがないと、アプリケーションの実行時間が著しく長くなります。

sequential_counts.nimの中身をリスト6.16のコードに置き換え、リリースモードでコンパイルして実行します。数秒後、次のような出力が表示されるはずです。

**Listing 6.17 Output for sequential_counts.nim**
```
Most popular is: (Field0: en, Field1: Main_Page, Field2: 271165, Field3: 4791
147476)
```
英語版ウィキペディアで最も人気のあるページは、実はメインページなのだ! これはとても理にかなっていて、後から考えても明らかですが、書いたコードを編集してもっと面白い統計を見つけるのは些細なことなのです。皆さんもsequential_counts.nimを編集して、データをいじってみてください。英語版 Wikipedia で最も人気のあるトップ 10 ページを見つけてみたり、異なるページ数のファイルをダウンロードして結果を比較したりすることができます。

これで、データを効率的に解析する方法についてよく理解できたはずです。

Nim アプリケーションで気をつけるべきボトルネックと、その修正方法を学んだことでしょう。次のステップは、このパーサーを並列化し、マルチコア CPU で実行時間がさらに短くなるようにすることです。

## 6.4 Parallelizing a parser
プログラムが並列であるためには、スレッドを利用する必要があります。前述したように、Nim でスレッドを作成するには、threads モジュールを使う方法と `threadpool`モジュールを使う方法の2通りがあります。どちらも動作しますが、このプログラムには`threadpool`モジュールがより適しています。
