Nim 環境構築
===
[戻る](../README.md)

Dockerfile
```
FROM nimlang/nim:alpine

RUN echo http://dl-cdn.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories
RUN apk update && \
    apk upgrade --no-cache && \
    apk add --no-cache \
        openssh-client \
        ca-certificates \
        openssl \
        pcre \
        bsd-compat-headers \
        lcov \
        sqlite mariadb-dev libpq && \
    rm /usr/lib/mysqld* -fr && rm /usr/bin/mysql* -fr && \
    update-ca-certificates
RUN git config --global http.sslVerify false
WORKDIR /root/project/
ENV PATH $PATH:/root/.nimble/bin
```

chosenim
```
apt install -y gcc
curl https://nim-lang.org/choosenim/init.sh -sSf | sh
echo 'export PATH=/root/.nimble/bin:$PATH' >> ~/.bashrc
source ~/.bashrc
```
