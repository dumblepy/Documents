
## 起動
```
dotnet run

# ホットリロード
dotnet watch
```

---

## MVCプロジェクト
[ASP.NET Core MVC の使用を開始する](https://docs.microsoft.com/ja-jp/aspnet/core/tutorials/first-mvc-app/start-mvc?view=aspnetcore-6.0&tabs=visual-studio-code)

RailsのようなMVC構造。エンドポイントがコントローラー名とメソッド名から自動で割り当てられる。

```
dotnet new mvc -o {ProjectName}
```

## Razor Pages Web アプリ
[チュートリアル: ASP.NET Core で Razor Pages Web アプリを作成する](https://docs.microsoft.com/ja-jp/aspnet/core/tutorials/razor-pages/?view=aspnetcore-6.0)  
[ASP.NET Core での Razor ページの概要](https://docs.microsoft.com/ja-jp/aspnet/core/razor-pages/?view=aspnetcore-6.0&tabs=visual-studio-code)  
[.NET 6でASP.NET CoreのRazor Pagesアプリケーションを理解しよう](https://codezine.jp/article/detail/15824)  

MVVM構造を実現する。ルーティングはファイル構造から自動で割り当てられ、NextJSみたいな感じ。

```
dotnet new webapp -o {ProjectName}
```

## WebAPIアプリ
[ASP.NET Core を使って Web API を作成する](https://learn.microsoft.com/ja-jp/aspnet/core/web-api/?view=aspnetcore-6.0)

```
dotnet new webapi -o {ProjectName}
cd {ProjectName}
code . -r # VSCodeをプロジェクトディレクトリで開き直す
dotnet watch
```

### The namespace *namespace* Already contains a definition for *Controller*のエラーが出る時

https://github.com/OmniSharp/omnisharp-vscode/issues/1402#issuecomment-294967081
```
Crtl + Shift + p
>Restart OmniSharp
```

### portの指定
Properties/launchSettings.jsonを編集する
```json
{
  "profiles": {
    "api": {
      "commandName": "Project",
      "dotnetRunMessages": true,
      "launchBrowser": true,
      "launchUrl": "swagger",
      // "applicationUrl": "https://localhost:7248;http://localhost:5101",
      "applicationUrl": "http://0.0.0.0:5000",
      "environmentVariables": {
        "ASPNETCORE_ENVIRONMENT": "Development"
      }
    },
}
```

### Viewを使う
https://stackoverflow.com/questions/63584319/how-to-add-views-to-asp-net-core-web-api

Program.cs
```cs
// builder.Services.AddControllers();
builder.Services.AddControllersWithViews();
```

XxController.cs
```cs
using Microsoft.AspNetCore.Mvc;

namespace Project.Controllers;

[Route("")]
public class XxController : Controller // BaseControllerからControllerに変える
{
  [HttpGet("")]
  public IActionResult Index()
  {
    return View("Views/Xx/Index.cshtml");
  }
}
```

### コントローラー
plaintextを返す
```cs
[HttpGet("plaintext")]
public string Plaintext()
{
  return "plaintext";
}
```

jsonを返す
```cs
[HttpGet("json")]
public object Json()
{
  return new {message = "Hello, World!"};
}
```

### printデバッグ
https://stackoverflow.com/questions/36516768/console-write-in-net-core

```cs
[HttpGet("{id}")]
public string Show(int id)
{
  Console.WriteLine("==== show");
  Console.WriteLine(id.GetType());
  Console.WriteLine(id);
  return $"api get test users {id}";
}
```

### リクエスト
#### URLパラメータ

`http://localhost:5000/api/test-users/1`
```cs
[HttpGet("{id}/")]
public object Show(int id){
  Console.WriteLine("show");
  return new {message = $"show {id}"};
}
```

#### リクエストボディ

`http://localhost:5000/api/test-users`
```json
{
  "email": "aaa@mail.com"
}
```

```cs
public class CreateRequest{
  public string email {get; set;}
}

[HttpPost()]
public IActionResult Create([FromBody] CreateRequest request){
  Console.WriteLine(request.email);
  return new StatusCodeResult(200);
}
```

### クエリパラメータ

`http://localhost:5000/api/test-users?email=aaa@mail.com`

```cs
[HttpGet("search/")]
public string Search(string email){
  Console.WriteLine(email);
  return email;
}
```

### マイグレーション
[C# EntityFramework マイグレーションまとめ](https://qiita.com/conpass/items/adb0c99c5af6f48d986e)

#### Entity Framework
パッケージインストール
```sh
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet tool install --global dotnet-ef
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL

# dotnet add package Npgsql
```

コンテキストを作成
```sh
dotnet ef dbcontext scaffold "Server=db;Database=database;Username=user;Password=pass" Npgsql.EntityFrameworkCore.PostgreSQL -o config
```

スキーマを作成
```sh
mkdir Entities
touch TestUser.cs
```

[新しいデータベースの Code First](https://learn.microsoft.com/ja-jp/ef/ef6/modeling/code-first/workflows/new-database)  
[Code First Data Annotations (Code First のデータ注釈)](https://learn.microsoft.com/ja-jp/ef/ef6/modeling/code-first/data-annotations)
```cs
using System;
using Microsoft.EntityFrameworkCore;
// アノテーションを有効にする
using System.ComponentModel.DataAnnotations;
// テーブル名・カラム名の型アノテーション
using System.ComponentModel.DataAnnotations.Schema;

namespace MigrationProject.Entities
{
  
  [Table("test_useer")]
  public class TestUser
  {
    [Column("id")]
    public Int64 Id { get; set; }

    [Column("email")]
    [Required]
    public string Email { get; set; }
  }
}
```

コンテキストでスキーマを呼ぶ

config/databaseContext.cs
```cs
using MigrationProject.Entities;

namespace MigrationProject.config
{
    public partial class databaseContext : DbContext
    {
        public DbSet<TestUser> TestUser {get; set;}
    }
}
```

マイグレーションを作成
```sh
dotnet ef migrations add InitTestUsersTable
```
`Migrations`の中にファイルが作成される
