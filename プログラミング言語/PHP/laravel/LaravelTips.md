Laravel Tips
===

## 実行させたSQLのログを取りたい時
```php
// 確認したいSQLの前にこれを仕込んで
DB::enableQueryLog();

// 確認したいSQL
$articles = Article::all();

// ログ出力
logger(DB::getQueryLog());
```

## timestampsの2036年問題解決
```php
$table->dateTime('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
$table->dateTime('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
```
