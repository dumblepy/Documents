Laravelモデル
===



## マイグレーション

### マイグレーションファイルを作るコマンド
```
php artisan make:migration create_sample_table
```

## ORM

### DBから取り出したデータからさらに特定のカラムだけ取り出したいとき

user

|id|name|email|
|---|---|---|
|1|taro|taro@gmail.com|
|2|jiro|jiro@gmail.com|
|3|saburo|saburo@gmail.com|

#### get()を使う時
```
$taroName = 'taro';
$taro = DB::table('user')
  ->where('name', $taroName)
  ->get();

$taroMail = json_decode(json_encode($taro))[0]->email;
```

#### first()を使う時
```
$taroName = 'taro';
$taro = DB::table('user')
  ->where('name', $taroName)
  ->first();

$taroMail = $taro->email;
```

### WHERE(a = a' OR b = b')
[【PHP入門】LaravelでWHERE句のANDやORをネストする方法](http://programming-study.com/technology/php-laravel/)
```
\DB::table('table_name')
->where(function($q){
    $q->where('a', 'a')
    ->orWhere('b', 'b'')
})
->get();
```

### WHERE(a = a' OR b = b')の中で変数を使うとき
```
\DB::table('table_name')
->where(function($q) use($value) {
    $q->where('a', $value)
    ->orWhere('b', $value)
})
->get();
```

## Tips
### ソースでテーブルのschemaを表示
```
$columns = \Schema::getColumnListing('table_name');
\Log::debug($columns);
```