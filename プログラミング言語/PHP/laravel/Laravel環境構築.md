Laravel環境構築
===
[戻る](../../README.md)

## ローカル
```
# PHPインストール
anyenv install phpenv

sudo apt install bison re2c libxml2-dev libssl-dev libbz2-dev libcurl4-openssl-dev libjpeg-dev libpng-dev libreadline-dev libtidy-dev libxslt-dev libzip-dev autoconf

git clone git://github.com/CHH/php-build.git $HOME/.phpenv/plugins/php-build

phpenv install 7.2.13

# composerインストール
apt install -y composer
# 確認
composer

# Laravelインストール
composer global require "laravel/installer"

# laravelコマンドにパスを通す
vi ~/.bashrc

export PATH="$PATH:$HOME/.config/composer/vendor/bin"

# Laravelプロジェクト作成
laravel new project
```

## Docker
[Dockerhub PHP](https://hub.docker.com/_/php)

docker-compose.yaml
```
version: '3'
services:
  laravel:
    build: .
    container_name: laravel
    tty: true
    volumes:
      - ./project/:/home/project/
    ports:
      - "8000:8000"
```

Dockerfile
```
#FROM ubuntu:latest
FROM php:7.2.16-cli-stretch
WORKDIR /home
SHELL ["/bin/bash", "-c"]
COPY ./install.sh install.sh
RUN chmod 755 install.sh
RUN bash install.sh
EXPOSE 8000
```

install.sh
```
apt upgrade
apt update -y
apt install git curl wget gcc make libxml2 libxml2-dev libssl-dev pkg-config zlib1g-dev libbz2-dev \
libjpeg-dev libpng-dev libreadline-dev libtidy-dev libcurl4-openssl-dev libxslt1.1 libxslt-dev \
autoconf automake libtool autoconf-doc libtool-doc vim zip unzip -y

# mysqlインストール
apt install mysql-server mysql-client -y
service mysql start

# composerインストール
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer
```

### コマンド
```
docker-compose build
docker-compose up -d
docker-compose exec laravel bash
```

コンテナ内
```
# mysqlを起動
service mysql start

cd project
php artisan serve --host 0.0.0.0
```

localhost:8000にアクセスするとLaravelが開く


```
composer create-project laravel/laravel .
```