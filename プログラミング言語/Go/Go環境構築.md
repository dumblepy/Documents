Go環境構築
===
[戻る](../README.md)

## anyenv内にgoenvを構築
goenvをインストール
```
anyenv install goenv  
exec $SHELL -l  
```

goenvがインストールできるバージョン一覧を表示  
```
goenv install -l
```

数が多すぎて画面に収まらない場合はファイル出力
```
bash goenv install -l > goV.txt
vi goV.txt
```

Goをインストール
```
goenv install 1.10.4
goenv install 1.11.1
```

インストールされたGoの一覧を表示
```
goenv versions
```

バージョンを設定
```
#グローバル
pyenv globa x.x.x

#ローカル
pyenv local x.x.x
```

確認
```
goenv versions
which go
go version
```

## DirenvでGopathを設定する
環境変数を設定したいディレクトリにcdし、
```
direnv edit .
```

vimが開くので以下を記入
```
export GOROOT=$(go env GOROOT)
export GOPATH=$(pwd)
```

```
direnv allow .
```
を実行して有効化する
プロジェクトディレクトリ/  がGOPATHになる

## delveのインストール
Go専用の強力なデバッガです。 初心者の頃に挙動を追ったり、ディープな問題を追跡したりするのに使います。

```
go get github.com/derekparker/delve/cmd/dlv
```

これの使い方は後述のVisualStudioCodeなどの高機能なエディタと組み合わせて使うのがオススメです。

## Gopkgsなどインストール
```
go get -u -v github.com/ramya-rao-a/go-outline
go get -u -v github.com/acroca/go-symbols
go get -u -v github.com/mdempsky/gocode
go get -u -v github.com/rogpeppe/godef
go get -u -v golang.org/x/tools/cmd/godoc
go get -u -v github.com/zmb3/gogetdoc
go get -u -v golang.org/x/lint/golint
go get -u -v github.com/fatih/gomodifytags
go get -u -v golang.org/x/tools/cmd/gorename
go get -u -v sourcegraph.com/sqs/goreturns
go get -u -v golang.org/x/tools/cmd/goimports
go get -u -v github.com/cweill/gotests/...
go get -u -v golang.org/x/tools/cmd/guru
go get -u -v github.com/josharian/impl
go get -u -v github.com/haya14busa/goplay/cmd/goplay
go get -u -v github.com/uudashr/gopkgs/cmd/gopkgs
go get -u -v github.com/davidrjenni/reftools/cmd/fillstruct
go get -u -v github.com/alecthomas/gometalinter
bin/gometalinter --install
```
