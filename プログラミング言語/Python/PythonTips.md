Python Tips
===
[戻る](../README.md)

### テーブル名とモデル名の相互変換

[言葉の単数形と複数形の変換](https://qiita.com/podhmo/items/bba7b2355266dab72996)

#### install
```
pip install inflection
```

モデル名→テーブル名
```
import inflection
model_name = 'Post'
table_name = inflection.tableize(model_name)
table_name
> posts
```

テーブル名→モデル名
```
import inflection as i
table_name = 'posts'
model_name = i.camelize(i.singularize(table_name))
model_name
> posts
```
camelize…キャメルケースにする
singularize…単数形にする

### クラスが持つメソッドを一覧表示
```
import inspect
user = User()
inspect.getmembers(user)
```

### インスタンスのプロパティに渡す値をdictからセットしたい時
```
user = User()
user.name = 'taro'
user.save()
```
これをdictから行いたい

```
user = User()
arr = {'name': 'taro', 'age': 20, 'nation': 'Japan'}

for key, value in arr.items():
    setattr(user, key, value)
user.save()
```