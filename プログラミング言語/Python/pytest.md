pytest
===
[戻る](../README.md)

```
python -m pytest tests/test_admin/info.py --cov=admin --cov-report=html
python -m pytest {対象テストファイル} --cov={テスト対象ソースディレクトリ} --cov-report={レポート出力形式}
```