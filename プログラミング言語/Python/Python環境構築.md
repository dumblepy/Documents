Python環境構築
===
[戻る](../README.md)

## anyenv内にpyenvを構築

必要モジュールをインストール
```
apt install -y gcc make python-dev python3-dev build-essential libbz2-dev libdb-dev libreadline-dev libffi-dev libgdbm-dev liblzma-dev libncurses5 libncursesw5-dev libsqlite3-dev libssl-dev zlib1g-dev uuid-dev tk-dev
```

pyenvをインストール
```
anyenv install pyenv  
exec $SHELL -l  
```

pyenvがインストールできるバージョン一覧を表示  
```
pyenv install -l
```

数が多すぎて画面に収まらない場合はファイル出力
```
bash pyenv install -l > pythonV.txt
vi pythonV.txt
```

pythonをインストール
```
pyenv install 3.6.6
```

インストールされたpythonの一覧を表示
```
pyenv versions
```

バージョンを設定
```
#グローバル
pyenv globa x.x.x

#ローカル
pyenv local x.x.x
```

確認
```
pyenv versions
which python
python -V
which pip
pip -V
```

## venv
プロジェクトフォルダ内にvenvを作る
```
python -m venv .venv
```

有効化
```
# UNIX
source ./venv/bin/activate

#Windows Power Shell
./venv/Scrips/activate.ps1

#Windows cmd
./venv/Scrips/activate.bat
```

## pipenv
pipenvをインストール
```
pip install pipenv
```

Pipfileを生成する
```
#ユーザーディレクトリ以下にパッケージをインストール
pipenv install

#プロジェクトディレクトリ以下にパッケージをインストール
set PIPENV_VENV_IN_PROJECT=true
pipenv install
  or
env PIPENV_VENV_IN_PROJECT=true pipenv install
```

パッケージをインストール
```
pipenv install xxx
```

### Macでopensslのエラーが出る時
```
brew upgrade openssl

export CPPFLAGS=-I/usr/local/opt/openssl/include
export LDFLAGS=-L/usr/local/opt/openssl/lib
```

### pip18.1でエラーが起きる時
```
pip install -U pip==18.0
```

## pip更新
### 更新可能なパッケージ一覧表示
```
pip list -o
```

### 更新可能なパッケージの一括更新
```
pip list -o | awk '{print $1}' | xargs pip install -U
```

ソースからビルド
===
ソースをダウンロード  
https://www.python.org/downloads/source/
```
wget https://www.python.org/ftp/python/3.7.2/Python-3.7.2.tar.xz
```

解凍
```
tar xf Python-3.7.2.tar.xz
```

インストール
```
cd Python-3.7.2.tar.xz
./configure --enable-optimizations
make -j 8
make altinstall #古いものと差し替える
```