Python規約
===
[戻る](../README.md)

## ソースレビューの観点
### ループ処理
インデックス付きでループできているか

```
l = ['a','b','c','d']

for i, v in enumurate(l):
    print(i)
    print(v)

d = {'a': 'あ', 'b': 'い', 'c': 'う', 'd': 'え'}

for i, v in d.items():
    print(i)
    print(v)
```

リスト内包表記を使っているか

```
arr = ['a','b','c','d']
check = ['a', 'c']

n = [v + '_checked' in for v in arr if v in check]
```
if文の検索条件が1つ以上複雑になる時は素直にif文を書く

### 正しい標準ライブラリの選択を