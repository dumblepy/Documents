DjangoとVueの連携
===
[戻る](../../README.md)

Djangoのtemplateと、CDN読み込みしたVueを共存させる。  
axiosとAPIを使わずVueに変数を送る。

## settings.py
```
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")
```

## project/templates/base.html
```
{% load static %}
<html>

<head>
  <title>Django</title>
  <link href='//fonts.googleapis.com/css?family=Lobster&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  <link href="https://cdn.jsdelivr.net/npm/vuetify@latest/dist/vuetify.min.css" rel="stylesheet">
  <link rel="stylesheet" href="{% static 'style.css' %}">
</head>

<body>
  <script src="https://cdn.jsdelivr.net/npm/vue@latest/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vuetify@latest/dist/vuetify.js"></script>
  {% block content %}
  {% endblock %}
  </div>
</body>

</html>
```

## views.py
```
class AppViews:
    def index(request):
        users = json.dumps([
            {'id': 1, 'name': 'user1'},
            {'id': 2, 'name': 'user2'},
            {'id': 3, 'name': 'user3'},
            {'id': 4, 'name': 'user4'},
        ])
        return render(request, 'app/index.html', {'users': users})
```

## project/templates/app/index.html
```
{% extends 'base.html' %}
{% load static %}

{% block content %}
<div id="app_index">
    <h1>index</h1>
  <v-app>
    <p>{|users|}</p>
  </v-app>
</div>

<script>
  new Vue({
    el: '#app_index',
    vuetify: new Vuetify(),
    delimiters: ['{|', '|}'],
    data: {
      users: JSON.parse('{{users|safe}}')
    }
  })
</script>
{% endblock %}
```