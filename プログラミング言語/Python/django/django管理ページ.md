管理ページ
===
[戻る](../../README.md)

[Django Adminの紹介](https://docs.djangoproject.com/ja/2.1/intro/tutorial02/#introducing-the-django-admin)  

## 設定
Person

|name|birth_year|age|
|---|---|---|

Book

|title|article|
|---|---|

/project/app/admin.py
```
from django.contrib import admin
from .models import Person, Book

admin.site.register(Person)
admin.site.register(Book)
```

### 特定のカラムを管理ページで表示しないようにする時
```
class BookAdmin(admin.ModelAdmin):
    exclude = ['article']
admin.site.register(Book, BookAdmin)
```

### 管理ページで編集不可(readonly)のカラムを設定したい時
```
class PersonAdmin(admin.ModelAdmin):
    readonly_fields = ['age']
admin.site.register(Person, PersonAdmin)
```

## アクセス
管理ユーザーを作成
```
python manage.py createsuperuser
```
サーバーを起動してアクセス
```
python manage.py runserver
```
[http://127.0.0.1:8000/admin/](http://127.0.0.1:8000/admin/)  