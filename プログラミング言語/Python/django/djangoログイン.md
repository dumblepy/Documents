ログイン
===
[戻る](../../README.md)

## Model

```
from django.db import models

from django.contrib.auth.hashers import make_password
from django.utils import timezone
import hashlib

# Create your models here.

class User(models.Model):
    def __str__(self):
        return str(self.name)

    name = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    email = models.CharField(max_length=255, blank=True, null=True)
    tel = models.CharField(max_length=255, blank=True, null=True)
    is_studio = models.BooleanField(default=0)

    class Meta:
        db_table = 'users'
        verbose_name_plural = 'user'

    def save(self, *args, **kwargs):
        self.password = make_password(self.password) #パスワード暗号化
        super().save(*args, **kwargs)


class LoginToken(models.Model):
    def __str__(self):
        # メールアドレスとアクセス日時、トークンが見えるようにする
        dt = timezone.localtime(self.access_datetime).strftime("%Y/%m/%d %H:%M:%S")
        return self.user.email + '(' + dt + ') - ' + self.token

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=40) #トークン
    access_datetime = models.DateTimeField() #アクセス日時

    class Meta:
        db_table = 'tokens'
        verbose_name_plural = 'token'

    @staticmethod
    def create(user: User):
        # ユーザの既存のトークンを取得
        if LoginToken.objects.filter(user=user).exists():
            # トークンが既に存在している場合は削除する
            LoginToken.objects.get(user=user).delete()

        # トークン生成（メールアドレス + パスワード + システム日付のハッシュ値とする）
        dt = timezone.now()
        str = user.email + user.password + dt.strftime('%Y%m%d%H%M%S%f')
        hash = hashlib.sha1(str.encode('utf-8')).hexdigest()    # utf-8でエンコードしないとエラーになる

        # トークンをデータベースに追加
        token = LoginToken.objects.create(
            user = user,
            token = hash,
            access_datetime = dt)

        return token
```

## views

```
from django.contrib.auth.hashers import make_password, check_password
from django.core import serializers
from django.http import HttpResponse, JsonResponse, HttpResponseNotFound
from rest_framework.views import APIView
import json

from ..models import User, LoginToken
from ..service import get_from_json_post

class Login(APIView):
    def post(self, request, format=None):
        # リクエストボディのJSONを読み込み、メールアドレス、パスワードを取得
        try:
            email = get_from_json_post(request, 'email')
            password = get_from_json_post(request, 'password')
        except:
            # JSONの読み込みに失敗
            return JsonResponse({'value': {'message': 'Post data injustice'}}, status=400)

        # メールアドレスからユーザを取得
        if not User.objects.filter(email=email).exists():
            # 存在しない場合は403を返却
            return JsonResponse({'value': {'message': 'Login failure mail.'}}, status=403)

        user = User.objects.get(email=email)

        # パスワードチェック
        if not check_password(password, user.password):
            # チェックエラー
            return JsonResponse({'value': {'message': 'Login failure password.'}}, status=403)

        # ログインOKの場合は、トークンを生成
        token = LoginToken.create(user)

        # トークンを返却
        return JsonResponse({'value': {'token': token.token}})

```