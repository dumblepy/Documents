環境構築
===
[戻る](../../README.md)

## インストール
[クイックインストールガイド](https://docs.djangoproject.com/ja/2.1/intro/install/)

```
pipenv install django
```
プロジェクト作成
```
django-admin startproject project
```

アプリケーション作成
```
cd project
python manage.py startapp app
```

## 環境設定

/project/project/setting.py
```
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app' #←追加
]

LANGUAGE_CODE = 'ja'

TIME_ZONE = 'Asia/Tokyo'
```

### MySQLの設定
[いつもDjangoでMySQL(utf8mb4)を利用するときに行っているDjangoのDATABASE設定](https://qiita.com/shirakiya/items/71861325b2c8988979a2)

/project/project/setting.py
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'DATABASENAME',
        'USER': 'appuser',
        'PASSWORD': 'Password!',
        'HOST': '127.0.0.1',
        'PORT': '3306',
         'OPTIONS': {
            'charset': 'utf8mb4',
            'sql_mode': 'TRADITIONAL,NO_AUTO_VALUE_ON_ZERO,ONLY_FULL_GROUP_BY',
        },
    }
}
```

/project/manage.py  
追加する
```
import pymysql
pymysql.install_as_MySQLdb()
```

/project/project/wsgi.py  
追加する
```
import pymysql
pymysql.install_as_MySQLdb()
```