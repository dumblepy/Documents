モデル
===
[戻る](../../README.md)

DBのスキーマ(=構造)をクラスに記す  
モデル名は単数形にする

## 設定
/project/app/models.py
```
from django.db import models

class Book(models.Model):
    def __str__(self):
        return str(self.title)

    title = models.CharField(
        max_length=255
    )
```

### null許可
adminページ内での未入力状態は、nullではなく空文字と認識されるため、「blank=True」も設定しないといけない  
[Djangoモデルフィールドのnullとblankの違いを理解する](https://www.djangobrothers.com/blogs/django_null_blank/)  
```
class Book(models.Model):
    article = models.TextField(
        null=True,
        blank=True
    )
```

### 単数形と複数形
persopn - peopleのように、単純に後ろに「s」を付ければいいわけではない英単語の時は、「verbose_name_plural」で設定する
```
class Person(models.Model):
    class Meta:
        verbose_name_plural = 'people'
```

### インデックスを張る
```
class Person(models.Model):
    class Meta:
        indexes = [
            models.Index(fields=['name', 'age'])
        ]
```

### 管理ページのフォームから入力した値をサーバー側で操作したい時
[定義済みのモデルメソッドをオーバーライドする](https://docs.djangoproject.com/ja/2.1/topics/db/models/#overriding-predefined-model-methods)

```
class Person(models.Model):
    birth_year = models.IntegerField(
        null=True,
        blank=True
    )
    age = models.IntegerField(
        null=True,
        blank=True
    )

    def save(self, *args, **kwargs):
        import datetime
        this_year = datetime.date.today().year
        self.age = this_year - self.birth_year
        super().save(*args, **kwargs)
```
ageはbirth_yearの入力値を元に計算されて自動入力される

## マイグレーション
```
python manage.py makemigrations
python manage.py migrate
```

マイグレーションでどんなsqlが発行されるか確認する時
```
python manage.py sqlmigrate app 0001
```

## DBとマイグレーションをリセットしたい時
/project/app/migrations/の__init__.py以外を削除  

databaseを作り直す
```
drop database dbname
create database dbname
```

再度マイグレーションしてadminユーザーを作る
```
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
```

# シード

/project/app/fixtures/seed.yaml
```
- model: app.user
  pk: 1
  fields:
    name: ユーザー1
    password: user1
    email:  user1@gmail.com
    tel: "08011112222"
```

```
python manage.py loaddata seed.yaml
```

## 初期データ中のパスワードを自動で暗号化する

/project/app/management/commands/initdata.py
```
from django.core.management import BaseCommand, call_command
#from django.contrib.auth.models import User
from app.models import User
# from yourapp.models import User # if you have a custom user


class Command(BaseCommand):
    # htlpで表示する説明文
    help = "DEV COMMAND: Fill databasse with a set of data for testing purposes"

    def handle(self, *args, **options):
        call_command('loaddata','seed.yaml')
        # Fix the passwords of fixtures
        for user in User.objects.all():
            user.save()

```

manage.pyにコマンドを追加できる
```
python manage.py inittada
```

/project/app/models.py
```
from django.db import models
from django.contrib.auth.hashers import make_password

class User(models.Model):
    def __str__(self):
        return str(self.name)

    name = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    email = models.CharField(max_length=255, blank=True, null=True)
    tel = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'users'
        verbose_name_plural = 'user'

    def save(self, *args, **kwargs):
        self.password = make_password(self.password) #パスワード暗号化
        super().save(*args, **kwargs)

```