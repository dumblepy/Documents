ダッシュボード
===
[戻る](../../README.md)

[Masonite Dashboard](https://docs.masoniteproject.com/official-packages/masonite-dashboard)

インストール
```
pipenv install masonite-dashboard
```
config/providers.pyに追加
```
from dashboard.providers import DashboardProvider
​
PROVIDERS = [
    ...
    DashboardProvider,
]
```
/route/web.pyに追加
```
from dashboard.routes import routes as DashboardRoutes
​
ROUTES = [
    ...
    DashboardRoutes(),
    ...
]
```
Userテーブルにis_admin列を追加
```
craft migration add_is_admin_to_users --table users

with self.schema.table('users') as table:
    table.integer('is_admin').nullable()

craft migrate

update users set is_admin=true where name = 'username';
```