モデル
===
[戻る](../../README.md)

## モデルを追加
モデル名は頭文字大文字の単数形。デフォルトで複数形になってテーブルと結び付けられる   
```
craft model Tab
```
/app/Tab.pyが作られる  

```
''' A Tab Database Model '''
from config.database import Model

class Tab(Model):
    pass
```

## 入力可能なカラム
```
class Tab(Model):
    __fillable__ = ['tab', 'tab_en']
```

## 外部キー
```
from orator.orm import belongs_to

class Tab(Model):
    @belongs_to('author_id', 'id')
    def author(self):
        from app.User import User
        return User
```