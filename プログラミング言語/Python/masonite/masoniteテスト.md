テスト
===
[戻る](../../README.md)

[Masonite Test](https://docs.masoniteproject.com/useful-features/testing)

## 単体テスト
/project/tests/unit/test_works.pyに書く

### 実行コマンド
```
python -m pytest test/unit/
```

### ルーティング
```
from masonite.testing import UnitTest

class TestRouting(UnitTest):
    def test_route_exist(self):
        assert self.route('/')
```

### レスポンスステータス
```
from masonite.testing import UnitTest

class TestRouting(UnitTest):
    def test_status(self):
        assert self.route('/').status('302 Found') #リダイレクト
        assert self.route('/blog').status('200 OK')
        assert self.route('/blog').ok()
```

### コントローラー
```
from masonite.testing import UnitTest

class TestRouting(UnitTest):
    def test_has_controller(self):
        assert self.route(
            '/blog/api//blog/api/getToppage').has_controller(BlogController)
```

### モデル
```
from masonite.testing import UnitTest
from app.models.Toppage import Toppage

class TestApi(UnitTest):
    def setup_method(self):
        super().setup_method()
        self.model = Toppage.find(1)

        _json = self.json('/blog/api/getToppage', None, method="GET").container.make('Response')
        self.response = json.loads(_json)

    def test_model(self):
        assert self.model.title == self.response['value']['toppage']['title']
```

## URLパラメーター
```
from masonite.testing import UnitTest
from app.models.Article import Article

class TestApi(UnitTest):
    def setup_method(self):
        self.model = Article.find(1)
        self.response = self.json('/blog/api/article/1', None, method='GET').container.make('Response')

    def test_api(self):
        assert self.route('/blog/api/article/@article_id:int')
        assert self.json('/blog/api/article/1').ok()
        assert self.model.title = self.response['value']['article']['title']
```

## コードカバレッジ
プラグインをインストール
```
pip install pytest-cov
```

カバレッジレポート出力
```
python -m pytest tests/unit/ -v --cov=app/
```

HTMLでカバレッジレポートを出力
```
python -m pytest -v  tests/unit/ --cov=app/ --cov-report=html
```
/project/htmlcov/index.html が出力される