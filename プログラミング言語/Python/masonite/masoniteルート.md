ルート
===
[戻る](../../README.md)

[Routing](https://docs.masoniteproject.com/the-basics/routing)

- /route/web.py  
- /route/api.py
```
from masonite.routes import Get
from masonite.helpers.routes import get
from app.http.controllers.BlogController import BlogController

ROUTES = [
    Get().route('/blog', BlogController.blog),
    Get().route('/blog', 'BlogController@blog'),
    get('/blog', 'BlogController@blog'),

    RouteGroup([
        Get().route('', BlogController.blog),
        Get().route('', 'BlogController@blog'),
        get('', 'BlogController@blog'),
    ],
    prefix='/blog'),
]
```

## HTTPメソッド
```
from masonite.routes import Get, Post, Put, Patch, Delete
from masonite.helpers.routes import get, post, put, patch, delete
```