マイグレーション
===
[戻る](../../README.md)

マイグレーションファイルを元に、実際のテーブルを作る。  
例：tabsテーブルを作る  
```
craft migration create_tabs_table --create tabs
```

/database/migrations/yyyy_mm_dd_hh_xxxxxx_create_tabs_table.pyを開いて編集  

|英語名|説明|型|null許可|外部キー|
|---|---|---|---|---|
|id|primary|int|x|---|
|tab|タブ名|varchar|x|---|
|tab_en|タブ英語名|varchar|o|---|

```
from orator.migrations import Migration


class CreateTabsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('tabs') as table:
            table.increments('id') #プライマリー
            table.string('tab')
            table.string('tab_en').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('tabs')
```

マイグレーション実行
```
craft migrate
```

確認
```
mysql -h 127.0.0.1 -uroot -pPassword!
```
```
use blog;

show tabses;
+----------------+
| Tables_in_blog |
+----------------+
| migrations     |
| tabs           |
| users          |
+----------------+

show columns from tabs;
+--------+------------------+------+-----+---------+----------------+
| Field  | Type             | Null | Key | Default | Extra          |
+--------+------------------+------+-----+---------+----------------+
| id     | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
| tab    | varchar(255)     | NO   |     | NULL    |                |
| tab_en | varchar(255)     | YES  |     | NULL    |                |
+--------+------------------+------+-----+---------+----------------+
```

使用可能な関数の一覧  
[Schema Builder](https://orator-orm.com/docs/0.9/schema_builder.html#adding-columns)  