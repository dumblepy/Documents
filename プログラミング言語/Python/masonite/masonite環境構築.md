masonite環境構築
===
[戻る](../../README.md)

## インストール
[masonite document introduction](https://docs.masoniteproject.com/prologue/introduction-and-installation)

```
pipenv install masonite-cli
source .venv/bin/activate
```

プロジェクト作成
```
craft new xxx
```

masoniteの依存パッケージをインストール
```
cd xxx
craft install
```

## auth
### mysql環境構築
```
docker run --name mariadb -e MYSQL_ROOT_PASSWORD=Password! -p 3306:3306 -d mariadb:latest
mysql -h 127.0.0.1 -uroot -pPassword!
create database DATABASENAME;
create user appuser@"%" identified by "Password!";
grant all on DATABASENAME.* to appuser@"%" identified by "Password!";
mysql -h 127.0.0.1 -uappuser -pPassword!
```

### .envを編集
```
DB_DRIVER=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=DATABASENAME
DB_USERNAME=appuser
DB_PASSWORD=Password!
```

### craftコマンド実行
```
craft auth
craft migrate
```
[http://localhost:8000/register](http://localhost:8000/register)にアクセス