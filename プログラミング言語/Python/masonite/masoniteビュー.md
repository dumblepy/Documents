ビュー
===
[戻る](../../README.md)

## ビューを作るコマンド
```
craft view index
```
/resources/templates/index.htmlが作られる

すでにフォルダが存在していれば
```
craft view blog/index
```
/resources/templates/blog/index.htmlが作られる

## 渡された変数を表示する  
コントローラー
```
def index(self, View):
    arg = 'aaaaa'
    return View('index', {'arg': arg})
```
ビュー
```
<p>{{arg}}</p>
```

## 表示する変数に初期値を設定する
```
<p>{{arg or '初期値'}}</p>
```

## if文
```
{% if arg %}
    <p>あり</p>
{% else %}
    <p>なし</p>
{% endif %}
```

## ループ
コントローラー
```
def index(self, View):
    lists = ['a','b','c','d','e','f']
    return View('index', {'lists': lists})
```
ビュー
```
<ui>
    {% for list in lists %}
        <li>{{list}}</li>
    {% endfor %}
</ui>
```

## CSRF
```
<form method="POST">
    <input type="text" name="arg"/>
    <button type="submit">ボタン</button>
    {{ csrf_field|safe }}
</form>
```