コントローラー
===
[戻る](../../README.md)

## コントローラーを作るコマンド
```
craft controller App
```
/app/http/controllers/AppController.pyが作られる

## ルーティング
ルートが get('/app', 'AppController@index') の時
```
class AppController:
    def index(self):
        pass
```

## リクエストパラメータを取得
/app?arg=abcde
```
def index(self, Request):
    arg =  Request.input('arg')
    print(arg)
```
```
def index(self):
    arg =  request().input('arg')
    print(arg)
```

## ビューを返す
```
def index(self, View):
    return View('index')
```
```
def index(self):
    return view('index')
```

## ビューに変数を渡す
```
def index(self, View):
    arg = 'aaaaa'
    return View('index', {'arg': arg})
```
ビュー
```
<p>{{arg}}</p>
```

## Jsonを返す
```
def index(self):
    val = 12345
    return {'key': val}
```