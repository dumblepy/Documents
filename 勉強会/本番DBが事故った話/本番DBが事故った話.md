---
marp: true
---

# 本番DBが事故った話

---

## 会社のSlackにて

## 「本番DBの中身がステージング環境のものになっています！😱」

---

# 🥶🥶🥶🥶🥶🥶🥶🥶🥶🥶🥶🥶🥶🥶🥶

---

## 原因: 本番環境でステージング用のマイグレーションが実行されていた

---

cloudbuild.production.yaml
```yaml
steps:
  # Build the container image
  - name: gcr.io/cloud-builders/docker
    args:
      - build
      - --build-arg
      - DATABASE_URL=$_DATABASE_URL
      - --file
      - ./docker/api-batch/production.Dockerfile
      - .
```

---

production.Dockerfile
```Dockerfile
RUN pnpm prisma migrate:staging
```

staging.Dockerfileからproduction.Dockerfileをコピペして作った時に差し替えるのを忘れていた

---

# 復旧

---

## Cloud SQLのバックアップからバックアップ時点まで戻す
## お客さんに送ったメールからデータ作成
## 新規システムは既存システムと別データベースにしていたのでかなり運が良かった😮‍💨

---

# 対策

---

ファイル構成

```sh
seed
├── data
│   ├── Xx_seeder.ts
│   ├── Yy_seeder.ts
│   └── Zz_seeder.ts
├── production.ts
├── staging.ts
└── development.ts
```

---

production.ts
```ts
import { PrismaClient } from '../prisma/dist/index';
import { Xx_seeder } from './data/Xx_seeder';

const prisma = new PrismaClient();

async function main() {
  try {
    if (process.env.APP_ENV !== 'production') {
      throw new Error('本番環境でのみ動きます。APP_ENVがproductionではありません。');
    }
    // seederを実行
    await Xx_seeder(prisma);
  } catch (error) {
    console.error('Seed error:', error);
    throw error;
  } finally {
    await prisma.$disconnect();
  }
}

main()
```

---

# 教訓

- 本番、ステージング、開発環境で実行するシーダーのファイルは完全に別に分けよう！
- ファイルを分けてることで安心せずに環境変数でチェックしよう！
- バックアップは大事！ログ出力やメールなども使ってデータを復元できるようにしよう！

---

![](./whoiam.jpg)

ブロックチェーン業界で分散アプリケーションの開発をしています（Solidity、Motoko、Lua）
分散マキシマリスト、ブロックチェーンエンジニア、貨幣デザイナーです
Nim言語でWebフレームワークとクエリビルダを作っています
