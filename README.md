Documents
===

## Docker
[環境構築](./Docker/Docker環境構築.md)  
[Dockerの概念](./Docker/Dockerの概念.md)  
[Dockerコマンド](./Docker/Dockerコマンド.md)  
[Tips](./Docker/DockerTips.md)  
[Dockerfileとdocker-compose.ymlの対応表](./Docker/Dockerfileとdocker-compose.ymlの対応表.md)

## Git
[環境構築](./Git/Git環境構築.md)  
[Gitの概念](./Git/Gitの概念.md)  
[Gitコマンド](./Git/Gitコマンド.md)  
[Tips](./Git/GitTips.md)  

## anyenv
[環境構築](./anyenv/anyenv環境構築.md)  

---

## JavaScript
[環境構築](./JavaScript/JavaScript環境構築.md)  
[Tips](./JavaScript/JavaScriptTips.md)  
[React](./JavaScript/React.md)  
[ReactをJqueryの代わりに使う](./JavaScript/ReactをJqueryの代わりに使う.md)
[Vue](./JavaScript/Vue.md)  
[APIアクセス](./JavaScript/APIアクセス.md)  

## Python
[環境構築](./Python/Python環境構築.md)  
[規約](./Python/Python規約.md)  
[テスト](./Python/pytest.md)  

### Django
- [環境構築](./Python/django/django環境構築.md)  
- [モデル](./Python/django/djangoモデル.md)  
- [管理ページ](./Python/django/django管理ページ.md)  
- [urls]()  
- [view]()  
- [template]()  

### Masonite
- [環境構築](./Python/masonite/masonite環境構築.md)  
- [ルート](./Python/masonite/masoniteルート.md)  
- [コントローラー](./Python/masonite/masoniteコントローラー.md)  
- [モデル](./Python/masonite/masoniteモデル.md)  
- [ビュー](./Python/masonite/masoniteビュー.md)  
- [マイグレーション](./Python/masonite/masoniteマイグレーション.md)  
- [ダッシュボード](./Python/masonite/masoniteダッシュボード.md)  


## PHP
[環境構築](./PHP/PHP環境構築.md)  

### Laravel
- [環境構築](./PHP/laravel/laravel環境構築.md)  


## Ruby
- [環境構築](./Ruby/Ruby環境構築.md)  
- [規約]()  

### Rails
- [環境構築](./Ruby/Rails/Rails環境構築.md)  
- [ルート](./Ruby/Rails/Railsルート.md)  
- [コントローラー](./Ruby/Rails/Railsコントローラー.md)  
- [モデル](./Ruby/Rails/Railsモデル.md)  
- [ビュー](./Ruby/Rails/Railsビュー.md)   
- [テスト](./Ruby/Rails/Railsテスト.md)  


## Go
[環境構築](./Go/Go環境構築.md)

### Revel
- [環境構築](./Go/Revel/Revel環境構築.md)  
- [ルート](./Go/Revel/Revelルート.md)  
- [コントローラー](./Go/Revel/Revelコントローラー.md)  
- [モデル](./Go/Revel/Revelモデル.md)  
- [ビュー](./Go/Revel/Revelビュー.md)   
- [テスト](./Go/Revel/Revelテスト.md)  


## Nim
[環境構築](./Nim/nim環境構築.md)

## デザイン
[CSS](./デザイン/css.md)

---

## Database
[Database](./database.md)

## heroku
[heroku](./heroku.md)

## Server
[gunicorn](./Server/gunicorn.md)  
[Nginx](./Server/nginx.md)  

## 設計
[システム設計](https://github.com/donnemartin/system-design-primer/blob/master/README-ja.md)  
[URL](./設計/url.md)  
[MVC](./設計/mvc.md)  
[DDD](./設計/ddd.md)  

## その他
[現場での重い要件](./その他/現場での重い要件.md)  
[Linuxでスワップを作る](./その他/swap.md)  

---

各言語での規約
===
|---|インデント|文字列|
|---|---|---|
|html|半角2スペース|属性の値をダブルクォート|
|javascript|半角2スペース|シングルクオート|
|ruby|半角2スペース|シングルクオート|
|python|半角4スペース|シングルクオート|
|go|タブ|シングルクオート|



ウェブプログラミングで覚えるべき機能
===
- MVC
    - ルート
    - モデル
    - ヘルパー
    - コントローラー
    - ビュー
    - テスト
- ログイン機能
    - DB
    - ORM
    - パスワードのハッシュ化
    - セッション
- 投稿
    - CRUD
- セキュリティ
    - SQLインジェクション
    - XSS対策
- 大量のデータ集計
    - CSVファイル読み込み→DB書き込み
    - PDF出力

## コンテナ内で作成されたファイルのownerをログインユーザーにする
```
local_UID=$(id -u $USER)
local_GID=$(id -g $USER)

echo 'start'
sudo chown ${local_UID}:${local_GID} * -R
echo '======================'
sudo find . -name ".*" -print | xargs sudo chown ${local_UID}:${local_GID}
sudo chown ${local_UID}:${local_GID} .git -R
echo 'end'
```

## 自動再起動バッチ
```sh
while [ 1 ]; do
  ./main1 & \
  ./main2 & \
  ./main3
done
```

## 本番環境Linux手順
```sh
sudo apt update && sudo apt upgrade -y
sudo apt install -y docker.io docker-compose tmux

// dockerグループに追加
sudo groupadd docker
sudo usermod -aG docker ubuntu
exec $SHELL -l
// サーバー再起動 & 再接続
docker info

//プロジェクト用ディレクトリ作成
sudo mkdir /app
sudo chown ubuntu:docker /app
// gitパスワード保存
git config --global credential.helper store
cd /app
git clone ***
```

![給与体系](https://pbs.twimg.com/media/EYiD3EEUcAEIbK8?format=jpg&name=large)

## オブジェクト指向
```
class ポケモン:
    waza:str
    
    def ワザ(self):
        print(self.waza)
        
class ピカチュウ(ポケモン):
    name:str
    
    def __init__(self, name):
        self.name = name
        self.waza = "電気ショック"
```
```
サトシのピカチュウ = ピカチュウ("サトシのピカチュウ")
print(サトシのピカチュウ.name)
サトシのピカチュウ.ワザ()
```

## 学習履歴
|年|上期|下期|
|---|---|---|
|2016||PHP|
|2017|サーバー構築|JS|
|2018|Go|Ruby|
|2019|React|Python|
|2020|Nim|DDD|
|2021|事業検証|ブロックチェーン|
