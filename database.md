データベース
===
[戻る](./README.md)

Mysql MariaDB
===

PostgreSQL
===
## コマンド
### ログイン
```
# 初期ユーザー
psql -U postgres

作成したユーザーにはdbの指定が必要
psql -h host_name -U user_name -d db_name
```

### ユーザー・DB作成
```
create role user_name login password 'password';
create database db_name;
grant all privileges on database db_name to useuser_namer1;
```


### データベース一覧表示
```
\l
```

### 構造表示
```
DB構造表示
\d

#テーブル構造表示
\d table_name

select column_name, data_type from information_schema.columns where table_name = table_name

#外部キー一覧表示
SELECT table_name, constraint_name FROM information_schema.table_constraints WHERE table_schema = 'public' AND constraint_type = 'FOREIGN KEY'
```

sqlite3
===
```
sqlite3 masonite_blog.db

#データベース確認
.database

#テーブル構造確認
.schema
.schema table_name

#終了
.quit
.exit

#コマンド一覧
.help

#テーブル一覧
.table
```


## スキーマ出力
```
docker run -v "$PWD/schema:/output" --net="host" schemaspy/schemaspy:snapshot -t <DB種類> -host <DBホスト名/IP>:<ポート> -db <DB名> -u <DBユーザー名> -p <DBパスワード>
```
DB種類は`mysql`, `mariadb`, `pgsql`, `jtds`(SQL Server)

https://qiita.com/ap8322/items/b93dfb2ff29b026ffa72  

## 研修資料
[サイバーエージェントSQL Training 2021](https://speakerdeck.com/yutamiyake/sql-training-2021)
