## Linuxでスワップファイルを作る

```sh
fallocate -l 7G /root/.swapfile
chmod 600 /root/.swapfile
mkswap /root/.swapfile
swapon /root/.swapfile
echo '/root/.swapfile  none  swap  0  0' >> /etc/fstab
swapon --show
```
