
## インフラ周り
php.iniとかphp extentionsとか
Nginxの書き方がPHPだけ違う

## 関数の返り値の型定義で配列の中のオブジェクトクラスを指定できない

## 全てがarray。ListとDictのような使いわける言葉が定義されていない

## datetime変換のバグ

## 配列の使いにくさ
[【PHP】オブジェクトを連想配列に変換する方法まとめ](https://qiita.com/ymm1x/items/f197ddd327356b1e9088)

## Laravelのクエリビルダで関数によって返る型が違う
`get()`だと`Collection`  
`first()`と`find()`だと`Object`  
一致しない時は`null`

## ターミナルとの連携の悪さ
Python/Rubyのようにフレームワーク内部のソースコードに`print()/puts`を書いてターミナルに変数表示させるということができない

## Enumがない
https://speakerdeck.com/takayukifujisawa/nazephpnihaenumganaifalseka

## 未定義シンボルを許容する仕様
`i is not definded`にならず、無限ループで動く
```php
for ($i = 0; i < 100; $i ++) {
  …
}
```


## 引数の数が違ってもエラーにならない
```php
function hoge($a){
    echo $a;
}

hoge("a", "b");
```

## 標準ライブラリのエラー処理が統一的ではないこと。
想定のエラー把握の場合、例外を上げるものもあれば、set_error_handlerでキャッチしないとだめな場合。

goみたいに愚直に戻り値チェックしろはナンセンスで、それすら忘れてしまうから想定外・例外なわけ。

## クラスのプロパティをタイポしても、新しいプロパティとして作られて、バグに気づくのが遅れる。

クラス（型）は厳格性があるから、意味があるんじゃないのかよ。

このエラーまわり、クラスまわりをみても、触りたくない言語。

## JSONの空オブジェクト定義
他の言語なら`{}`で済むものを`new \stdClass`と書かなくてはならない
