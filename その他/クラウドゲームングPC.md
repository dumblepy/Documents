## AWS
https://tomikyblog.com/aws_game_pc/#toc6

マシンイメージは「Windows Server 2019」  
g4dn.xlargeを選択  
セキュリティグループにRDPを選択


## GCP Windows
Tokyoならasia-northeast1-aかasia-northeast1-c  
N1シリーズを選択

ディスク  
ゾーン標準…HDDなので安い  
ゾーンバランス…SDDでコスパ良い

可用性ポリシー プリエンプティブをオンにする


### クライアント
https://rkkoga.com/remote-connection-from-ubuntu-to-windows/

Linuxの場合Remminaをインストール  
Parsecをインストール（GPU設定後こちらから接続する）

### サウンドの設定  
https://vb-audio.com/Cable/

### ドライバ
[https://tomikyblog.com/aws_game_pc/#toc6](AWSでゲーム用のPCを作って遊んでみる)  
[ゲーミングPCをクラウドに作る 手順編](https://troches.jp/contents/tech/518)

Nvidiaドライバーをインストール
https://s3.amazonaws.com/nvidia-gaming/GRID-436.30-vGaming-Windows-Guest-Drivers.zip

[Windows インスタンスへの NVIDIA ドライバーのインストール](https://docs.aws.amazon.com/ja_jp/AWSEC2/latest/WindowsGuide/install-nvidia-driver.html#public-nvidia-driver)

[GPU 設定の最適化](https://docs.aws.amazon.com/ja_jp/AWSEC2/latest/WindowsGuide/optimize_gpu.html)

デバイスマネージャから「Microsoft基本ディスプレイアダプター」を無効にする  
OSを再起動

デスクトップ右クリックして出るタスクバーのNvidiaコントロールパネルのアイコンがあるので、そこからNVIDIAコントロールパネルを起動  
PhysX構成の設定でTesla4を選択

Parsecをインストールしクライアントから接続

## GCP Ubuntu
Tokyoならasia-northeast1-aかasia-northeast1-c  
N1シリーズを選択  
10VCPU 12GB  
1GPU Tesla T4  
NVIDIA GRIDはオンにしない  
可用性ポリシー プリエンプティブをオンにする

[Ubuntu on GCP にリモートデスクトップ接続](https://qiita.com/smiler5617/items/b1fd439299cc58c5f69b)
```sh
# パッケージ更新
sudo apt update && sudo apt -y upgrade

# ユーザー作成
sudo adduser dumblepy && sudo usermod -aG sudo dumblepy
sudo passwd dumblepy
sudo passwd ubuntu

# リモートデスクトップに必要なパッケージをインストール
sudo apt install -y ubuntu-desktop xrdp
sudo systemctl enable xrdp && sudo systemctl start xrdp && sudo systemctl status xrdp

# 日本語入力パッケージmozcをインストール
sudo apt install -y ibus-mozc language-pack-ja-base language-pack-ja gdebi-core wget
sudo localectl set-locale LANG=ja_JP.UTF-8 LANGUAGE="ja_JP:ja"
source /etc/default/locale
# 確認
echo $LANG
```

[UbuntuにNVIDIA driverをインストール/再インストールする方法](https://qiita.com/yto1292/items/463e054943f3076f36cc)
```sh
# GPUドライバーインストール
sudo apt-get purge nvidia-*
sudo add-apt-repository -y ppa:graphics-drivers/ppa
sudo apt update
# 推奨ドライバ確認
ubuntu-drivers devices
# recommendedのものをインストール
sudo ubuntu-drivers autoinstall
# sudo apt install -y nvidia-driver-470
# 再起動
sudo reboot
# 確認
nvidia-smi
```

RDPから接続

Steam Remote Playを使ってクライアントからホストに接続する
