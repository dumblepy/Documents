## 環境構築
```
apt update && apt upgrade -y
```

```
apt install -y tmux git
curl https://get.docker.com/ | bash
```

basolato.dockerfile
```dockerfile
FROM ubuntu:22.04 AS build

# prevent timezone dialogue
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt upgrade -y
RUN apt install -y --fix-missing \
        gcc \
        xz-utils \
        ca-certificates \
        curl \
        git

ARG VERSION="2.0.2"
WORKDIR /root
RUN curl https://nim-lang.org/choosenim/init.sh -o init.sh
RUN sh init.sh -y
RUN rm -f init.sh
ENV PATH $PATH:/root/.nimble/bin
RUN choosenim ${VERSION}

ENV PATH $PATH:/root/.nimble/bin

ADD ./ /basolato
WORKDIR /basolato

RUN nimble install -y
RUN ducere build -p:8080 -o:speed


FROM ubuntu:22.04 AS runtime

# prevent timezone dialogue
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt upgrade -y
RUN apt install -y --fix-missing \
        xz-utils \
        ca-certificates \
        libpq-dev

WORKDIR /basolato
COPY --from=build /basolato/main .
RUN chmod 111 main
COPY --from=build /basolato/startServer.sh .
RUN chmod 111 startServer.sh


# Secret
ENV SECRET_KEY="pZWEVzA7h2FcKLgVM3ec5Eiik7eU9Ehpf0uLdYOZDgr0uZKIo5LdQE9sjIub3IDkUTrf3X2Jsh1Uw8b02GtAfWRn4C9NptfdSyoK"
# DB Connection
ENV DB_DATABASE="hello_world"
ENV DB_USER="benchmarkdbuser"
ENV DB_PASSWORD="benchmarkdbpass"
ENV DB_HOST="tfb-database"
ENV DB_PORT=5432
ENV DB_MAX_CONNECTION=2000
ENV DB_TIMEOUT=30
# Logging
ENV LOG_IS_DISPLAY=false
ENV LOG_IS_FILE=false
ENV LOG_IS_ERROR_FILE=false
# Session db
# Session type, file or redis, is defined in config.nims
ENV SESSION_TIME=20160
ENV LOCALE=en


EXPOSE 8080

CMD ./startServer.sh

```

run.sh
```sh
while [ 1 ]; do
  ./main
done
```

max_connectins in .env
 - single…600
 - multi…300

timeout…15

```sh
# Run a verification
./tfb --test basolato --mode verify

# Run a test in debug mode, which skips verification and leaves the server up for testing endpoints
./tfb --test basolato --mode debug

# Run the default benchmark
./tfb --test basolato

# Specify which test types are run during benchmark
./tfb --test basolato --type json
./tfb --test basolato --type db
./tfb --test basolato --type fortune

./tfb --test nim-stdlib basolato echo axum axum-web-diesel laravel rails django --mode verify
./tfb --test nim-stdlib basolato echo axum axum-web-diesel laravel rails django

axum > actix
echo >= gin


実行結果のresults.jsonをここにコピペ  
https://tfb-status.techempower.com/share

https://www.techempower.com/benchmarks/#section=test&shareid=a631cec9-399b-468e-b922-c196f811d1be&test=composite

https://www.techempower.com/benchmarks/#section=test&shareid=bd0e8b10-3879-40ae-a1f7-a2d73c332946&test=composite

https://www.techempower.com/benchmarks/#section=test&shareid=81112a03-6903-4571-8556-b8380b940ef9&test=composite

https://www.techempower.com/benchmarks/#section=test&shareid=1b8aa2f1-4f4b-4781-a482-10137ee7500c&test=composite

https://www.techempower.com/benchmarks/#section=test&shareid=cc027c81-ee70-4dec-bb93-228f16fd1d1e&hw=ph&test=fortune
