環境構築
===
[戻る](../README.md)

## Windows
### Git For Windowsをインストール
基本的にはここに従う  
[自分用 Git For Windowsのインストール手順](https://qiita.com/toshi-click/items/dcf3dd48fdc74c91b409)  
Select Componentsのページだけは、Windows Explorer Integrationへのチェックを忘れずに

### コマンドで環境設定
- Git bashを起動して、ディレクトリを移動して、git cloneコマンドを実行する  
- Widnowsではパスの区切りは「\」になるけど、それは全部「/ 」に置き換えてcdコマンドで移動しすること
- Basic認証では、ユーザー名にメールアドレスを入力すること
```
例
C:\Users\owner\Desktop\フォルダ\git
↓
cd C:/Users/owner/Desktop/フォルダ/git
```

GithubかGitlabでアカウントを作ってから、以下のコマンドを実行
```
git init
git config --global user.name "username"
git config --global user.email "username@gmail.com"
git clone http://gitlab.com/xxx.git
```

## Linux

## Mac