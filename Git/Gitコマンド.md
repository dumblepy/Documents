Gitコマンド
===
[戻る](../README.md)

## クローンする時
gitのアカウントを設定する
```
git config --global user.name "username"
git config --global user.email "username@xx.co.jp"
```
gitクローン実行
```
git clone https://gitlab.com/xxxx.git
```

#### アカウント情報をプロジェクトに持たせたくない時

gitのアカウントを設定する
```
git init
git config --local user.name "username"
git config --local user.email "username@xx.co.jp"
```
gitクローン実行
```
git clone https://gitlab.com/xxxx.git
```
クローンしてできたディレクトリ内にはまた.gitフォルダがあり、その中は別のgit空間になるので、その中で再度アカウント情報を設定しないといけない
```
cd xxxx
git config --local user.name "username"
git config --local user.email "username@xx.co.jp"
```

---  
  
**※Windowsの時**  
Basic認証の画面が出るが、ユーザー名には必ずgitlabに登録してある **「メールアドレス」** を入力すること  

## 基本的な運用手順
```
git checkout develop
git pull
git checkout -b Feature/n
～ソース編集～
git status
git add .
git commit
git pull origin develop
git push --set-upstream origin Feature/n
～ソース編集～
git push 
```
#### 各コマンドの説明
- git checkout develop  
開発ソースはdevelopブランチにあるので、現在のブランチをdevelopに移動する

- git pull  
developブランチを最新の状態にする

- git checkout -b Feature/n  
developブランチを元にして、「Feature/n」という名前のブランチを切る  
nはissu番号に同じ

- git status  
何のファイルが編集されたか、余計なファイルにまで変更が入っていないか確認する  

- git add  
ファイルをgitに追跡させる  

```  
#全ファイルを追跡させる
git add .

#特定のファイルだけ追跡させる
git add route/web.rb

#ファイルは複数指定できる
git add route/web.rb public/js/app.js
```  

- git commit  
ローカルブランチにコミットする  
ソースの変更がローカルに記録されるだけで、リモートには何も影響しない  
コミットメッセージには、「何のために何をどうしたか」を明記すること  
viが自動起動するので、その中でコメントを書く  

```  
issue #1234 削除フラグが更新されない不具合の修正

更新SQLの対象カラムに削除フラグが含まれていなかったため追加しました。
```  

[参考 Gitのコミットメッセージの書き方](https://qiita.com/itosho/items/9565c6ad2ffc24c09364)

- git pull origin develop  
他のブランチのプルリクが取り込まれて、自分がブランチを切った時点よりも進んだdevelopの変更を自分のブランチに取り込む。

- git push --set-upstream origin Feature/n  
初めてpushする時点では、リモートに自分の作業ブランチが存在しない。そのため新規作成する必要がある。新規作成するのが「--set-upstream」のオプション  
2回目以降のpushは「git push」だけで良い  

## 公開ブランチで他ブランチの変更を取り込む（次期バージョンリリースなどでdevelopへの変更を取り込む場合など）
```
git switch develop
git pull
git switch feature/xx
git merge --no-ff
コミットが作られる
git push
```

## 自分の作業ブランチでツリー派生元の変更を取り込む（リベースする）
```
git switch develop
git pull
git switch feature/xx
git rebase develop
コンフリクト修正
  git add .
  git rebase --continue
  これをコンフリクトが無くなるまで繰り返す
git push --force-with-lease
```

## マージされた作業ブランチ削除する
```
git switch feature/xxx
git pull
git switch develop
git pull --prune
git branch -d feature/xx
```

## リベースでコンフリクトが発生した時
1. 手動でソースの差分を修正する
2. 「git rebase --continue」を実行する  

これを問題が無くなるまで何回も繰り返す

## ツリーに他人のコミットログが残る時
```
#現在のブランチを元にバックアップを取る
git checkout -b Feature/n_back

#元のブランチに戻る
git checkout Feature/n

#developを元にリベースする
git pull --rebase origin develop

ソースの差分修正

#リベースを続ける
git rebase --continue

#状態がきれいになったら強制プッシュ
git push --force-with-lease
```