GitTips
===
[戻る](../README.md)

#### commit時のエディタを変える
```
git config --global core.editor 'vim -c "set fenc=utf-8"'
or
git config --global core.editor 'emacs -nw'
```

#### 全てのブランチを表示させたい時
```
git branch -a
```

#### 現在のブランチの変更をstashに隠して、他のブランチに行きたい時
```
git stash
git checkout Feature/n
```

#### stashに隠した変更を復元したい時
```
git stash pop
```

#### stashの一覧を確認→削除したい時
```
git stash list
git stash drop stash@{n}
```

#### ローカルブランチを削除したい時
```
#もしリモートにマージされていたら削除
git branch -d Feature/n

#確認せず強制的に削除
git branch -D Feature/n
```

#### リモートブランチを削除したい時
```
git push --delete origin Feature/n

＃リモートでブランチを消したことをローカルにも反映
git fetch --prune
```

#### 他人が作った、既にリモートから消されたブランチが一覧に表示される時
```
git remote prune origin
```


#### 追跡済みのファイルをローカルに残したままgitの追跡を外したい時
[GITの対象から外したいけど、ファイル自体は残しておきたい](https://tech.nosuz.jp/2017/05/stop-tracking-files-on-git/)

gitにファイルを削除したと通知
```
git rm --cached test.txt test/
```
   
.gitignoreに消したいファイルを追加
```
test.txt
test/*
```

.gitignoreをコミット
```
git add .
git commit
```

#### 前回コミットのコミットメッセージを再編集したい時
```
git commit --amend
```

#### ファイルの編集を取り消して、前回コミット時の状態に戻したい時
```
git reset --hard HEAD
```

#### 1つのブランチを複数人で編集していて、他人のpushを取り込みたい時
```
git pull --rebase
```

#### git bashに現在のブランチ名がおかしい！反映されない！時
```
rm  -fr .git/rebase-apply
```

#### git pushをしたら「Updates were rejected because the tip of your current branch is behind」のエラーが出た時
```
git push --force-with-lease
```
```
git pull origin Feature/n
差分修正
git commit
git push
```

#### アクセストークン
Github上でアクセストークンを作る https://github.com/settings/tokens

[Git に GitHub の認証情報をキャッシュする](https://docs.github.com/ja/github/getting-started-with-github/getting-started-with-git/caching-your-github-credentials-in-git)
```
git config --global credential.helper store

git push
>> username:
>> password:
```
このパスワードに生成したトークンを入力する。以降は認証情報は求められない
