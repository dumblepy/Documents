anyenv環境構築
===
[戻る](../README.md)

pyenv、rbenv、ndenvなど**env をまとめて管理できるツール  
[anyenv](https://github.com/riywo/anyenv)  
```
git clone https://github.com/riywo/anyenv ~/.anyenv
echo 'export PATH=$HOME/.anyenv/bin:$PATH' >> ~/.bash_profile
echo 'eval "$(anyenv init -)"' >> ~/.bash_profile
exec $SHELL -l
anyenv install --init
```

```
vi ~/.bashrc

if [ -d $HOME/.anyenv ]
then
    export PATH=$HOME/.anyenv/bin:$PATH
    eval "$(anyenv init -)"
fi
```

## anyenv-updateをインストール
anyenvやそこで管理している**envのアップデートを簡単に行えるようにするプラグイン
```
mkdir -p $(anyenv root)/plugins  

git clone https://github.com/znz/anyenv-update.git $(anyenv root)/plugins/anyenv-update  
```