# Windows開発環境構築
## Windowsの設定
「Windowsの機能の有効化と無効化」を開くき以下にチェックを入れる
- Linux用Windowsサブシステム
- 仮想マシンプラットフォーム

Windowsを再起動する

## wslをインストール
Windowsターミナルを起動 Windowsの検索🔍から

```sh
wt
```

ターミナルが起動したら

```sh
wsl --install
```

インストール済みディストリビューション一覧を表示

```sh
wsl -l
```

インストール可能なディストリビューション一覧を表示

```sh
wsl -l --online
```

デフォルトのubuntuを削除

```sh
wsl --unregister Ubuntu
```

wslをインストールするソフトのx86_64.zipをDL https://github.com/nullpo-head/wsl-distrod/releases

解凍して出たexeを実行するとランチャーが起動する 対話形式で聞かれるので以下を選ぶ - ubuntu - jammy - Linux用のname, passwordを入力

ターミナルを終了

wslを再起動

```
wsl --shutdown
wsl
```

## wsl内にdockerをインストール

Dockerインストールの前準備

```
sudo apt update && sudo apt upgrade -y
sudo apt install -y curl wget gnupg2
curl -fsSL http://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Dockerインストール

```
curl https://get.docker.com | sudo sh
```

### sudoなしでdockerコマンドを使えるようにする

現在のユーザーの所属グループを確認

```
groups $USER
```

dockerグループを作成

```
groupadd docker
```

グループに追加

```
sudo usermod -aG docker $USER
```

確認

```
groups $USER
```

wslから抜ける

```
exit
```

windowsターミナル内でwslを再起動

```
wsl --shutdown
wsl
```

Linuxでdockerコマンドを使えるか確認

```
docker ps
```

## VSCodeからdockerコンテナに接続する

VSCode拡張機能のRemode Developmentをインストールする

wsl内でVSCodeを起動

```
code
```

VSCodeの左下の緑の「><」をクリックして、「Attach to Running Container」を選択 動いてるコンテナを選択 Dockerに接続したVSCodeが開く

## LinuxにNodejsの環境を作る

gitからclone

```
git clone https://github.com/nodenv/nodenv.git ~/.nodenv
```

make、gccをインストール

```
sudo apt install -y make gcc
```

nodenvを高速化

```
cd ~/.nodenv && src/configure && make -C src
```

pathを追加

```
echo 'export PATH="$HOME/.nodenv/bin:$PATH"' >> ~/.bashrc
```

シェルをセットアップ

```
~/.nodenv/bin/nodenv init
```

bashrcを再読み込み

```
source ~/.bashrc
```

nodenv installコマンドを使えるようにする

```
git clone https://github.com/nodenv/node-build.git $(nodenv root)/plugins/node-build
```

正常にインストールできたかチェック

```
curl -fsSL https://github.com/nodenv/nodenv-installer/raw/master/bin/nodenv-doctor | bash
```

プロジェクトフォルダ内の `.node-version`のファイルに書いてあるバージョンをnodenvでインストールする