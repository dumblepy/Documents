Nginx
===
[戻る](../README.md)

/etc/nginx/nginx.conf
```
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
        worker_connections 768;
        # multi_accept on;
}

http {

        ##
        # Basic Settings
        ##

        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 65;
        types_hash_max_size 2048;
        # server_tokens off;

        # server_names_hash_bucket_size 64;
        # server_name_in_redirect off;

        include /etc/nginx/mime.types;
        default_type application/octet-stream;

        ##
        # SSL Settings
        ##

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
        ssl_prefer_server_ciphers on;

        ##
        # Logging Settings
        ##

        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;

        ##
        # Gzip Settings
        ##

        gzip on;
        gzip_vary on;
        gzip_proxied any;
        gzip_comp_level 6;
        gzip_buffers 16 8k;
        gzip_http_version 1.1;
        gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

        ##
        # Virtual Host Configs
        ##

        include /etc/nginx/conf.d/*.conf;
        #include /etc/nginx/sites-enabled/*;
        #include /etc/nginx/sites-enabled/*;

        upstream app1 {
                server          unix:///var/run/app1.sock;
        }

        upstream app2 {
                server          unix:///var/run/app2.sock;
        }

        server {
                listen 80;
                server_name www.mydomain.co.jp;
                return 301 https://mydomain.co.jp$request_uri;
        }

        server {
                listen 443;
                server_name www.mydomain.co.jp;
                return 301 https://mydomain.co.jp$request_uri;
        }

        server {
                listen 80 default_server;
                server_name mydomain.co.jp;

                # SSL証明書更新のための80番ポート
                location ^~ /.well-known/acme-challenge/ {
                        default_type "text/plain";
                        root         /var/www/ssl;
                }

                # それ以外はSSL通信にリダイレクト
                location / {
                        # Redirect all HTTP requests to HTTPS with a 301 Moved Permanently response.
                        return 301 https://$host$request_uri;
                }
        }

        server {
                listen      443 default ssl http2;
                server_name  dumblepy.site;
                ssl on;
                ssl_certificate /etc/letsencrypt/live/mydomain.co.jp/fullchain.pem; # サーバー証明書のパス
                ssl_certificate_key /etc/letsencrypt/live/mydomain.co.jp/privkey.pem; # 秘密鍵のパス
                server_tokens off;

                location /static {
                        alias   /where/static/dir;
                        expires 1w;  # キャッシュ1週間
                        access_log off;
                }

                location /favicon.ico {
                        alias    /where/favicon/dir/favicon.ico;
                        expires 1d;  # キャッシュ1日
                        access_log off;
                }


                location /service-worker.js {
                        alias    /where/service-worker/dir/service-worker.js;
                }

                location / {
                        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                        proxy_set_header Host $http_host;
                        proxy_redirect off;
                        proxy_pass   http://app1;
                }
        }
}


#mail {
#       # See sample authentication script at:
#       # http://wiki.nginx.org/ImapAuthenticateWithApachePhpScript
#
#       # auth_http localhost/auth.php;
#       # pop3_capabilities "TOP" "USER";
#       # imap_capabilities "IMAP4rev1" "UIDPLUS";
#
#       server {
#               listen     localhost:110;
#               protocol   pop3;
#               proxy      on;
#       }
#
#       server {
#               listen     localhost:143;
#               protocol   imap;
#               proxy      on;
#       }
#}
```