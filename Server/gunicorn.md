gunicorn
===
[戻る](../README.md)

run.sh
```sh
killall gunicorn
service nginx restart
nohup gunicorn wsgi --config gunicorn.py > /dev/null 2>&1 &
```


gunicorn.py
```python
import multiprocessing

socket_path = 'unix:/var/run/masonite.sock'
bind = socket_path

# Worker Processes
workers = 1
worker_class = 'sync'
#worker_class = 'gthread'
threads = 1

# Logging
logfile = '/home/www/masonite_blog/project/log.log'
loglevel = 'info'
logconfig = None

```