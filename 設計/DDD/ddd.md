DDD
===
[戻る](../README.md)

- そもそもの設計自体の難しさ
- なぜ設計がfixできないのか
  - PMFしてるからと言って仕様がfixしているとは限らない
  - PMF前ならなおさら
- DDD自体の難しさ
  - Rails、Laravel、Django的な既存のMVCフレームワークの「モデルが何をしているか」を知っていないといけない？
  - そもそもエヴァンズ本って2005年にDajngo、Railsが生まれたより前に書かれているから、ActiveRecordというのはエヴァンズを踏まえた上で極限まで簡素化を目指した形になっているのでは
  - キモはポリモーフィズムとレイヤー分け
- 要求と設計
  - 開発者はコーダーにあらず、要求から設計をすることまでを開発者の責務とすると設計がバッティングするのでは？
  - →エンジニアでみんなで設計しきる
  - DDDを前提にしてユースケースのフローチャートやクラス図まで作り切る
  - 要求がまとめられて、それがタスクに落とされて説明されて割り振られる
- アジャイルとDDDは合うのかどうか
  - →小さい粒度でPR出す
  - issue着手からPRまで長くて2日
  - レビューをするのではなく共有をする
  - 「コードの共同所有」
- →開発する前に設計思想を共有したりコミュニケーション大事
  - スクラムイベントは全部動画で残す
  - チャット・録画（デイリーのスクラムイベントより大きい単位）・wiki・コードで共有
- 誰がDB設計をして誰がエンティティを作るのか
  - そもそもschemaとマイグレーションが変更しずらい開発の上にエンティティを作る人も別だと開発者に仕様が降りてきづらい
  - しかしそれを一人でやりきらなければいけないのであれば、めちゃ時間かかるのでは？
  - チーム開発でのベストプラクティスを見つけたい
  - →みんなで設計する
- なぜ仕様が迷子になるのか
  - ユースケース駆動開発と組み合わせるべき？
  - ロバストネス分析をすれば機械的に発見できるか？
  - →全然そんなことない
    - アプリケーション層のインターフェースを決めるのには役立つかも
- →エヴァンズ「話せ！！！」
  - 話したことをパワポとかではなくモデリングに落とす
- →XPの人たちは概念やプラクティスだけでなくツールを作っている
  - 例：WikiやCI
  - HanamiやSymfoneyは戦術的なアプローチにとどまっているから広まってない？
- →モジュラモノリスやりきれる強いエンジニアがいないならDDDはしない
  - 式年遷宮で作り直していく
  - その式年遷宮を体験したエンジニアは全てのドメインが見えているはずだから戦略DDDできる

## 参考文献
[ドメイン駆動設計で保守性をあげたリニューアル事例 〜 ショッピングクーポンの設計紹介](https://techblog.yahoo.co.jp/entry/2021011230061115/)  
[DDDになぜ失敗するのか](https://zenn.dev/dorarep/scraps/53c55bf94cb094)  
[DDD難民に捧げるDomain-Driven Designのエッセンス](https://www.ogis-ri.co.jp/otc/hiroba/technical/DDDEssence/chap1.html)


## サンプル
ユーザーへのCRUDを例にして説明する

- 何のためにやるか？
  - DBの1テーブル単位のCRUDという作り方から脱却し、業務用件から設計できるようにするため
  - 責任と関心を分離して、要件の変更時に変更箇所を少なくするため

実際のソースコードが以下のようになることを目指す
- バリューオブジェクトのメソッドはエンティティしか知らない
- エンティティのメソッドはサービスクラスしか知らない
- リポジトリのメソッドはサービスクラスしか知らない
- サービスクラスのメソッドはビューしか知らない

#### ルーティング
app/urls.py

- URLとビューのメソッドを紐付ける
```py
from app.views.user_views import UserViews

users_urls = [
    path('', UsersViews.index),
    path('create/', UsersViews.create),
    path('store/', UsersViews.store),
    path('<int:id>/', UsersViews.show),
    path('<int:id>/edit/', UsersViews.edit),
    path('<int:id>/update/', UsersViews.update),
    path('<int:id>/destroy/', UsersViews.destroy),
]

app_urls = [
    path('users/', include(users_urls)),
]
```

### ビュー
app/views/user_views.py

- リクエストパラメータの取得
- サービスクラスのメソッドを実行
- エラーハンドリング
- レスポンスの返却

<details><summary>サンプルコード</summary><div>

```
from django.shortcuts import redirect, render
from rest_framework.decorators import api_view

from ..domain.services.users_service import UserService


class UserViews:
    @api_view(['GET'])
    def index(request):
        users = UserService.index()
        return render(request, 'users/index.html', {'users': users})

    @api_view(['GET'])
    def create(request):
        return render(request, 'users/create.html')

    @api_view(['POST'])
    def store(request):
        user = {
            'name': request.POST['name'],
            'email': request.POST['email'],
            'password': request.POST['password'],
            'permission': request.POST['permission']
        }
        try:
            UserService.store(**user)
        except Exception as e:
            return render(
                request,
                'users/create.html',
                {'user': user, 'error': str(e)}
            )

        return redirect('/users/')

    @api_view(['GET'])
    def show(request, id: int):
        user = UserService.show(id)
        return render(request, 'users/show.html', {'user': user})

    @api_view(['GET'])
    def edit(request, id: int):
        user = UserService.edit(id)
        return render(request, 'users/edit.html', {'user': user})

    @api_view(['POST'])
    def update(request, id: int):
        user = {
            'id': id,
            'name': request.POST['name'],
            'email': request.POST['email'],
            'permission': request.POST['permission']
        }
        try:
            UserService.update(**user)
        except Exception as e:
            return render(
                request,
                'users/edit.html',
                {'user': user, 'error': str(e)}
            )

        return redirect(f'/users/{id}/')

    @api_view(['POST'])
    def destroy(request, id: int):
        result = UserService.delete(id)
        if result:
            return redirect('/users/')

```
</div></details>

### サービスクラス
app/domain/services/users_service.py

- 参照系処理(SELECT)の場合
  - リポジトリのメソッドを実行して返り値(DBから取得した値)を取得
  - 引数にリポジトリからの返り値を入れてエンティティのインスタンスを生成
  - エンティティのメソッドを実行して返り値を取得
  - ビューにエンティティからの返り値を返す
- 登録/更新系処理(INSERT, UPDATE)の場合
  - 重複チェック→不正なら例外を発生
  - 引数にビューから渡された値を入れてエンティティのインスタンスを生成
  - エンティティのメソッドを実行して返り値を取得
  - ビューにエンティティからの返り値を返す
- 削除系処理(DELETE)の場合
  - リポジトリのメソッドを実行

<details><summary>サンプルコード</summary><div>

```
from ..domain_models.entities.user_entity import UserEntity
from ...repositories.users_repository import UserRepository


class UserService:
    @staticmethod
    def index():
        users = UserRepository.index()
        users = [
            UserEntity(
                id=val['id'],
                name=val['name'],
                email=val['email'],
                permission=val['permission']
            ).get_index_dict()
            for val in users
        ]
        return users

    @staticmethod
    def store(name: str=None, email: str=None, password: str=None,
                permission: str=None):
        """store new user.

        Args:
            name (str): user name
            email (str): user email
            password (str): user password
            permission (str): user permission
        """
        # 重複チェック
        duplicate_users = UserRepository.check_unique(name, email)
        if duplicate_users:
            print(duplicate_users)
            raise Exception('name and email should be unique')

        user = UserEntity(
            name=name,
            email=email,
            password=password,
            permission=permission
        ).get_store_dict()
        result = UserRepository.store(user)
        return result

    @staticmethod
    def show(id: int):
        user = UserRepository.show(id)
        user = UserEntity(
            id=user['id'],
            name=user['name'],
            email=user['email'],
            permission=user['permission_id'],
            created_at=user['created_at'],
            updated_at=user['updated_at']
        ).get_show_dict()
        return user

    @staticmethod
    def edit(id: int):
        user = UserRepository.show(id)
        user = UserEntity(
            id=user['id'],
            name=user['name'],
            email=user['email'],
            permission=user['permission_id'],
            created_at=user['created_at'],
            updated_at=user['updated_at']
        ).get_edit_dict()
        return user

    def update(id: int, name: str, email: str, permission: str):
        """update user.

        Args:
            id (int): primary_ley
            name (str): user name
            email (str): user email
            permission (str): user permission
        """
        # 重複チェック
        duplicate_users = UserRepository.check_unique(name, email, id=id)
        if duplicate_users:
            print(duplicate_users)
            raise Exception('name and email should be unique')

        user = UserEntity(
            name=name,
            email=email,
            permission=permission
        ).get_update_dict()
        new_user = UserRepository.update(id, user)
        return new_user

    @staticmethod
    def delete(id: int):
        """delete user.

        Args:
            id (int): user id

        Returns:
            bool: if delete succress, reutrn True, or return False.
        """
        result = UserRepository.delete(id)
        return result

```
</div></details>

### エンティティ
app/domain/domain_models/entities/user_entity.py

- 一つの業務で扱うテーブルの、1レコード(行)の定義を持つクラス
  - 可変
  - 区別される：例えば同姓同名かもしれないので識別子が必要
  - 同一性を持つ：名称が変わっても同一人物なので識別子が必要
  - 値と振る舞い(パラメータとメソッド)を持つ
- 振る舞いがないパラメータにはプリミティブな値、振る舞いを持つパラメータはバリューオブジェクトを持つ
- インスタンス生成時に、引数で渡された値を元にパラメータをセットする
- サービスクラスで実行される、複数のパラメータをまとめてdictで返却するためのメソッドを持つ

<details><summary>サンプルコード</summary><div>

```
from ..value_objects import (
    EmailValueObject,
    PermissionValueObject,
    PasswordValueObject,
    DateTimeValueObject
)

class UserEntity:
    def __init__(self, id:int=None, name:str=None, email:str=None,
                password:str=None, permission_id:int=None, permission=None,
                created_at=None, updated_at=None):
        self.id = id
        self.name = name
        self.email = EmailValueObject(email)
        self.password = PasswordValueObject(password)
        self.permission = PermissionValueObject(permission)
        self.created_at = DateTimeValueObject(created_at)
        self.updated_at = DateTimeValueObject(updated_at)

    def get_index_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email.get_label(),
            'permission': self.permission.get_label()
        }

    def get_store_dict(self):
        return {
            'name': self.name,
            'email': self.email.get_label(),
            'password': self.password.get_hashed_value(),
            'permission_id': self.permission.get_number(),
            'created_at': self.created_at.get_label(),
            'updated_at': self.updated_at.get_label()
        }

    def get_show_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email.get_label(),
            'permission': self.permission.get_label(),
            'created_at': self.created_at.get_label(),
            'updated_at': self.updated_at.get_label()
        }

    def get_edit_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email.get_label(),
            'permission': self.permission.get_label(),
        }

    def get_update_dict(self):
        return {
            'name': self.name,
            'email': self.email.get_label(),
            'permission_id': self.permission.get_number(),
            'updated_at': self.updated_at.get_label()
        }

```
</div></details>

### バリューオブジェクト
app/domain/domain_models/value_objects.py

- エンティティの一つのパラメータの値と振る舞いを表現するクラス
- 不変
- プライベートなパラメータを一つだけ持つ
- インスタンス生成時に値のバリデーションチェックをする→正常ならパラメータに値をセット、不正なら例外を発生する
- パラメータを返却するメソッドを持つ

<details><summary>サンプルコード</summary><div>

```
from django.contrib.auth.hashers import make_password
import re

class EmailValueObject:
    def __init__(self, value):
        EMAIL_REGEX = re.compile(
            r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
        if not EMAIL_REGEX.match(value):
            raise Exception(f'invalid email {value}')
        else:
            self.__value = value

    def get_label(self):
        return self.__value

class PasswordValueObject:
    def __init__(self, value):
        self.__value = value

    def get_hashed_value(self):
        return make_password(self.__value)
```
</div></details>

### リポジトリ
app/repositories/users_repository.py

- データの永続化処理を行うクラス
- 一般的にはRDBを使うが、NoSQLやファイル出力の場合もある
- RDBを使う場合には、ORMを実行する

<details><summary>サンプルコード</summary><div>

```
from app.orator.User import User


class UserRepository:
    @staticmethod
    def index():
        return User \
            .select(
                'users.id as id',
                'users.name as name',
                'users.email as email',
                'permissions.permission as permission',
            ) \
            .join('permissions', 'users.permission_id', '=', 'permissions.id') \
            .order_by('users.id') \
            .get() \
            .serialize()

    @staticmethod
    def show(id: int):
        user = User.find(id).serialize()
        return user

    @staticmethod
    def store(user):
        return User.insert(user)

    @staticmethod
    def update(id, user):
        return User.where('id', id).update(user)

    @staticmethod
    def delete(id: int):
        return User.find(id).delete()

    @staticmethod
    def check_unique(name:str, email:str, id:int=None):
        if id:
            return User \
                .where('id', '!=', id) \
                .where(
                    User.where('name', name).or_where('email', email)
                ) \
                .get() \
                .serialize()
        else:
            return User \
                .where('name', name) \
                .or_where('email', email) \
                .get() \
                .serialize()
```
</div></details>
