URL
===
[戻る](../README.md)

### ウェブページ
|動詞|URI|アクション|内容|
|---|---|---|---|
|GET|/posts|index|全件表示|
|GET|/posts/{id}|show|一件表示|
|GET|/posts/create|create|新規作成ページ|
|POST|/posts/create|create_confirm|新規作成確認|
|POST|/posts|store|新規作成|
|GET|/posts/{id}/edit|edit|一件編集ページ|
|POST|/posts/{id}/edit|edit_confirm|一件編集確認|
|POST|/posts/{id}|update|一件編集|
|GET|/posts/{id}/delete|delete_confirm|一件削除確認ページ|
|POST|/posts/{id}/delete|delete|一件削除|


### APIサーバーの時
|動詞|URI|アクション|内容|
|---|---|---|---|
|GET|/posts|index|全件表示|
|POST|/posts/confirm|create_confirm|新規作成確認ページ|
|POST|/posts/store|store|新規作成|
|GET|/posts/{id}|show|一件表示|
|PATCH|/posts/{id}/confirm|edit_confirm|一件編集確認ページ|
|PATCH|/posts/{id}/update|update|一件編集|
|DELETE|/posts/{id}/destroy/confirm|destroy_confirm|一件削除確認ページ|
|DELETE|/posts/{id}/destroy|destroy|一件削除|

### Django

|動詞|URI|内容|
|---|---|---|
|GET|/posts|全件表示|
|GET|/posts/{id}|一件表示|
|GET|/posts/create|新規作成ページ|
|POST|/posts/create|新規作成|
|GET|/posts/{id}/edit|一件編集ページ|
|POST|/posts/{id}/edit|一件編集|
|GET|/posts/{id}/delete|一件削除確認ページ(オプション)|
|POST|/posts/{id}/delete|一件削除|

#### urls.py
```
from views import (
    PostListView,
    PostDetailView
)

urls = [
    path('posts/', PostListView.as_view()),
    path('posts/<int:id>/', PostDetailView.as_view()),
]
```

#### views.py
```
from rest_framework.views import APIView

class PostListView(APIView):
    def get(self, request):
        pass

class PostDetailView(APIView):
    def get(self, request, id):
        pass
```


### Laravel、Masonite

|動詞|URI|アクション|内容|
|---|---|---|---|
|GET|/posts|index|全件表示|
|GET|/posts/create|create|新規作成ページ|
|POST|/posts|store|新規作成|
|GET|/posts/{id}|show|一件表示|
|GET|/posts/{id}/edit|edit|一件編集ページ|
|PUT/PATCH|/posts/{id}|update|一件編集|
|DELETE|/posts/{id}|destroy|一件削除|


### CakePHP

|動詞|URI|アクション|内容|
|---|---|---|---|
|GET|/posts|index|全件表示、新規作成ページ|
|POST|/posts|add|新規作成|
|GET|/posts/{id}|view|一件表示|
|PUT/PATCH|/posts/{id}|edit|一件編集|
|DELETE|/posts/{id}|delete|一件削除|

[Restfulなルーティング - CakePHP Cookbook](https://book.cakephp.org/3.0/ja/development/routing.html#restful)

### Rails

|動詞|URI|アクション|内容|
|---|---|---|---|
|GET|/posts|index|全件表示|
|GET|/posts/new|new|新規作成ページ|
|POST|/posts/create|create|新規作成|
|GET|/posts/{id}|show|一件表示|
|GET|/posts/{id}/edit|edit|一件編集ページ|
|PUT/PATCH|/posts/{id}|update|一件編集|
|DELETE|/posts/{id}|destroy|一件削除|

[Railsのルーティング - Railsガイド](https://railsguides.jp/routing.html#crud、動詞、アクション)