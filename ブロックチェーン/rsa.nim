import std/math

const
  P = 5
  Q = 7
  N = P * Q # 35
  PHI = (P-1)*(Q-1) # 24
  E = 65537
  D = 17

proc enc(m, e, n:int):int =
  (m ^ e) mod n

proc dec(c, d, n:int):int =
  (c ^ d) mod n

block:
  var m = 10
  # let c = enc(m, E, N)
  # echo c

  # m = dec(c, D, N)
  # echo m

  echo 10 ^ 65537
