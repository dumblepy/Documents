EthersJS
===

## wallet connect
useHook.ts
```ts
const walletAddressState = atom<string>({
  key: 'walletAddressState',
  default: '',
  effects_UNSTABLE: [persistAtom],
});

const isConnectedWalletState = atom<boolean>({
  key: 'isConnectedWalletState',
  default: false,
  effects_UNSTABLE: [persistAtom],
});

const balanceState = atom<string>({
  key: 'balanceState',
  default: '',
  effects_UNSTABLE: [persistAtom],
});

export const useWeb3 = () => {
  const [walletAddress, setWalletAddress] = useRecoilState(walletAddressState);
  const [isConnected, setIsConnected] = useRecoilState(isConnectedWalletState);
  const [balance, setBalance] = useRecoilState(balanceState);
  const provider = new ethers.providers.Web3Provider(window.ethereum);

  const connectEthereum = async () => {
    await provider.send('eth_requestAccounts', []);
    setWalletAddress(window.ethereum.selectedAddress);
    if (window.ethereum.selectedAddress.length > 0) {
      setIsConnected(true);
    }
    const balanceNum = await provider.getBalance(window.ethereum.selectedAddress);
    let balance = ethers.utils.formatEther(balanceNum);
    balance = balance.substring(0, 5);
    setBalance(balance);
  };

  // 接続解除した時、アカウントを切り替えた時、accountsChangedイベントが起きる
  window.ethereum.on('accountsChanged', (accounts: string[]) => {
    (async () => {
      if (accounts.length == 0) {
        setWalletAddress('');
        setBalance('');
        setIsConnected(false);
      } else {
        setWalletAddress(accounts[0]);
        const provider = new ethers.providers.Web3Provider(window.ethereum);
        const balanceNum = await provider.getBalance(accounts[0]);
        let balance = ethers.utils.formatEther(balanceNum);
        balance = balance.substring(0, 5);
        setBalance(balance);
      }
    })();
  });

  return {
    walletAddress: walletAddress,
    isConnected: isConnected,
    balance: balance,
    connectEthereum: connectEthereum,
  };
};
```

## Metamaskで開くボタン
Metamask deeplink  
https://metamask.github.io/metamask-deeplinks/#

[パラメータ有りiPhoneアプリのURLスキーム一覧](https://hibikanblog.net/blog-entry-820.html)


Solidity
===