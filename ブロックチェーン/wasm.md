wasm contract
===

[NimでWRC20を書くサンプル](https://github.com/status-im/nim-eth-contracts/blob/master/examples/wrc20.nim)

[Nimplay DSL for Nim contract](https://github.com/status-im/nimplay)

[WRC20 and Nim - the eWASM token challenge](https://discuss.status.im/t/wrc20-and-nim-the-ewasm-token-challenge/1167)

[CでWRC20を書くサンプル](https://gist.github.com/poemm/edc40a7103d1efc91f9fe0c7ba19ddf4)

## Rust
[Ink DSL for Rust contract](https://github.com/paritytech/ink)

[Ink documents](https://paritytech.github.io/ink-docs/)

[Astarのコントラクトをinkで書くドキュメント](https://docs.astar.network/build/smart-contracts/wasm/smart-contract)

[Nearのコントラクトをinkで書くドキュメント](https://docs.near.org/docs/develop/contracts/rust/intro)

[Nearサンプル](https://github.com/near-examples)

Solana
===
[Developing with C](https://docs.solana.com/developing/on-chain-programs/developing-c)

[ヘッダーファイル](https://github.com/solana-labs/solana/blob/master/sdk/bpf/c/inc/solana_sdk.h)

