[フレームワーク、Gnosis Safe](https://gnosis.io/safe/)

[aragon](https://aragon.org)

[superdao](https://superdao.co/)

[分散Github Radicle](https://radicle.xyz)

## ガバナンスとDAOの研究
https://twitter.com/officer_cia/status/1421239988574949376

### DAOによる分散型複合クエリの統治
Governing Decentralized Complex Queries Through a DAO  
https://arxiv.org/abs/2107.06790

### 集合知とブロックチェーン。テクノロジー、コミュニティ、社会実験
Collective intelligence and the blockchain: Technology, communities and social experiments  
https://arxiv.org/abs/2107.05527

### 分散型自律組織のプラットフォーム選択のための意思決定モデル。3つの産業のケーススタディ
A Decision Model for Decentralized Autonomous Organization Platform Selection: Three Industry Case Studies  
https://arxiv.org/abs/2107.14093

### 技術から社会へ。ブロックチェーンベースのDAOの概要
From Technology to Society: An Overview of Blockchain-based DAO  
https://arxiv.org/abs/2011.14940

### ブロックチェーンベースの金融のガバナンスはどの程度分散しているか。4つのガバナンス・トークンの配布による実証的な証拠
How Decentralized is the Governance of Blockchain-based Finance: Empirical Evidence from four Governance Token Distributions  
https://arxiv.org/abs/2102.10096

### ブロックチェーンガバナンスに関する体系的な文献調査
A Systematic Literature Review on Blockchain Governance  
https://arxiv.org/abs/2105.05460

### ブロックチェーンガバナンス-コラボレーションの新しい組織方法？
Blockchain Governance—A New Way of Organizing Collaborations?  
https://pubsonline.informs.org/doi/pdf/10.1287/orsc.2020.1379

### 信頼マシンとしてのブロックチェーン 信頼の問題とガバナンスの課題
Blockchain as a confidence machine: The problem of trust & challenges of governance  
https://www.sciencedirect.com/science/article/pii/S0160791X20303067

### ブロックチェーン経済におけるガバナンス。フレームワークと研究アジェンダ
Governance in the Blockchain Economy: A Framework and Research Agenda:  
https://pure.itu.dk/portal/files/82931178/Governance_in_the_blockchain_economy_a_framework_and_research_agenda.pdf


### ブロックチェーン技術と社会契約論におけるガバナンス。
Governance in Blockchain Technologies & Social Contract Theories:  
http://www.ledgerjournal.org/ojs/ledger/article/view/62/51

### FASTEN: スマートコントラクトを用いた公正で安全な分散型投票
FASTEN: Fair and Secure Distributed Voting Using Smart Contracts  
https://arxiv.org/abs/2102.10594

### Always on Voting: ブロックチェーン上での反復的な投票のためのフレームワーク
Always on Voting: A Framework for Repetitive Voting on the Blockchain  
https://arxiv.org/abs/2107.10571

### 重層的なブロックチェーンガバナンスゲーム
Multi-Layered Blockchain Governance Game  
https://arxiv.org/abs/2106.09518

### 行動変容のトークン化：持続可能な交通介入のためのブロックチェーン技術の最適化
Tokenising behaviour change: optimising blockchain technology for sustainable transport interventions  
https://arxiv.org/abs/2104.01852

### Bisq DAO：参加するためのプライバシーコストについて
The Bisq DAO: On the Privacy Cost of Participation  
https://arxiv.org/abs/2007.07048

## DAO（分散型自律組織）とティール組織（次世代型組織）の親和性
http://speakerdeck.com/abenben/dao-fen-san-xing-zi-lu-zu-zhi-toteiruzu-zhi-ci-shi-dai-xing-zu-zhi-falseqin-he-xing

## DAO：分散自立型組織の革命とその全貌に迫る
https://note.com/go_palermo/n/nf8a11eb944b5  
