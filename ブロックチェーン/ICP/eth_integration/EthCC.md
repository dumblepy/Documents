https://www.youtube.com/watch?v=yITg81LqJ7A

皆さん、こんにちは。  
インターネット・コンピューターについてお話ししましょう。  
私はDfinity財団の創設者でチーフ・サイエンティストのドミニク・ウィリアムズです。  
今日はインターネット・コンピュータについて、特にイーサリアムのエコシステムでどのように利用できるかに焦点を当ててお話しします。

hello everyone. thanks for coming to listen to.  
let's talk about the internet computer.  
I'm Dominic Williams, founder and chief scientist of the Dfinity foundation which is the primary developer of the internet computer.  
we're going to talk about internet computer today with a particular focus on how it can be used in the ethereum ecosystem.

---

Dfinityプロジェクトは、2015年に初期のイーサリアム・コミュニティから生まれたもので、絶対に何でも構築できるような規模で動作するワールド・コンピュータのビジョンを追求することを目的としていた。  
このプロジェクトは、その複雑さゆえに本番稼動までに長い時間を要した。  
Dfinityには、暗号解読者、コンピューターサイエンス研究者、エンジニアなどからなる大規模なチームがあることはご存知でしょう。

the Dfinity project actually came out of the early ethereum community in 2015 with a view to pursuing a vision of a World's computer that ran at scale.  
such that you could build absolutely anything on it.  
the project took a long time to get into production just because of the complexity that was involved.  
You may have heard Dfinity has a large team of cryptographers computer science researchers engineers and so on.

---

私たちがこの分野に足を踏み入れたのは、単に世界のコンピューター・ビジョンに関心があったからではなく、テクノロジーが私的なインフラからオープンなネットワークへと進化していく様を、ある種、一般的に理解していたからである。  
1990年代には、インターネットが台頭し、マイクロソフトやオラクルといった大企業によってコンテンツが作成される、一種の壁で囲まれた庭のようなネットワークである情報スーパーハイウェイというアイデアを推し進め始めた。  
結局、私たちは、TCP/IPプロトコルによって1つのオープンなグローバル・ネットワークに統合された、民間が所有し運営するルーティング・デバイスによって形成されるインターネットを手に入れた。  
今日では、クラウドサービス、データベース、ウェブサーバー、ミドルウェア、シティエンド・ファイアウォールなど、多くの人々が構築している伝統的なITがある。  
そして、いずれはオープンネットワークに移行していくと考えています。  
インターネット・コンピューターは、ICPプロトコルによって接続された個人所有のノードマシンによって構築され、サーバーレスの自律クラウドを形成する。

We got into this not just because of the world computer vision but because of a sort of General appreciation of the way that Tech History arcs from private infrastructure towards open Networks.  
and back in the 1990s we saw this the internet was emerging and people were proposing Alternatives Microsoft and Oracle began pushing this idea of the information superhighway which would be a kind of Walled Garden Network where the content would be created by these big corporations.  
In the end we got the internet which is formed by privately owned and operated routing devices that are combined by the tcpip protocols into one open Global Network.  
Today we have traditional IT which is where most people build and that's cloud services, databases, web servers, middleware, City ends firewalls, all of that stuff.  
and we think eventually people are going to move on to the open network.  
the internet computers formed by privately operated and owned node machines that are connected by the ICP protocols to form a serverless autonomous cloud.

---

ここに2つの概念があります。インターネット・コンピューター・ネットワークと、インターネット・アイデンティティと呼ばれるものだ。  
この2つによって、私たちが非常に興味を持っている他の2つのことが可能になる。ひとつはオープン・インターネット・サービスだ。オープン・インターネット・サービスとは、ダオ・テクノロジーを進化させたサービス・ナーバス・システムと呼ばれるものの制御下で、完全に自律的に実行されるサービスのことです。  
もうひとつは、私たちがソブリン・エンタープライズと呼んでいるもので、インフラが改ざんされないようになっていて、ファイアウォールで保護される必要がないものです。もちろん、クローズド・ソースのソフトウェアとクラウド・サービスで構築すれば、隠しバックドアやキル・スイッチは存在する。

there are two things. the internet computer network and something called internet identity.  
they enable two other things that we're very interested in one is open internet services an open internet service is a service which is fully autonomous runs under the control of something called a service nervous system which is an evolution of Dao technology.  
and the other is what we refer to as a sovereign Enterprise where the infrastructure is tamper-proof doesn't need to be protected by
firewalls and doesn't have any hidden back doors or kill switches which of course if you build with closed Source software and cloud services are present.

---

これがイーサリアム・コミュニティとイーサリアム・エコシステムにどのように役立つのか。  
インターネット・コンピューターは、イーサリアム分散化の最後のフロンティアに対処するのに役立ちます。  
今日、メタバースのようなものがある場合、実際には中央集権化された従来のITで動いています。大半のコンピューテイングとデータは、おそらくアマゾンのウェブサービス上にある。  
サービスは実際にはデータベース上で実行され、ウェブサーバーのmemcacheを使って高速化している。そこでハッカーたちが問題を起こしているのは明らかだ。検閲に強いわけではないので、開発者を信用するしかない。彼らはバックドアを持っていて、ユーザー名とパスワードでログインし、コードやデータを恣意的に変更することができる。  
私たちは、エコシステムがこの方向に進み、従来のITが自律的なインターネット・コンピュータのキャニスター・ソフトウェアに取って代わられることを望んでいる。  
そして、そのキャニスター・ソフトウェアは、DAOテクノロジーを進化させたようなサービス・ナーバス・システムの管理下に置かれ、エンド・ツー・エンドで全体が安全かつ分散化され、もちろん自律化される。

so how does this help the ethereum　community and the ethereum ecosystem.  
so the internet computer can help address the final frontier of ethereum decentralization.  
so today when you have something like a metaverse, in practice that runs on centralized traditional IT like all of the compute all of the data in vast
majority is probably on Amazon web services. and the services really running on you know databases, using a web server memcache to speed it up, and that's where all the hackers had that's where the problems typically go obviously it's not censorship resistant you have to trust the developers those
guys have a back door you know they can log in with their username and password and arbitrarily modify code and data and things like that.  
and we want the ecosystem to progress in this direction and replace traditional IT with autonomous internet computer canister software.  
and for that canister software to be placed under the control of a service nervous system which is a kind of evolution of DAO technology so end to end the whole thing secure and decentralized and of course autonomous.

---

では、インターネット・コンピューターはどのような仕組みになっているのでしょうか？  
独立したオペラが所有・運営するノードマシンがあると書きましたが、これは世界中のデータセンターのノードプロバイダーと呼ばれる人々によって運営されています。  
クラウドインスタンスではなく、物理的なハードウェアをベースにしています。ビットコインがハッシュにASICを使うのと同じです。そして実は、このネットワーク・モデルはPoUWが適用されているだ。  
インターネット・コンピュータ・プロトコルのICPは、これらのノードを組み合わせて効率的なサブネット・ブロックチェーンを形成する。  
これらのサブネット・ブロックチェーンを追加するごとに、キャニスター・スマート・コントラクトを実行するための容量が増える。そして、これらのサブネットは1つのサーバーレス・クラウド環境に統合される。  
ネットワークは、新しいサブネット・ブロックチェーンやインファント・アイテムを追加することで容量を増やすことはできない。

So just quickly how does the internet computer work.  
I mentioned that you have these independently Opera owned and operated node machines.  
they're run by people we call node providers from data centers around the world so this thing isn't based on cloud instances it's based on physical Hardware a bit like Bitcoin uses ASIC for hashing. And actually the moniker applied to the network model is proof of useful work.  
so the internet computer protocol ICP combines these nodes to form efficient subnet blockchains.  
Each of these additional subnet blockchains adds more capacity for running canister smart contracts and we'll come back to those in a second. And then these subnets are combined into one serverless cloud environment I mean more specifically serverless autonomous cloud in the environment.  
That actually has unbounded capacity the network can't just increase the capacity by adding new subnet blockchains and infant item and that's what the internet computer is.

---

まさにブロックチェーンの新しい形だ。  
2009年にはビットコインが登場し、2015年にはイーサリアムが登場した。そして、インターネット・コンピュータが登場すると、まったく新しい自律型クラウドを提供することになった。  
インターネット・コンピューターには、非常に興味深い技術的能力がある。  
そのひとつがネイティブなマルチチェーン機能で、これはチェーンキー暗号と呼ばれるものに由来している。  
これはチェーン・キー暗号と呼ばれるものから派生したもので、インターネット・コンピューターがブリッジズを介さずに他のチェーンと相互作用することを可能にする。  
これは、2014年に発表されたイーサリアム・ウェル・コンピューター・ビジョンを実現する重要な方法のひとつです。  
クールなのは、インターネット・コンピューター上のキャニスターが他のブロックチェーン上の取引に署名できることだ。  
もちろん、キャニスター自身が巧妙な暗号化を行っているわけではない。インターネット・コンピュータ環境が提供するAPIと相互作用しているだけだ。  
例えばECDSA公開鍵を作成し、仮想秘密鍵を使ってメッセージに署名することができる。しかし、秘密鍵は存在せず、署名はチェーン・キー暗号と呼ばれるものを使ってインターネット・コンピューター自体によって行われる。  
イーサリアムとの完全なネットワーク・レベルの統合は、現時点でもイーサリアムのトランザクションに署名することはできますが、ユーザー・エクスペリエンス自体によって転送する必要があります。  
ネットワーク・レベルの統合は今日完全に機能しており、チェーン・キー・ビットコインと呼ばれる、キャニスターが処理できるビットコインの双子のようなものを生成します。  
また、非常に安価に、1秒程度でビットコインを確定できるため、DeFiやソーシャルメディアなどでビットコインを利用することができる。  
双方向のICPとETHは近々実装される予定だが、ガスやその他の理由でネットワークレベルで実装するのはかなり難しい。  
インターネット・コンピューター上のキャニスター・スマートコントラクトがイーサリアムのスマートコントラクトを直接呼び出したり、その逆ができるようになります。ERC20トークンをインターネット・コンピューターに持ち込んで、オーダーブック取引所で処理することはできる。  
しかし、それ以上に高度なことはできません。

It's really a new form of blockchain.  
you know 2009 we saw digital gold emerge Bitcoin, and 2015 we've got some more contracts with ethereum. And when the internet computer launched it provided something completely new autonomous cloud.  
The internet computer has some very interesting technical capabilities.  
The one of these is native multi-chain capabilities and this derives from something called chain key cryptography.  
It allows the internet computer to interact with other chains without there being Bridges just using trustless cryptography.  
This is one of the key ways we think that the original ethereum well computer vision from 2014 can be realized.  
What's cool is that canisters on the internet computer can sign transactions on other blockchains.  
And of course canisters aren't doing any clever cryptography themselves. They're just interacting with APIs that the internet computer environment provides them with.  
They can create for example ECDSA public keys and then sign messages using the virtual private key. But there isn't a private key the signings done by the internet computer itself using this thing called chain key cryptography.  
Full Network level integration with ethereum's coming you can still sign ethereum transactions at the moment but you have to forward them by the user experience itself.  
One Network level integration is fully working today, and it produces something called chain key Bitcoin as a kind of Bitcoin twin that can be processed by canisters.  
and a very very cheaply has a one the finality about one second enables you to use Bitcoin where like DeFi or social media and so on.  
Bidirectional ICP and ETH is coming soon it's quite challenging to implement at the network level for a bunch of reasons related to gas and so on.  
But it's coming and it's going to enable canister smart contracts on the internet computer to directly call into ethereum smart contracts and vice versa it's obviously a very basic level. You could bring erc20 tokens onto the internet computer and process them in an order Book Exchange.  
But you cannot to do too much more sophisticated things than that too.

---

キャニスター・コードとは何かというと、まず一般的なビジョンのようなものだ。それは、ソフトウェアが世界を蝕んでいるのと同じように、すべてがソフトウェア上で動くようになるということだ。小さな契約はソフトウェアを食べるようになる。  
つまり、最終的にはワールド・コンピューターが誕生するということだ。そして、すべてがスマート・コントラクトを使ってコード化される。そしてもちろん スモール・コントラクトを使えば、データベースは必要ない。つまり、データはスマート・コントラクトの中に保存される。これがスモール・コントラクトの興味深い特性のひとつであり、スモール・コントラクトと従来のソフトウェアとの違いでもある。データは外部のデータベースではなく、コントラクト自体に保存されます。そして、それを拡張することができれば、データベースは不要になり、ファイアウォールも不要になります。

So canister code what is it firstly we have a sort of General Vision again. Which is that just in the same way the software is eating the world like everything's going to run on software. Small contracts are going to eat software.  
And that means eventually there'll be a world computer. And everything will be coded using smart contracts. And of course when you code with small contracts you don't need a database right. I mean the data is stored inside the smart contract. That's one of the interesting properties of small contracts and the differences between small contracts and traditional software. The data is actually stored inside the contract itself rather than in an external database. And if you can scale that up you don't need databases anymore, don't need firewalls of course because they're tamper-proof.

---

2015年に巻き戻って、スマート・コントラクト・ソフトウェアが登場した。スマート・コントラクトは新しい形式のソフトウェアで、素晴らしい特性を備えています。つまり、イーサリアム上で動作する改ざん防止システムであり、ファイアウォールに囲まれていても、その必要はありません。  
もちろん止められないし、自律的だ。つまり、人や組織から独立して存在するソフトウェアを作ることができる。  
スマート・コントラクトはトークンを処理することができ、独立した当事者間のダイナミックなコラボレーションを可能にするような方法で非常に簡単に構成することができる。スマート・コントラクトが正確にどこに存在するのか、誰も知らない。どこにも存在せず、サイバースペースに存在するだけだ。  
サイバースペースでもいい。定義による。

So rewinding to 2015 what was introduced with smart contract software. Well it's actually a new form of software and they have all of this you know amazing smart contract have all these amazing properties. So they're tamper-proof systems running on ethereum on surrounded or protected by firewalls they don't need it.
They're Unstoppable of course they're autonomous which means you can create software that exists independently of a person or organization.  
Smart contracts can process tokens that can very easily composable in ways that enable kind of dynamic collaboration between independent parties and of course they're borderless. No one even where does a smart contract live exactly. It doesn't live anywhere it just lives in cyberspace.  
Or cyberspace depending on your definition.

---

キャニスターはスマート・コントラクトの新しい形態だ。キャニスターは、2015年当時我々が持っていたものを一般化することを目的としている。  
つまり、ブロックチェーンのスピードではなく、ウェブのスピードで高速に動作する。スケーリングも可能で、インターネット・コンピューター上でのスケーリングを、従来のIT技術を使うよりも簡単にすることを目指しています。  
ブロックチェーンはネイティブなマルチチェーンで、非常に効率的です。これはカーボンフットプリントのようなものにとって重要ですが、コスト面でも重要です。インターネット・コンピューターの計算コストは、従来のITに近づいています。また、HTTPの処理も可能で、一元化されたユーザー・エクスペリエンスを提供したいのであれば、それはもちろん不可欠です。

Canisters are a new form of smart contracts. And they are aim to sort of generalize what we had back in 2015.  
So the fast they sort of run at web speed rather than blockchain speed. They can scale we aim to make scaling on the internet computer easier than it is on traditional using you know traditional IT.  
It's a natively multi-chain it's highly efficient. It's important for things like the carbon footprint but also cost. Compute costs on the internet computer are approaching traditional IT. And they can process HTTP and that's of course essential if you want to accredits a centralized user experience.

---

これらは一種の純粋なウェブ3を可能にする。  
ここにインターネット・コンピューターがある。画面はオープンチャットと呼ばれる、インターネットコンピュータ上に構築されたサービスを示している。  
実際には、オープンチャットは何千ものキャニスターで構成されています。ユーザーはウェブブラウザを使って、HTTP経由でこれらのサービスとやりとりする。そして、伝統的なITは全く使われていない。

They enable a sort of pure form of web3.  
Here we have the internet computer. Obviously a very big approximation with the screens showing something called open chats the service built on the internet computer.  
Actually open chat is composed of many thousands of canisters. So there's something to bear in mind. But the user is interacting by the web browser with these Services over HTTP. And there's no involved traditional IT.

---

先に述べたインターネット・アイデンティティは、RPCトランザクション・セッションを作成する。つまり、ウォレットは関係ない。個々のトランザクションに署名したり、台帳のウォレットで承認したりするわけではありません。その代わりに行われるのが、インターネット・アイデンティティというものを使ってログインすることです。  
もちろん、あなたはセッションを作成するための鍵を持っていますが、インターネット・コンピューターはこれが本当に興味深い点の1つで、インターネット・コンピューター自体が1つのマスター48バイトのチェーン・キーを持っています。この48バイトのチェーンキーを知っていれば、認証することができます。この48バイトのチェーン・キーを知っていれば、ネットワークがどんなに大きくなっても、そのネットワークとのやりとりの一つひとつの正しさを検証することができる。  
もちろんチェーン・キーは、ハッシュ・パスや閾値BLS署名など、さまざまなものの集合体だ。しかし、あなたが知っている必要があるのは、このチェーン・キーによる48バイトだけで、インターネット・コンピューターとのやりとりを検証することができます。  
そしてそのやりとりが検証されれば、実際のチェーンも正しく実行されることがわかる。

Internet identity which I mentioned earlier is creating RPC transaction sessions. So there's no wallet involved. You're not like signing every individual transaction and authorizing it with your ledger wallet. Instead what's happening is that you log in if that's the right word you credit session using this thing called internet identity.  
And you of course have a key that's used to create your session but the internet computer, this is one of the really interesting aspects of it the internet computer itself, has a single Master 48 byte chain key. Right so if you know this single 48 byte chain key, you can authentic. You can verify the correctness of every single one of your interactions with the network no matter how big it gets.
And of course a chain key is  an assembly of a bunch of things like you know hash paths and threshold BLS signatures and so on. But all you need to know is this 48 by Chain key and you can verify your interactions with the internet computer.  
And if your interactions verify you know the actual chains run it correctly too.

---

最後に、これはサービス・ナーバス・システムです。全ネットワーク、全インターネット・コンピューターは、このサービス・ナーバス・システムと呼ばれるものの制御下で動いている。  
これはDAO・テクノロジーの進化と同じようなものです。  
これはサービス・ナーバス・システムの工場なのです。そして、更新が必要なキャニスターがあれば、もちろん、そのコントローラーを正しく設定するだけで、キャニスターコードを自律させることができる。  
そうすれば、もう誰も更新することはできない。  
しかし、それが複雑なシステムであれば、アップグレードや修正が必要になる。だから、それを自律化したいのであれば、サービス・ナーバス・システムに割り当てることになる。

The last thing I mentioned this thing is the service nervous system. The whole network, the whole internet computer runs under the control of this thing called the network nervous system.  
This is the same kind of evolution of DAO technology.  
It's a factory for service nervous systems. And if you have canisters that need to be updated, of course you can make canister code autonomous just by setting its controllers to know right.  
Then nobody can update it ever again.  
But if it's a complex system you're going to need to upgrade it so and fix things. So if you want to make that autonomous, you assign it to a service nervous system.

---

インターネット・アイデンティティは、実際にはこれら3つの技術の組み合わせによって作られている。  
トラステッド・プラットフォーム・モジュールとは、携帯電話やノートパソコンのような最新の機器に搭載されているチップのことだ。  
このチップは鍵を保持しており、その鍵を使ってチップ内のメッセージに署名することができる。チップを壊して鍵を取り出すことはできない。  
WebAuthnと呼ばれるプロトコルは、ウェブブラウザ内のコードをTPMチップに接続するもので、もちろんインターネット上のコンピュータには暗号の束がある。  
例えばoc.appのようなオープンチャットのURLにアクセスし、アプリの起動をクリックし、インターネットIDを選択し、認証する。指紋センサーでも顔認証でも、TPMSを作動させるものなら何でもいい。そして、あなたの鍵が署名しているTPM内にセッションが送信される。複数のデバイスが接続されている場合は、インターネットIDを入力すれば、すぐに使えるようになる。  
つまり、Web3サービスに対して自分を認証する別の方法なのだ。とても簡単だ。安全です。キーカードが盗まれた場合、様々な方法でキーの紛失を防ぐことができますが、例えば、インターネットIDに複数のデバイスを追加したり、シードフレーズ付きのリカバリーキーを追加したりすることができます。


internet identity does a bunch of things it's actually created by a combination of these three Technologies TPMS.  
A trusted platform module, it's a chip that's inside modern devices like a phone and a laptop.  
That holds keys and it can sign using those keys to sign messages inside the chip. You can't break the chip open to get the key out.  
A protocol called WebAuthn which connects code inside the web browser to those TPM chips and of course a bunch of cryptography on the internet computer.  
This is what it looks like you go to the open chat URL for example oc.app, click launch app, choose your internet identity,  authorize. It could be whatever you want right fingerprint sensor or face ID that's activating the the TPMS. Then the sessions being sent inside the TPM where your key is signing it. If you have multiple devices attached your internet identity and boom you're in.  
So it's a different way of authenticating yourself to web3 service. It's very easy. It's secure. Wen your key card is stolen you prevent there are various ways you can stop yourself losing the key but for example by adding multiple devices to your internet Identity or if you want adding a you know a recovery key with a seed phrase.

---

これらの異なるピースをすべて組み合わせれば、より強力なエンド・ツー・エンドの分散型自律型ウェブ3サービスを作ることができる。  
今言ったオープンチャットは、ネットワーク上のチャットアカウントとウォレットで100回実行されます。つまり、完全なソーシャル・ファイです。メッセージでサトシを送ることができます。  
もちろん、すべてのメッセージはブロックチェーンのトランザクションであり、ブロックチェーンの状態の一部として保存されます。  
そしてこれは、実は最初のオープン・インターネット・サービスだった。4月に本稼働し、サービス・ナーバス・システムによって完全に制御されている。つまり、オープンチャットの更新はすべてこのサービス・ナーバス・システムに提案される。つまり、オープンチャットのアップデートはすべてこのサービス・ナーバス・システムに提案され、それが通れば適用される。

When you combine all of these different pieces you can create these end-to-end decentralized autonomous web3 services that are more powerful.  
So open chat I just mentioned runs 100 on the network chat accounts also wallets. So it's kind of full social fi. You can send satoshi's with a message.  
There's no wallet or anything involved every under the skin of course every message is a blockchain transaction and in you know the thing is stored as part of the blockchain state.  
And this was actually the first open internet service. It kind of went live in April and it's fully controlled by a service nervous system which means any updates to open chat are propose to this service nervous system. And that it applies them if they go through.

---

ホット・オア・ノットは、最近、サービス・ナーバス・システムシステムの制御下で実行されるオープンなインターネットサービスに移行した別のものです。  
これは完全にトークン化されたTick Tockのようなもので、ブラウザに配信される。ある種の独創的な分散型経済を生み出している。つまり、ユーザーはホットトークンに賭けるわけではなく、動画がバイラルになるかどうかにホットトークンを賭けるのです。そして、バイラル動画のクリエイターはこのトークンによって報酬を得る。そして最終的には、広告主はこのトークンで支払うことになる。  
広告をサービスに載せるために、それがトークンの価値を高め、経済を生み出すのだ。  
このサービスは、ユーザーをチームの一員にし、ユーザーからこのようなバーチャルな労働力を生み出すように、ユーザーをファウンドレイズするように設計されている。  
それがウェブ3の未来だと私たちは考えています。  
繰り返しますが、全体はSNSによってコントロールされています。

The Hot Or Not is another one that recently transitioned to becoming an open internet service running under the control of a service nervous system.  
It's fully like a fully tokenized Tick Tock delivered into the browser. It's creating like a sort of ingenious decentralized economy. So users bet it's not actually hot tokens ,but we'll go with that for the second for now, uses bet hot tokens on whether videos will go viral users help with content moderation. And then the creators of viral videos get rewarded by these tokens. And eventually advertisers are going to have to pay with these tokens.  
In order to get their ads on onto the service that's what drives the value of the tokens and creates the economy.  
It's designed to be a service that foundraizes its users like makes its users part of the team and creates this sort of virtual industrious workforce from the users.  
That's what we think is the future of web3.  
Again the whole thing's controlled by an SNS.

---

最後の例はICデックス。  
これはアルファ版で、まだオープンなインターネットサービスではない。  
しかし、完全にインターネット・コンピューター上で動作する。  
スマート・コントラクトによって作成された完全な先物取引所だ。  
そして明らかにハッキングを証明し、透明性があり、すぐに自律化される。FTXのような、サム・バンクマン・フリードマンのような人物によって作られた秘密のバックドアによって、630億ドルのチート・コードなどが与えられ、すべての資金を吸い上げることができるような中央集権的な世界に代わるものです。  
これが未来だと思う。  
スマート・コントラクトによって作成された継続的なダブル・オークション取引所やオーダーブック取引所では、ユーザー・エクスペリエンスさえもスマート・コントラクトによって作成され、バックドアは一切存在しません。そして、取引所へのアップデートはすべて、サービス・ナーバス・システムに提案されたサービスを通過し、適用される。

The final example IC Dex.  
This is an alpha. it's not an open internet service yet.  
But it's runs entirely on the internet computer.  
It's a full order Book Exchange created by a smart contract.  
And obviously it's hack proof, and transparent, soon, it'll be autonomous. And so you know it provides an alternative to the centralized world where you have something like FTX where can be a secret back door created by someone like Sam Bankman Freedman which gives them a 63 billion dollar cheat code Etc and allows them to sort of suck all the money out.  
This we think is the future.  
Continuous double auction exchanges and order book exchanges created by smart contracts where even the user experience is created by a smart contract where there's absolutely no back door whatsoever. And all of the updates to The Exchange pass through the service you know proposed to the service nervous system and then applied.

---

これらの事例がすべて通過してきたことを考えてみよう。  
非常にクールで洗練された複雑な機能を持ちながら、小さな技術チームによって作られたものもある。最初の2つは、それぞれ3人の開発者からなるコアチームだと思う。  
エクスチェンジはもう少し大きい。10人以上はいると思いますが、それでもこれらのチームは小規模です。  
クラウドもデータベースサーバーもなく、インターネット上のコンピューターだけで動いている。  
正式版ではインターネット向けのビデオストリーミングができます。ホット・オア・ノットは、今はまだそうではないかもしれませんが、将来的にはそうなると思います。  
オープンチャットは非常に複雑なメッセージングサービスで、チャットアカウントは実際には暗号ウォレットです。暗号化されたチャットメッセージを送ることができ、しかもファイアウォールもセキュリティチームも必要ない。  
スマート・コントラクトがソフトウェアの新しい形としていかに革命的であるかを物語っていると思います。

So considering all of these examples have just been through.  
Some things we should know they're very cool very sophisticated got complex functionality and yet they were built by tiny Tech teams. The first two I think have core teams of three developers each.  
The Exchange is a bit larger. There's like 10 plus but nonetheless these teams are small.  
There's no Cloud, no database servers, they're running exclusively on the internet computer.  
You can do video streaming for the internet in Productions. I think "Hot or Not" may not be at the moment but it's it's coming.  
No files right I mean if you think about open chat is very complicated messaging service and chat accounts are actually crypto wallets. And you can send crypto chat messages and yet you don't need firewalls and you don't need a security team.
So I think that just speaks to how revolutionary smart contracts are as a new form of software.

---

インターネット・コンピューターに導入されつつあるのが、サブネットのローカライズだ。現在、サブネットは世界中のノードマシンを結合しています。つまり、異なるノード・プロバイダー、異なるデータ・センター、異なる地理的位置、異なる管轄区域のノード・マシンというルールです。これが決定論的分散化と呼ばれるもので、レプリケーションを最小限に抑え、コストを非常に低く抑えるために使われています。  
ローカライズされたサブネットは、同じ地理的地域にあるノード・マシンを結合する。  
ここで問題になるのは、今後、社会の基盤やアイデンティティを支える基本的なコンピュート・インフラを米国企業に依存すべきかどうかということだ。  
例えば、次世代IDがVisaやMastercardのようなものになった場合、私たちはノーと言うでしょう。主権社会は、他の国家がキルスイッチやバックドアを持っているかもしれないデジタル基盤に依存することはできない。  
クラウド・コンピューティング・サービスやクローズド・ソース・ソフトウェア、シングルサインオン・サービス、さらにはセキュリティ・ハードウェアを利用することで、そのようなことが可能になる。セキュリティ・ハードウェアにはトロイの木馬やバックドア、そして トロイの木馬やバックドア が付いていることが何度も証明されている。

Something that's coming on the internet computer is the localization of
subnets. Currently subnets come to combine node machines from all over the world. So you know the rule is node machines from different node providers, in different data centers, in different geographers, and different jurisdictions. Which is what we call the term deterministic decentralization which is used to minimize the  replication involved and to keep the cost very low.  
Localized subnets will combine node machines that are in the same geographical locality.  
The question here is going forward and we're looking at things like you know fundamental compute infrastructure that runs the foundations of society and identity and things like that should we be depending on U.S corporations.  
For example should you know next Generation identity turn out to be something like Visa or Mastercard and we say no. And that Sovereign societies cannot depend on digital foundations in which other states might have kill switches and back doors.  
And that's what you get when you use Cloud Computing Services closed Source software single sign-ons Services even security Hardware. So security Hardware is proven time and time again to actually come with Trojans and back doors and things like that.

---

キャニスター・スマート・コントラクトのコードからイーサリアム・スマート・コントラクトへの双方向の呼び出し、またその逆も可能であることは述べた。  
つまり、これらの機能はすべて活用できるか、少なくともまもなく、あるいはイーサリアムから非常に簡単に活用できるようになります。  
つまり、インターネット・コンピューター上でユーザー・エクスペリエンスを構築し、イーサリアム上でDeFi RLSを実現できるのだ。  
さらに、他のクールな機能の1つにhttpsのアウトコールがある。インターネット・コンピュータのキャニスター・スモール・コントラクトは、コンセンサスを通過するコールを行うことができ、その結果は正規化されるため、coinbaseやさまざまな取引所からキャニスター・スモール・コントラクトに価格フィードなどを取り込むことができます。  
これにより、中央集権的なオラクル企業に依存することなく、ウェブ2インテグレーションとソブリン・オラクルの機能を作成することができます。

So I mentioned how you know you're going to get bi-directional calling from canister smart contract code into ethereum smart contracts and vice versa.  
That means that all of these capabilities can be leveraged or at least shortly or it'll be possible to leverage them very easily from ethereum.  
So you could build a user experience on the internet computer and have DeFi RLS on ethereum.  
In addition one of the other cool features is a https out calls. The internet computer canister small contracts can make our calls that go through consensus and the results are normalized so you can pull things like price feeds into a canister small contract from someone like coinbase or even from a variety of exchanges.  
And that enables you to create web 2 Integrations and then Sovereign Oracle functionality without relying on a centralized Oracle company.

---

最後になるが、インターネット・コンピューターは、完全なAIスマート・コントラクトへの道筋を持つAIコンピュート・ユニットと呼ばれるものを確実にサポートするようになるだろう。  
つまり、AIはウェブ3の絶対的な基礎となるものであり、社会の将来にとってどれほど重要なものになるか、誰も過小評価してはならない。  
将来的には、AIがすべてのデータを分析することを望むようになるでしょう。  
メタバースのようなものを考えた場合、多くの環境は実際にAIによって生成される可能性が高い。  
AIは Stabel Diffusion や大規模な言語モデルだけでなく、圧縮データのようなこともできる。  
重要なのは、これらの機能すべてに ウェブ3からアクセスできるということです。  
つまり将来的には、イーサリアムのスマートコントラクトの中から、インターネット・コンピュータのAIコンピュート・ユニットにコールをかけて、モデルをスピンアップさせたり、学習データに向かわせたり、プロンプトを与えてレスポンスを得たり、それを処理したりすることが可能になる。  
完全なAIスマートコントラクトはまだ先ですが、インテグラル・ニューラル・ネットワークと呼ばれるものをやっている人たちと協力して、AIモデルに変換を適用して決定論的にすることができます。  
医療用の大規模な言語モデルのように、ソフトウェアが何であるか、パラメータが何であるか、学習データが何であるかを絶対的な確信を持って知る必要があり、ユーザーが特定のプロンプトを入力した場合、推論が実行され、特定の応答が返されたことを絶対的な確信を持って知る必要があるような場合に、非常に便利です。

Lastly but not least internet computers are going to surely sort of add support for something called AI compute units with a pathway to full AI smart contracts.  
So AI is going to be absolutely fundamental to to web3 and nobody should underestimate how important it's going to be to the future of society.  
In the future you know AI is going to be want to people are going to want to have AI analyzing all of the data.  
If you think about something like a metaverse very likely a lot of the environment will actually be generated by AI which will be fantastic.  
AI isn't just like "the stable diffusion" and large language models, you can even do things like compressed data.  
What's important is that all of this functionality is accessible from web3.  
So in the future it'll be possible from within an ethereum smart contract to make a call into an internet computer AI compute unit spin up a model, even direct it towards some training data, give it a prompt get a response process it do it do whatever.  
Full AI smart contracts further off but working with people who are doing something called integral neural networks and you can apply transforms to AI models to make them deterministic.  
That's how it's done if you're curious and they'll be very useful for things like you know Medical large language models where you need to know with absolute certainty, what the software is, what the parameters are, what the training data is, and the users need to know if they Supply a particular prompt they need to know with absolute certainty that an inference was performed and produced a specific response.

---

だから私たちの使命は、基本的に人文科学を扱うサービスやシステムをワールド・コンピューターにできるだけ多く取り込むことです。  
この業界ではインターネット・コンピューターについてあまり耳にしませんが、ネットワーク上のコンピュートはどんどん増えています。だから現在、1秒間に約250000ETH相当のトランザクションを実行している。  
実際に実行されている命令の数を見ると、その数は増加しています。  
だから成長しているし、うまくいっている。ここにいる誰かが興味を持ってくれることを期待している。  
インターネット・コンピューターを見て、分散化の次の段階を推し進めるために、私たちと一緒に参加しましょう。  
ありがとう。

So our mission is to essentially get as much of Humanities services and systems services onto World computer.  
You don't hear much about the internet computer within the industry but it's the compute on the network is growing. So currently it's performing about 250000 ETH equivalent transactions a second.  
When you actually look at the number of instructions that are being run.  
So it's growing and it's doing well and I hope you know anyone here is interested I'll be around for a while.  
Takes a look at the internet computer and sort of joins us in pushing and then the next phase of decentralization.  
yeah thank you.
