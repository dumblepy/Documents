## ckETH

### サイケデリックの取り組み

https://medium.com/terabethia-bridge/terabethia-bridging-contracts-assets-across-ethereum-and-the-internet-computer-dc45797de1dd

### IC Light House の取り組み

https://github.com/iclighthouse/icETH


GoerliのETHtestトークンをIC上に持ってくることに成功した  
https://medium.com/@ICLighthouse/iclighthouse-multi-chain-asset-hub-guide-29666c754f7f

しきい値ECDSAを使ったオフチェーンブリッジなしの仕組み、ic Router  
https://medium.com/@ICLighthouse/iclighthouse-two-week-updates-jun-12-jun-25-2323e9612674

### Dfinityの取り組み


ckETH  
https://github.com/dfinity/ic/tree/master/rs/ethereum/cketh

```
ICPのイーサリアム統合時期情報。
このフォーラムでは「イーサリアム統合のフェーズ1に基づいてckETHとckERC-20を構築することは、今年の第4四半期に完了する予定」との回答。

延期せずに完了させて欲しいところ！

https://twitter.com/Eikichi_WLI/status/1685285952875601920
```
