# Chain Key Cryptography: The Scientific Breakthrough Behind the Internet Computer

チェーン・キー暗号インターネット・コンピュータを支える科学的ブレークスルー

https://medium.com/dfinity/chain-key-technology-one-public-key-for-the-internet-computer-6a3644901e28

_チェーン・キー暗号は、インターネット・コンピュータを構成するノードをオーケストレーションする一連の暗号プロトコルである。_

インターネット・コンピュータは、世界初のウェブスピード、インターネット規模のパブリック・ブロックチェーンであり、スマートコントラクトにより、エンドユーザーのブラウザに直接インタラクティブなウェブコンテンツを安全に提供することができる。これにより、開発者、組織、起業家は、安全で自律的なソフトウェア・プログラムを公共のインターネット上に直接構築し、展開することができる。DFINITY 財団の長年にわたる献身的な研究開発により、この野心的なビジョンが実現しました。この野心的なビジョンは、斬新な暗号技術とコンピュータ・サイエンスを組み合わせることで、オンチェーンで無制限のストレージと計算を提供するブロックチェーンを作り上げたものです。

多くの世界トップクラスの暗号学者、研究者、エンジニアからなる熟練したチームが、分散化を可能にし、困難な技術的課題を解決するために、この取り組みの先頭に立ちました。私たちは協力して、先進的なコンセプトを現実の世界に持ち込み、コンピューティングの世界全体に多大な影響を与える最先端技術を実装してきました。

インターネット・コンピューターは、最終的には数百万ノードの規模で稼働する。チェーン・キー暗号は、これを可能にする基本的なブレークスルーのひとつである。これは、インターネット・コンピュータを構成するノードをオーケストレーションする一連の暗号プロトコルで構成されている。チェーン・キー暗号の最も目に見える革新は、インターネット・コンピュータが単一の公開鍵を持つことである。これは、スマートウォッチや携帯電話を含むあらゆるデバイスが、インターネット・コンピュータからの成果物の真正性を検証できるという大きな利点である。これとは対照的に、イーサリアムのような従来のブロックチェーンではこのようなことは不可能であり、小さなアーティファクトの検証には膨大なデータが必要となる。

単一の公開鍵は氷山の一角にすぎない：チェーン・キー暗号は、インターネット・コンピュータを動かし、その運用を可能にするエンジンなのだ。これにより

- 新しいノードを追加して新しいサブネットを形成し、ネットワークを無限に拡張します。
- 故障やクラッシュしたノードを、停止することなく新しいノードと交換する。
- サブネット内の多くのノードが故障した場合でも、サブネットを復活させる。
- インターネット・コンピューター・プロトコルをシームレスにアップグレードし、ネットワークのバグ修正や新機能の追加を可能にする。

## The Internet Computer

チェーン・キー暗号を理解するために、まずインターネット・コンピュータを詳しく見てみよう。インターネット・コンピュータは、インターネット・コンピュータ・プロトコル（ICP）を実行する世界中の多数のノードで構成されている。このプロトコルはこれらすべてのノードを接続し、これらのノード上でキャニスター・スマート・コントラクトの実行をオーケストレーションする。ソフトウェアキャニスターはコードとステートの両方を含むバンドルで、開発者によってインターネットコンピュータ上にデプロイされる。アプリケーションは単一のキャニスタで構成されることもあれば、キャニスタのサイズには制限があるため、相互に作用し合う多数のキャニスタで構成されることもあります。キャニスタは、Web ページ、エンタープライズ・システム、DeFi ユースケース、さらにはオープン・インターネット・サービスなど、あらゆるものを実装できます。

ユーザがインターネット・コンピュータにデプロイされたアプリとインタラクトすると、ICP を介してメッセージがキャニスタに送信されます。次に、インターネット・コンピュータはそのメッセージの入力に対してキャニスタを実行し、最終的にユーザがクエリバックできる出力メッセージを計算します。メッセージを実行することで、キャニスタの状態はそのコードで規定されたとおりに更新されます。

インターネット・コンピュータによって計算された結果のロバスト性と正しさを達成するために、各キャニスタは単一のノードではなく、複数のノードで実行される。ノードはハードウェアの故障でクラッシュしたり、インターネットから遮断されたり、さらに悪いことに意図的に間違った結果を報告したりする可能性があります。そのため、インターネット・コンピュータ・プロトコルは、キャニスターが複数のノードで実行され、これらのノードがどのメッセージをどの順序で処理するか、また各計算結果の値について合意することを保証します。これにより、結果の正しさが保証され、ネットワークはあらゆる種類の障害に対して堅牢であり、キャニスターは永遠に実行できる。

## Chain Key Cryptography

インターネット・コンピュータのユーザに、送信された結果の正しさを保証するために、ユーザが受信したメッセージが本当にインターネット・コンピュータから発信されたものであり、他の場所から発信されたものではないことを確認できることがさらに必要である。単一のノードは信頼されないため、メッセージは、ユーザが結果を問い合わせるキャニスタをホストするすべてのノードによって共同で署名される必要があります。そこで登場するのがチェーン・キー暗号です：すべてのノードは、要求された結果を含むメッセージに共同で署名できる秘密鍵の共有を受け取ります。こうして作成された署名は、インターネット・コンピュータの公開鍵のみを使用して検証することができる。ここでの優れた点は、インターネット・コンピュータが何百万ものノードと何千ものサブネットによって支えられているにもかかわらず、ネットワークが必要とするのは、サブネットからの結果を検証するための単一の公開鍵だけだということです。

インターネット・コンピュータの規模を拡大するために、すべてのノードがすべてのキャニスターを実行できるわけではない。そのため、ノードはいわゆるサブネットに分割され、異なるキャニスターはこれらのサブネットに分散されます。より正確には、キャニスタをインターネット・コンピュータにアップロードすると、それがインストールされるサブネットが割り当てられ、そのサブネットに参加するノードでのみ実行されます。メッセージを認証するために、各サブネットは独自の公開鍵を持っている。サブネットのすべてのノードは、そのサブネットの公開鍵に対応する秘密鍵を共有する。いわゆる閾値署名スキームを使用し、十分な数のノード（必要な閾値以上）が合意すれば、それぞれの鍵シェアを使用してメッセージに共同で署名することができる。メッセージの署名は、サブネットの公開鍵を使ってユーザーによって検証される。すべてのサブネット公開鍵は、順番にインターネット・コンピュータの単一の公開鍵で署名される（これがどのように行われるかは後述する）。

要約すると、チェーン・キー暗号は、単一の 48 バイトの公開鍵（インターネット・コンピュータの公開鍵）を知っていれば、レスポンスの検証とインターネット・コンピュータの計算に十分であることを可能にする。これとは対照的に、イーサリアム上のスマートコントラクトの結果を検証するためには、オープンなイーサリアムクライアントは 400 ギガバイトをダウンロードする必要がある。さらに悪いことに、必要なダウンロード・サイズは時間とともに直線的にしか大きくならない：1 年前には 200 ギガバイト程度だった必要サイズは、1 年で倍増している。

## Scaling out infinitely

規模が大きくなると、インターネット・コンピュータが何千ものサブネットを持つようになり、これは各サブネットに対応する何千もの個別の公開鍵も持つようになることを意味する。これらの公開鍵と対応する秘密鍵の共有は、容量を増やすためにノードが追加され、新しいサブネットが形成され、さまざまな理由でノードが故障して交換され、通信をブロックしたり、通信を操作しようとしたり、ノードを危険にさらして任意の方法でプロトコルから逸脱させようとする攻撃者がいる環境において、安全な方法で生成、管理、維持される必要がある。

ノードが共同でメッセージに署名を生成するには、標準的な閾値署名アルゴリズムで十分です。すべての鍵の生成と維持のために、DFINITY チームは新しい暗号を発明しました。(1)新しいサブネットの形成とそのための鍵材料の生成は、インターネット・コンピュータのネットワーク・ナーバス・システム(NNS)によって行われ、これは生成時に作成される最初のサブネット上で動作するキャニスターによって実装される。(2)新しいサブネットを形成するノードが NNS から鍵材料を受け取り、新しいサブネットの運用を開始すると、そのサブネットはインターネット・コンピュータ・プロトコルに従って自ら鍵を管理・維持する。

これら 2 つの手続きは、閾値署名、公開鍵暗号化、非対話的ゼロ知識証明など、 多数の暗号プリミティブから構築されている。まず、インターネット・コンピュータが新しいサブネットの鍵を生成する方法を説明しよう。より正確には、NNS サブネットが新しいサブネットの公開鍵と、新しいサブネットの実行に指定されたノードの対応するキー・シェアを生成する方法である。閾値署名スキームの通常の鍵生成アルゴリズムは、単一の信頼できるディーラーを想定している。しかし、インターネット・コンピュータでは悪意のあるノードを許容する必要があるため、この仮定は保証されない。ICP が実装する解決策は、1) 複数のディーラーを使用し、2) 指定されたノードのために正しい共有を暗号化したことを証明させ、3) 彼らが生成した鍵を単一の公開鍵と対応する秘密鍵共有に結合することである。

チェーン・キー暗号は、1 つのディーラーが誠実である限り、安全なキー・マテリアルを生成する。この基準は、NNS サブネットの 3 分の 1 以上のノードがディーラーとして機能する場合に満たされる。彼らはそれぞれ公開鍵を生成し、新しいノードがインターネット・コンピュータに登録する際に利用可能にした暗号化鍵の下で秘密鍵の共有を暗号化し、そして私たちがオーダーメイドで設計した非インタラクティブなゼロ知識証明を追加して、彼らがこのすべてを正しく行ったことを証明する。NNS サブネット全体がこれらの証明を検証し、それが正しければ、すべてのディーラーが提供した情報を新しいサブネットのノードが利用できるようにします。これらの新しいノードはそれぞれのキー・シェアを解読し、異なるディーラーからのシェアを 1 つのシェアに結合する。(これは、鍵空間に存在する同型性と呼ばれる数学的性質のために可能である)。ノードがこれを実行すると、新しいサブネットを運用する準備が整う。

サブネットの運用中、そのノードは秘密鍵共有を維持する必要がある。これは、ノードが故障して交換が必要になったり、ノードが攻撃者に侵入されて秘密鍵シェアが漏えいしたりする可能性があるためです。どちらの場合も、秘密鍵共有は交換されなければならない。このため、残りのノードは秘密鍵の共有を再共有することができる。したがって、各ノードは他の多くのノードから元の秘密鍵共有の共有を受け取ることになる。秘密鍵共有の数学的特性により、これらの「共有の共有」は秘密鍵の通常の共有に組み換えることができる。このように生成された新しい共有は、秘密鍵の新たな共有に相当する。したがって、鍵の生成と同様に、各ノードは秘密鍵の共有の共有を得て、元の 秘密鍵の共有を共有し、サブネットを実行するために指定された(おそらく異なる)ノード セットのためにこれらの共有を暗号化し、正しく実行される非対話的なゼロ知識証明を 追加する。こうして新しいノード・セットは、このような共有をいくつか受け取り、証明が検証された場合、共有を復号化し、それらを 1 つの鍵共有に結合する。このプロセスの後、新しいノード・セットは、古い共有があればそれを削除し、サブネットの運用を継続できるようになる。この再共有プロセスにより、1) 新鮮なノードが必要な秘密鍵の共有を得ること、2) 攻撃者の手に渡った共有は古くなり、価値がなくなることが保証される。

## Subnet operation requires keys and canister state

先ほど説明したように、チェーン・キー暗号は NNS サブネットを無限にスケールアウトすることを可能にする。どのノードをどのサブネットに割り当てるかを決定し、すべてのサブネットの公開鍵を最初の秘密鍵共有とともに生成し、証明する。そのため、ユーザーが必要とするのは、NNS サブネットの公開鍵だけであり、サブネットから得られるレスポンスで証明書を検証することができる。

ネットワークが進化するために非常に重要なチェーン・キー暗号のさらなる側面について説明しよう。まず、秘密鍵の共有とは別に、実行中のサブネットに参加する新しいノードは、そのサブネットに割り当てられているすべてのキャニスターの現在の状態も必要とすることに注目する。これは、ノードがキャニスタへのメッセージを処理して状態を更新し、サブネットに完全に参加するために必要です。参加ノードは、すでにサブネットを運用しているピアノードに状態を要求できます（状態同期プロトコルを介して）。ただし、ノードが故障している可能性もあるため、参加ノードはピアノードから受信したステートの真正性と正確性を検証できなければなりません。このため、他のノードはすべてのキャニスターの状態に閾値署名（ルートハッシュ）します。

残念なことに、非対話的な鍵の再共有と状態の認証はコストのかかる操作であるため、ブロックごとに行うことはできず、一定の間隔、たとえば 200 ブロックごとにしか行うことができない。そのため、サブネットは、ノードがサブネットに参加するために必要なすべての情報をバンドルした、いわゆるキャッチアップ・パッケージを定期的に作成します。キャッチアップ・パッケージは、その名前が示すように、新しいノードがサブネットに参加することを可能にするだけでなく、既存のノードがネットワークから切り離された場合などに動作を再開することも可能にします。

## The power of catch-up packages

キャッチアップ・パッケージは、基本的にサブネットの実行において明確に定義された状態を提供するため、最終的にはさらに多くのことを可能にする。そのため、サブネットがキャッチアップ・パッケージを生成したときはいつでも、以前の状態をすべて削除することができ、すべての状態が永久に維持される典型的なブロックチェーンよりも、インターネット・コンピュータの方がはるかにストレージ効率が高くなる。

第二の特徴として、キャッチアップ・パッケージは、ノードの大半がクラッシュした場合でも、インターネット・コンピュータがサブネットを復活させることを可能にする。1 つのノードが生き残っている限り、キャッチアップ・パッケージを復旧し、新しいキー・セットを生成し、新しいサブネットがキャッチアップ・パッケージの状態からキャニスターを実行し続けることができる。

第三に、キャッチアップパッケージによって、インターネットコンピュータはロードバランスをとることができる。あるサブネットにインストールされたキャニスターが非常に人気があり、単一のサブネットが提供できる以上のリソースを必要とすると仮定すると、インターネット・コンピュータは、サブネットを 2 つのサブネットに分割し、それぞれが同じキャッチアップ・パッケージから継続するが、キャニスターは半分しか保持しないようにノードに指示できる。

最後に、キャッチアップパッケージは、インターネットコンピュータプロトコル自体のアップグレードも可能にする。ここでのアイデアは、次のキャッチアップパッケージが生成された後に、プロトコルの異なるバージョンを実行するようにサブネットのノードに指示することです。これにより、プロトコルのバグの修正や新機能の追加などが可能になる。これは、インターネット・コンピュータが永遠に稼動するために最も重要な機能だろう。

# Inside the Internet Computer | Chain Key Cryptography
https://www.youtube.com/watch?v=vUcDRFC09J0

こんにちは、私はヤン・カメニッシュです。Dfinityで研究と暗号部門を運営しています。
今日は、インターネット・コンピュータの中身を覗いて、重要なテクノロジーについてお話ししましょう。

hi, i'm jan kamenisch and i'm running the research and crypto department at dfinity.
today we're going to take a look under the hood of the internet computer and talk about key technology.

---

インターネット・コンピューターは、インターネット・コンピューター・プロトコルと呼ばれるプロトコルによって管理されており、このプロトコルが世界中のノードを指示し、接続している。
このプロトコルは、開発者がキャニスターをインターネット・コンピューターにアップロードし、そのキャニスターをインターネット・コンピューター上で実行するようノードに指示する。
ユーザーはキャニスターに接続し、キャニスターは他のキャニスターに接続して、開発者がプログラムした機能を実行することができる。
キャニスターが提供するウェブページであれ、企業システムであれ、オープン・インターネット・サービスであれ、LinkedInやフェイスブックなどのオープン・バージョンのようなものであれ、その他のインターネット・サービスであれ、キャニスターはあらゆるものを実装することができる。
また、取引所を実装することもできる。

the internet computer is governed by a protocol the internet computer protocol that directs and connects nodes that are running all over the world.
the protocol allows developers to upload canisters onto the internet computer, and it then directs the node to run those canisters on the internet computer.
users can then connect to canisters and canisters can connect to other canisters in order to execute and the functionalities the developers have programmed.
and canister can implement about any anything it could be web pages that are served by canisters they could implement an enterprise system or open internet service sort of open versions of linkedin or facebook or any other internet service.
you could also implement distributed exchanges as well.

---

では、キャニスターとは何なのか？
キャニスターはコードの束のようなものだ。キャニスターにはプログラム・コードが入っていて、プログラムの状態も入っている。
そして、ユーザーがキャニスターと対話するとき、キャニスターはユーザーに対して、あるいはインターネット・コンピューターの1つのノードに対してメッセージを送信し、インターネット・コンピューターはそのメッセージの入力に対してキャニスターを実行する。

so what is a canister?
well a canister is really like a bunch of code when it contains a program code and it also contains the state of the program.
and then when a user interacts with a canister it sends the user they send a message to the canister or to one node of the internet computer and then the internet computer executes the canister on the input of that message.

---

では、キャニスターとは何か？
キャニスターは、プログラムコードとプログラムの状態を含むコードの束のようなものだ。
そして、ユーザーがキャニスターとやりとりするとき、キャニスターはユーザーにメッセージを送ったり、インターネット・コンピュータの1つのノードにメッセージを送ったりします。
そして最終的に出力メッセージを計算し、それをユーザーが問い合わせることができる。
メッセージを実行することで、キャニスターの状態は最新の変更や開発者が実際にキャニスターにさせたかったことを反映するように更新されます。
別のキャニスタをライブラリやサブシステムとして使用できます。ユーザからのメッセージを処理するために、キャニスタは別のキャニスタにメッセージを送信し、メッセージを処理するために使用する応答を返します。
そしてもちろん、全体がセキュアである必要がある。インターネット・コンピュータの要点は、そのようなキャニスターが動作するためのスケーラブルでセキュアな環境を提供することだ。キャスターが1台のマシンでしか動作せず、そのマシンがクラッシュしたり、悪意があったり、欠陥があったりして、ユーザーが受け取ったメッセージが本当に正しいコンピュータなのかどうかがわからなくなることは明らかだ。
そのため、インターネット・コンピューターは、複製された方法でキャニスターを実行するノードを接続する。つまり、基本的にノードはすべてメッセージに対してキャニスターを実行する。
そして実行後、同じ結果が得られたかどうかをオープンプロセスで確認する。
そして、本当に同じ結果が得られた場合、その結果だけをユーザーに送り返す。
インターネット・コンピューターによって計算された結果の正しさと堅牢性を達成するために、キャニスターは単一のノードで実行されるのではなく、複数のノードで実行される。なぜなら、1つのスコープが失敗すると、状態が失われたり、ノードが悪意を持っている可能性があり、実際に結果が正しくない可能性があるからだ。
そのため、キャニスターやプロトコルは、キャニスターが複数のノードで実行されていることを確認し、メッセージを受け取り、メッセージの下でキャニスターを実行し、その後、同じ結果が得られたことを確認します。
つまり、キャニスターが永遠に稼働し、計算結果が実際に正しいことを保証するわけです。

So what is a canister?
Well a canister is really like a bunch of code when it contains a program code and it also contains the state of the program.
And then when a user interacts with a canister it sends the user they send a message to the canister or to one node of the internet computer and then the internet computer executes the canister on the input of that message.
And eventually compute like an output message that then the user can query back.
By executing the message the state of the canister is updated to reflect the latest changes or whatever the developer actually wanted the canister to do.
Now a canister can also cut out to other canisters during its execution so you can use another canister as a library or a subsystem and so in order to process a message then from a user the canister would send a message to another canister and get the reply back that you would then use to process its message.
And now of course the whole thing needs to be secured that's the whole point of the internet computer to provide scalable and secure environment for such canisters to run. Obviously if a caster would only be run on a single machine and then the machine could crash or it could be malicious or faulty and we wouldn't be sure whether the messages that the user gets back or actually correctly computer.
Therefore the internet computer connects nodes that would then in a replicated fashion execute a canister. So essentially the nodes would all execute the canister on the message.
And then after that execution they would agree open process whether or not they obtained the same result.
And if they indeed do obtain the same result they would then only then send that result back to the user.
To achieve correctness and robustness of the results computed by the internet computer, a canister is not run on a single node but run on multiple nodes. Because a single scope fail and then the state would be lost or a node could be malicious and then actually the result might not be correct.
So what we do is the canisters or the protocol will make sure that canisters are around multiple nodes, and this then take the message run the canister under message and afterwards make sure that they have achieved the same result.
So this guarantees that canisters run forever and the results the compute are actually correct.

---
そのためにはもちろん、ユーザーがインターネット・コンピューターと話すとき、その結果が転送中にどこかで変更されることがないように、本当に認証されていることを保証したい。
そのために、インターネット・コンピューターは公開鍵とそれに対応する署名鍵を持ち、キャニスターの実行結果を計算したノードは、その結果に共同で署名する。
この目的のために、私たちは実際に閾値署名スキームを使用しています。つまり、すべてのノードは閾値キーから自分の共有秘密鍵を持っているということです。
そしてこの方式で、計算された結果に対して部分署名を行う。
そして、十分な数のノード、例えば3分の2以上のノードが、その結果が正しいということに合意し、その結果に署名する。
そして、これらの署名を組み合わせて、メッセージに対する完全な署名完全な証明書を作成し、それをユーザーに送り返すことができる。

In order to do that of course the user when it talks to the user when she talks to the internet computer, it wants to be assured that the result is indeed authenticated so that it could not be modified somewhere in transit.
And to this end the internet computer has a public key and the corresponding signing key and then the nodes after having computed the results of a canistar execution would sign that result jointly together.
So to this end we actually use threshold signature scheme so that means every node has their own shared secret key from the threshold key.
And then with that scheme they each sign like produce a partial signature on that computed result.
And when enough of them like two-thirds of them or more than two-thirds of them have agreed that the result is correct and have signed that result.
Then you can combine these signatures into a full signature full certificate on the message that then is sent back to the user.

---

これはチェーン・キー・テクノロジーの非常にクールな特徴のひとつで、たったひとつの公開鍵で、インターネット・コンピュータの計算応答を検証することができる。
必要なのは48バイトの値だけです。
そして、インターネット・コンピュータの公開鍵があれば、その正しさを検証することができる。
例えばイーサリアムと比較すると、オープンなイーサリアムのクライアントは、結果を検証するために400ギガバイトをダウンロードする必要がある。
クライアントによってはさらに大きくなり、gatcなら700ギガバイト、アーカイブ・ノードなら7000ギガバイト近くをダウンロードしなければならない。
1年前は200ギガバイトちょっとだったのが、1年で400ギガバイトを超えました。つまり、時間とともにリニアに成長していくのに対して、インターネット・コンピューターにとっては48バイトという単一の値で、本当に必要なのはそれだけなのです。

So this is actually one of the very cool features of chain key technology mainly now with a single public key you can actually validate the responses the computation of the internet computer.
So all you need to know is a single 48 byte value.
And the public key of the internet computer with that you can verify the correctness.
Compared for instance to ethereum, the open ethereum client needs to download 400 gigabytes in order to validate the result.
Or depending on the client can be even be larger it can be for gatc it can be up to 700 gigabytes or if you want to be an archive node actually you have to download almost 7000 gigabytes.
That value actually grows it was just a bit more than 200 gigabyte a year ago now it has grown over 400 gigabyte within a year. So it will grow linearly with time whereas for the internet computer it will be a single value this 48 bytes that is all that you really need.

---

それでは一歩下がって、インターネット・コンピューターが実際にどのように全ノードのキャニスターを計算しているのか見てみましょう。
というのも、すべてのノードですべてのキャニスターを計算することはできないからです。
また、何百万ものノードが同じキャニスターを実行することになります。もちろんその一方で、今話したようなセキュリティ特性を得るためには、十分な数のノードでキャニスターを実行する必要があります。
そのため、すべてのノードをノードのサブセットに分割する必要があります。それぞれのサブセットをサブネットと呼びます。そして、これらのサブセットは十分に大きく、異なるキャニスターを異なるサブネットに割り当て、そこでキャニスターを実行します。
もちろん、これらのサブネットが互いに通信してメッセージを認証したり、ユーザーへのメッセージを認証したりするためには、それぞれのサブネットが独自の公開鍵を持つことになる。
つまり、各サブネットはそれぞれ独自の公開鍵を持ち、サブネットの全ノードがその公開鍵を共有することになる。
そしてもちろん、ユーザーがサブネットで認証されたメッセージを見るときには、そのサブネットで署名されることになる。
あるいは、キャニスターが他のキャニスターを呼び出して応答を受信した場合も、そのメッセージはサブネットの鍵で署名される。
ということは、多くの公開鍵が存在することになります。異なるサブネットからの応答を検証するために、多くの公開鍵を保存しなければならなくなったのは確かなので、先ほど私が言ったことは間違っていることになります。

So let us take a step back and see how the internet computer actually disputes the canisters on all the nodes.
Because obviously we cannot compute all canisters on all the nodes because that wouldn't scale at all.
Also we would have millions of nodes that would run the same canisters. Of course on the other hand we need to run the canister on sufficiently many nodes that should we get the security properties that we just talked about.
So for that reasons we have to split all the nodes into a subset of nodes. And each subset we call like a subnet. And these supplements are sufficiently large and then our different canisters are assigned onto different subnets where they get drawn.
Now of course in order for these subnets to talk to each other to authenticate their messages but also to authenticate messages towards the user, each of those subnets will have their own public keys.
So every subnet has their own public keys or and also all the nodes of the subnet have a key share of those public keys.
And now of course when a user gets to see a message that is authenticated with a submit it will be signed with that subnet.
Or where the canister calls out to another canister and receives a response that message will also be signed with the with respect to the subnet key.
So that means now we certainly have many public keys so it's a statement that i was just making before wrong right because now certainly we seem to have to store a number of public keys in order to validate responses from the different subnets.


---

というのも、nnsサブネット、あるいはネットワーク・ニューラル・システム・サブネットと呼ぶ特別なサブネットがあり、nnsサブネットは他のすべてのサブネットを統括しているからだ。
例えば、どのノードがどのサブネットに行くかを決定したり、サブネット内の広告がこれらすべてのサブネットの鍵を認証したりする。
したがって、サブネットはその公開鍵を持っているが、同時にその公開鍵のnnsサブレットからの証明書も持っていることになる。
これで、サブネットの鍵で署名されたサブネットからのメッセージをユーザーが受信した場合、サブジェクトはrnsサブネットからの証明書を渡すことができる。そして、ユーザが必要とするのは、nnsサブネットの公開鍵だけである。そして、サブネットの公開鍵の証明書を検証し、ユーザーがサブネットから受け取ったレスポンスのメッセージの証明書を検証することができる。
つまり、インターネットコンピュータは何百万ものノードと何千ものサブネットによって支えられているにもかかわらず、必要なのは、どのサブネットからの結果も検証できる単一の公開鍵だけなのだ。
すべてのサブネットがそれぞれの鍵を持っているため、何千ものサブネットが存在し、サブネットの個々のノードが持っている鍵や鍵共有の数も膨大になる。

Well no that's not not quite the case because we have like a special subnet that we call the nns subnet or the network neural system subnet, the nns subnet orchestrates all the other subnets.
So for instance it will decide which nodes go to which subnets the ad in a subnet will also certify the keys of these all these subnets.
And therefore now subnet has its public key but it also has a certificate from the nns sublet on that public key.
And now if there's a message that a user receives from a subnet signed by the subnets key, the subject can pass along that certificates from the rns subnet. And then all the user needs is the public key of the nns subnet. And then it can validate the certificate of subnet's public key and then validate the certificate on the message on the response it gets the user get from subnet.
So really what we have is uh although the internet computer is powered by millions of nodes and thousands of subnets, all we really need is a single public key to validate the results from any of those subnets.
As all the subnets have their own keys, there's a ton of subnets, thousands of subnets, and a lot of keys a lot of key shares that the individual nodes of these subnets have.

---

もうひとつの疑問は、これらの鍵をどうやって管理するかということだ。
特にノードは故障するかもしれないし、危険にさらされるかもしれないし、悪意を持って鍵の一部をブロードキャストされるかもしれない。
そこで、2つの主要な手続きをサポートする必要がある。
1つ目は、これまで見てきたように、新しいサブネットを生成するnnsで、ノードに新しいサブネットの形成を指示する。
nnsはサブネットの公開鍵だけでなく、サブネットの各ノードのキー・シェアも生成する。
また、サブネットの公開鍵の証明書も生成する。
そのため、何十万ものサブネットに対応できるようなスケールが必要だ。

Another question is how can we manage all these keys how we can generate all these keys how can we maintain that.
in particular nodes may fail they might be compromised or even be malicious and maybe broadcast some of those keys.
so there's two main procedures here that we need to support.
the first one is the nns as we have seen that generates new subnets it instructs nodes to form a new subnets.
the nns will generate the public key of the subnet, but also the key shares for the individual nodes of the subnet.
And also the certificate on the public key of the subnet.
So we need to be able to do that in a way that scales, to hundreds of thousands of subnets.

---

一方、いったんサブネットが公開鍵を受け取り、ノードが個々のキー・シェアを受け取ったら、サブネットの存続期間中、それを維持できなければならない。
そのため、ノードが故障したり、鍵共有が失われたり、他のノードが破損したり、悪意を持ってサブネットから排除されたりする可能性がある。
そのため、ノードの運用を維持するためには、ノードは一緒にその鍵の材料で運用する必要がある。
では、どうすればいいのか。それを見てみよう。
そこで、閾値署名方式と呼ばれるものを使います。
閾値署名スキームはいくつかのアルゴリズムで構成されています。
まず鍵生成アルゴリズムがあり、一般的にディーラーが公開鍵を生成し、後に閾値署名スキームを使用するパーティのための鍵共有を行います。
つまり、この場合はサブネットのノードということになる。
次に署名アルゴリズムがあり、個々の当事者（この場合もノード）は、その秘密鍵共有を使ってメッセージの部分署名を作成する。
十分な数のノードが署名し、十分な数のパーティが秘密鍵を使用して部分署名を生成した。
これらの署名を組み合わせて、メッセージ全体の署名とすることができる。
そしてそれは、例えば4人中3人のような十分に多くの当事者がメッセージに署名した場合にのみ機能する。
その結合された署名は、最初に生成された公開鍵と照合して検証することができる。

On the other hand once the subnet has received their public key and the nodes have received their individual key shares, they need to be able to maintain that throughout the lifetime of a subnet.
So nodes might fail they might lose their key shares other nodes might be become corrupt or malicious and maybe for that reason excluded from the subnet.
So the nodes together need to have to operate in the that key material to maintain their operations.
So how can we do that? Let's look at that.
So we use something that's called threshold signature scheme.
A threshold signature scheme consists of a number of algorithms.
So first of all there's a key generation algorithm where a dealer typically throws the party generates the public key and the key shares for the parties that are later going to use the threshold signature scheme.
So in our case that would be the nodes of a subnet.
Then there's a signing algorithm, where the individual parties, in our case again the nodes, would use their secret key share to produce a partial signature on a message.
Now once sufficiently many nodes have signed, sufficiently many parties have used their secret keys to generate a partial signature.
Those signatures can be combined into an overall signature on the message.
And that only works if sufficiently many like three out of four parties have signed the message for instance.
That combined signature can then be verified against the public key that was generated originally.

---

ここで、ノードの鍵を生成する方法を見てみよう。現在、インターネット・コンピュータでは、鍵を生成する信頼できるパーティを1人にするわけにはいかない。
というのも、ここで想定されているのは、3分の2以上のパーティが正しい限り、コンピュータは安全であるということだからだ。
ということは、悪意のあるパーティも許容しなければならないということだ。
特に、運悪く鍵を生成してくれるディーラーを選んでしまった場合、それは悪意があることになり、システム全体が機能しなくなる。
そこで、このトリックに対処する方法が2つあります。
1つ目は、ある文の正しさを証明する暗号的知識証明です。
この場合、ディーラーが異なるノードの公開鍵と秘密鍵を生成するとする。
そして、暗号文に暗号化されているものが、実際にそのノードが提供する公開鍵に関して正しい共有であることを、暗号学的に証明する。

Let's see how we can generate the keys here for the nodes. now on the internet computer we cannot afford a single trusted party that generates those keys.
Because the assumption here is that the computer should be secure as long as more than two-thirds of the parties are correct.
that means we have to be able to tolerate some parties that are malicious.
And in particular if we were out of luck and chose a dealer that would generate those keys for us, that would be malicious the whole system would fail would fail.
So there's two ways here that we can address that to to tricks, if you want so.
The first one is cryptographic student knowledge proofs that prove the correctness of some statement.
In our case, if a dealer generates, the keys to public keys and the secret key shares, for the different nodes.
It would encrypt those key shares for those nodes and provide cryptographic proofs that what is encrypted in the ciphertext is actually a correct sharing with respect to the public key that did also that the party also provides.

---

これが最初の要素だ。
もちろん、これを行うのは1つの当事者だけなので、このシグナル部分が悪意あるものでないことをどうすれば確認できるかを考えなければならない。
ここで重要なのは、複数の当事者を使うということです。
つまり、共有や証明を行うパーティを複数用意することで、共有の数、公開鍵の数、証明の数を増やし、それらのすべてが正しいことを検証できるようにするのです。
しかし、公開鍵における秘密鍵の性質は、実は同形と呼ばれるものなのです。
つまり、公開鍵を圧縮して1つの公開鍵にする数学的な操作が可能なのです。
秘密鍵についても同じことができる。
つまり、その数学的な操作を公開鍵にも適用すればいいということだ。
公開鍵でも秘密鍵でも、いくつもの共有鍵を1つの共有鍵に圧縮することができる。
そして、それぞれの共有が正しいことは証明書を検証したことで分かっているので、結果として得られる共有も正しいことが分かる。正しい公開鍵があり、正しい秘密鍵がいくつもある。

So that's the first ingredient here.
Now of course we still are stuck with a single party that does that, so now we somehow need to figure out how can we make sure that this signal part is not malicious.
And the trick here is that we actually use multiple parties.
So we have a number of parties that do sharings here, do those proofs so we have a number of sharings, a number of public keys, number of proofs so we can verify that all of those are correct.
But now the properties that we have here of these secret keys in public keys are actually that they are what we call homomorphic.
So that means that there is a mathematical operation that we can do on the public keys to compress them if they want into a single public key.
And the same thing we can do on the secret keys the same thing.
So that means if we apply those that mathmatical operation.
On the public keys as well in the secret keys we can compress a number of sharings into single sharing.
And as we know that each of those sharings are correct because we verified the proof we know that the resulting sharing is also correct. We have a correct public key and we have a number of correct secret keys.

---

そこで今、私たちが行っているのは、シェアリングを行うディーラーをいくつも用意し、それらを1つのシェアリングにまとめることだ。
そうすれば、一人のディーラーが誠実である限り、スキーム全体が安全であることが保証される。
このゲームが安全であるためには何が必要かというと、鍵が完全にランダムに生成され、その鍵がディーラーから漏れたり公開されたりしないことである。
ここでの同型演算は空間全体に対して働くので、これらのキーの1つが純粋にランダムである限り、実際に数学的に保証されている。
そして、その結果生成される鍵もまた、純粋にランダムであり、同様に秘密にされる。

So now what we have done is we have we can have a number of dealers that do sharings and then we combine them into a single sharing.
Then we're guaranteed that as long as a single dealer is honest the whole scheme will be secure.
Nearly what do we need for this game to be secure well we require that the keys are generated perfectly random. and the keys were not leaked or not published by the dealer.
As the homomorphic operation here works on the whole space it is actually mathematically guaranteed that as long as one of those keys was purely random.
And is not leaked the resulting key is purely random and is kept secret as well.

---

これで、サブネットの秘密鍵と公開鍵を非対話的に生成する方法がわかった。
これをnnsサブネットに使って、新しいサブネットを生成する。
つまり、nnsサブネットの各ノードはディーラーであり、コンピュータであり、共有し、取引する。そして、nsのノードが一緒になって、その公開鍵を1つの公開鍵にまとめ、公開鍵として認証する。
そしてnnsのノードが一緒になって公開鍵を1つにまとめ、公開鍵で認証するのです。すべての資料が計算されると、新しいサブネットを形成するノードに送られ、彼らはその秘密をすべて復号して1つの秘密鍵のようにし、公開鍵を組み合わせてnnsの公開鍵を手に入れます。
そして、新しいサブネットを開始する準備が整った。これでサブネットのノードは秘密鍵を受け取り、サプリメントは公開鍵を受け取り、運用を開始する。

So now we see how we can non-interactively generate secret keys and public keys for subnets.
And that's actually now how we use that for the nns subnet to generate new subnets.
So each node of the nns subnet will be a dealer and a computer sharing and dealing, and then together the nodes in the ns would combine that public into single public key and certified at public key.
So once all that material is computed it is sent to the nodes who form a new subnets, they will decrypt all of that combine their secrets into like a single secret keyshare combine the public key to obtain the public key of the nns.
And they're ready to go to start a new subnet. So now the nodes of the subnet have received their secret key shares, supplement has received the public key, and they start their operations.

---

しかし今、ノードがクラッシュして秘密鍵を失ったり、ノードがジョインしたりした場合、ノードはこの出力の公開鍵を安定させながら、これらの秘密鍵を管理する必要がある。
どうすればいいのだろうか。私たちは鍵を再生成したくない。なぜなら、そうするとソフトネス公開鍵が変わってしまうからだ。
ですから、安定した状態を保ちたいのです。
つまり、ある意味では以前と同じメカニズムを適用していることになる。つまり、各サブネット・ノードが他のノードに対して秘密鍵を再共有するのです。実際には、いくつかのノードがクラッシュしたり、アナリストが新しいノードをサブネットに割り当てたりする可能性があるため、ノードの新しいセットアップが必要になる。
各ノードは自分の秘密鍵を取り出し、ディーラーが行ったように、サブネット上の他のすべてのノードと秘密鍵を再び共有します。同様に、これらの共有はノードの公開鍵の下で暗号化されるため、暗号文がいくつも存在することになる。
もちろんノードが持つ秘密鍵の共有も暗号化する。
そしてノードは、共有が正しいこと、それがすべて有効であることを証明する暗号文を再び作成します。

But now if a node crashes loses its secret key or if a node is a join then the nodes need to manage those secret case while keeping the public key of this output stable.
How can we do that? Because we do not want to regenerate the keys.ecause then that would change the softness public key.
So we want to keep that stable.
So the trick here is that in some sense we applied the same mechanism as before. But just a little different, namely what we want to do is here that the subnets nodes each of them re-shares their secret key for the other nodes. Actually the new setup of nodes because some of the nodes might be crashed or the analyst might have assigned new nodes to the subnet.
Each nodes will take their secret key and share that secret key with all the other nodes on the subnet again as the dealer would have done. As well those shares are encrypted under the site under the public keys of the node so we have a number of ciphertexts.
We also encrypt of course the secret key share that the nodes has.
And then the node produces again a cryptographic proves, that sharing is correct that it's all valid.


---

十分な数のノードが秘密鍵を再共有すると、新しいノード・セットは、これらの個々の共有鍵を解読し、サブネットの公開鍵に対応する秘密鍵の完全な共有に再び結合することができる。
新しいノード・セットは、これらの個々の共有鍵を復号化し、それらを再び結合して、サブネットの公開鍵に対応する秘密鍵の完全な共有にすることができる。
特に、共有するサブネットがある場合、ノードがクラッシュすると、nsは新しいノードをサブネットに割り当てる。その後、残りのノードが新しいサブネットの秘密鍵を共有する。
そして共有が完了すると、新しいノードは同じ公開鍵を共有することになる。ここで重要なのは、サブネットの公開鍵は変更されないということだ。

Once sufficiently many of the nodes have reshared their secret key.
Onto a new set of nodes can decrypt the these individual sharings and combine them again, into a full sharing of the secret key that corresponds to the public key of the subnet.
In particular if we have a if you have a subnet with shares a node crashed the ns would assign a new node to the subnet. Then the nodes the remaining Nodes would share their secret keys for the new subnet of the nodes.
And then after that sharing has happened the new set of the nodes can obtain from that a new sharing of the same public key. So again it's important here that the public key of the subnet actually remains unchanged.

----

さて、秘密鍵の共有だけではサブネットに参加するには不十分だ。
というのも、サブネットやサブネットの各ノードはキャニスターを運用しており、もちろんキャニスターの状態は時間とともに変化するからだ。
新しいノードがサブネットに追加され、鍵の再共有が行われた場合、そのノードは鍵を持つことになる。新しいノードは鍵を持っているが、キャニスターの状態は持っていない。そのため、サブネットがそのキャニスターを実行するのを手助けすることはできない。

Now the secret key shares alone are not sufficient to participate in a subnet.
Because remember the subnet or each nodes of the subnet run those canisters and of course the state of the canisters will change over over time.
And if a new node adds to the subnet and after we have done this new re-sharing of the keys. It will have a keys but it doesn't have the state of these canisters. So we cannot really participate and help the subnet run that canister.

---

つまりここでのトリックは、他のノードが新しいサブネットにその状態を伝える必要があるということだ。
もちろん、新しいノードが正しい状態を受け取ったと確信できるような方法で。
では、キー・シェアを受け取った新しいノードは、どのようにして状態を受け取り、オペレーションを開始できるのだろうか？
基本的には、他のノードに状態を問い合わせることができる。
しかし、1つのノードを信頼することはできないことを忘れないでください。そのため、何らかの方法ですべてのノードに問い合わせるか、あるいはキャニスターの状態を認証できる残りのノードに問い合わせ、そのすべてを新しいノードに送信することになります。
私たちがやっているのはこれです。
だから私たちは定期的にすべてのキャニスターの状態を認証しています。もちろんキャニスター自体もです。
そして、新しいノードの古い秘密鍵の共有と合わせて、新しいノードがサブネットに参加し、そのサブネット内の他のノードと実際に運用するのに十分な情報となります。
そしてこの認証された状態と新しい共有が、キャッチアップ・パッケージと呼ばれるものだ。
このキャッチアップ・パッケージが、ノードがサブネットに参加するために必要なすべてです。

So the trick here is now that the other nodes need to tell the new subnet of that state.
Of course again in a fashion that the new node is actually sure that it will have received the correct state.
So how could a new node that now has received the key share received the states they could start operations?
Principally could just ask any other nodes for that state.
But then remember we cannot trust a single node. so it would have somehow asked all the nodes or alternatively actually these remaining nodes that Could certify authenticate that state of the canisters and then send that all to the new node.
And that's actually what we do.
So we regularly authenticate the state of all the canisters. and of course the canisters themselves as well.
And together with the sharings of the old secret keys for the new set of nodes that will be enough information for a new node to join a subnet and actually operate along with the other nodes in that subnet.
And that certified state and the new sharing that's what we call the catch-up package.
And that catch-up package is all you need for a node to join a subnet.

---

そしてキャッチャー・パッケージは、実際には非常に強力なツールだ。
つまり、サブネットのノードが多くのケースで運用を維持できるようにするものだ。
例えば、先ほど説明したように、ノードがクラッシュしたときにノードを交換することができます。
そこで新しいノードはキャッチャー・パッケージを受け取り、必要な秘密鍵共有と必要な状態を取得する。
あるいは、インターネット接続が切れてノードが長時間オフラインになった場合なども、キャッチャーを要求することができる。
あるいは、他のノードにキャッチャー・パッケージを要求することもできる。
この場合も、サブネットに再参加するために必要なすべての情報を持っていることになる。
あるいは、サブネットの多くのノードがクラッシュした場合、鍵共有に関する情報を失いすぎてしまう。
そのため、公開鍵を再構築することはできない。
では、ノードが定期的に認証された状態であるとして、何ができるのでしょうか？
私たちnsができることは、その認証された状態から新しい公開鍵を生成することです。
そしてまたサブネットを立ち上げ、溺れさせることができる。この場合は別の公開鍵だが、すべて安全だ。

And the catcher package is actually a very powerful tool.
Namely it allows really the nodes of a subnet to maintain this operated in a number of cases.
So for instance as we just discussed when a node crashes we want to replace the node.
So the new node will receive that catcher package and then it will get the required secret key share plus the required state.
Or also if a node was offline for toolong time because the internet connection broke down so.
Or so also it could request a catcher package from the other nodes.
Again it would have all the information it requires to rejoin a subnet.
Or if too many nodes of a subnet have crashed that actually they lost too much information on the key shares.
So they could not reconstruct that public key but they still have to state.
So and as the nodes regularly certified state, what we could do?
Or actually what we, the ns, can do is it will just generate a new sharing a new public key take that certified state.
And again the subnet can be up and drowning although in this case it was a different public key but it will all be secure.

---

最後に、キャッチャー・パッケージでできることとして、おそらく最も重要なのは、プロトコルのアップグレードもできるということです。
というのも、キャッチャー・パッケージはサブネットの状態を定義するようなもので、その状態では常にオペレーションを継続することができるからだ。
だから、プロトコルをアップグレードしたい場合は、サブネットがキャッチャー・パッケージを生成するのを待って、サブネットにOKを伝える。
次のキャッチャー・パッケージが生成されたら、新しいバージョンのプロトコルを実行してください。
こうしてプロトコルをアップグレードして、プロトコルの不具合を修正したり、プロトコルに新しい機能を追加したりすることができる。

The last but probably most important part that we can do with catcher packages is that we can also upgrade the protocol.
Because the catcher package sort of defines a state of the subnet for which operations can always continue.
So if we want to upgrade the protocol then we wait for the subnet to generate a catcher package and then we tell the subnet okay.
So now after the next catcher package please run the new version of the protocol.
And then that's how we can actually upgrade the protocol we can fix box in the protocol if they happen or we can add new features to the protocol as well.

---
非対話型dkgと鍵の再共有は、チェーン・キー技術の2つの要素に過ぎない。
もっとたくさんあるので、近々オンラインでこれらすべてについてお話する予定です。
チェーン・キー技術の中心は、すべての異なるサブ・プロトコルを調整するコンセンサス・プロトコルである。
もちろん、コンセンサス・プロトコルの主な仕事は、ユーザーからのメッセージを収集し、その度合いを決定し、実行させることだ。しかし、コンセンサス・プロトコルは、非対話的なdkgとすべての異なるノード間の再共有もオーケストレーションしなければならない。計算が正しく行われるようにするのだ。また、ハードディスクの故障や宇宙的な不具合が原因で、あるノードが故障してしまったとしても、その不具合が広がってしまったとしてもだ。
このボックスが他のネットに広がることなく、含まれたままであることをいくつかのビットを反転させた。
また、再開と状態の同期もオーケストレーションする。与えられた状態から新しいサブネットをスタートさせる、すべてのアップグレードを指揮する。そしてもうひとつの興味深い特徴は、コンセンサス・プロトコルが安全なランダム性を提供することだ。アプリケーションにとって、これを正しい方法で実現するのは実は非常に難しい。

Non-interactive dkg and key re-sharing are just two ingredients of chain key technology.
There's much more and we will have talks about all of this soon online.
At the heart of chain key technologies are consensus protocols that orchestrates all the different sub protocols.
Of course consensus the main task of it is to collect and degree messages from users and get them executed. But it also has to orchestrate the Non-interactive dkg and the re-sharing between all the different nodes. It ensures that computations are done correctly. And even if some box happened because of a hard disk failure or some cosmic glitches that spread.
That flipped some bits that this box wouldn't spread to others up nets but remain contained.
It also orchestrates the resumption and the synchronization of state. It orchestrates all the upgrades, starting a new subnet from a given state. And another interesting feature is actually that the consensus protocols also provide secure randomness. To applications that is actually very very hard to achieve in a correct way.

---

さて、チェーン・キー技術で実際に何ができるかを見てきた。
チェーン・キー技術によって、インターネット・コンピュータの単一の公開鍵を持つことができる。
つまり、このインターネット・コンピューターからのメッセージを検証するのに必要なのは、この単一の公開鍵だけなのだ。
これにより、匿名でも新しいサブネットを追加し、ネットワークを永遠に拡張することができる。
また、あまりにも多くのノードが故障した場合でも、サブネットを復活させることもできる。
あるいは、ノードがクラッシュしたり、ノードが危険にさらされたりした場合、ノードを交換する必要があります。チェーン・キー・テクノロジーは、そのような事態から回復し、アプリケーションの運用を維持することを可能にします。
そして最後に、チェーン・キー・テクノロジーによって、プロトコルをアップグレードしてボックスを修正したり、新しい機能を追加したりすることができます。
つまり、チェーン・キー技術によって、インターネット・コンピューターは永遠に拡張され続けるのです。
私たちは無限大の分散性を手に入れたのです。
ありがとう。

So now we have seen what the chain key technology can actually do.
It allows us to have a single public key of the internet computer.
So all that is needed to verify a message from this internet computer is that single public key.
It allows the anonymous to add new subnets and scale the network forever.
And it can even revive a subnet if too many nodes have failed.
Or if nodes have crashed or nodes are compromised, they need to replace chain key technology allows us to recover from that and to maintain the operations of a application.
And finally chain key technology allows us to upgrade the protocol to fix box and to add new features.
So really chain key technology sort of makes the internet computers scale and drown forever.
So we have decentralized infinity.
Thank you.
