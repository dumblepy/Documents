# ETH integration

https://internetcomputer.org/ethereum-integration

## イーサリアム <> ICP

真のワールドコンピュータは、中央集権的なブリッジが廃止され、スマートコントラクトがブロックチェーン間でシームレスに通信できるマルチチェーン環境を実現する。ICP はすでにビットコイン・ネットワークと統合しており、ネイティブ ETH 統合も進行中です。

## World Computer capabilities for Ethereum

[イーサリアムのワールド・コンピューター機能](./Trustless%20multi-chain%20web3%20using%20the%20IC.md)

100%オンチェーンの Web3  
フロントエンドとデータをインターネットコンピュータ上でホスティングすることで、Ethereum dapp を完全に分散化します。

ガス不要のトークン交換  
ckERC-20 トークンを使えば、ユーザーはガス代ゼロで数セントでトークンの交換や移動ができる。

DAO の機能拡張  
DAO が制御できるものを拡張します。強力な ICP キャニスター・スマートコントラクトは、あなたの Dapp 全体をチェーン上にもたらします。

## ETH の統合

ICP と ETH の統合は 2 つのフェーズからなる。フェーズ 1 では、キャニスター・スマートコントラクトがクラウド API プロバイダーへの [HTTPS アウトコール](https://internetcomputer.org/https-outcalls)を使用してオンチェーンのイーサリアム・フルノード API を提供し、イーサリアム・ブロックチェーンに安全に問い合わせ、トランザクションを送信します。イーサリアムのトランザクションへの署名は、インターネットコンピュータ上のどのキャニスタースマートコントラクトでも既に利用可能な[チェーンキー ECDSA](./Threshold%20ECDSA%3A%20technology%20overview.md) によって可能になります。

フェーズ 2 では、インターネットコンピュータ上でオンチェーンのイーサリアム API を実現するために、プロトコルレベルの完全な統合が行われる。この API は、大規模な ICP サブネット上の各 ICP レプリカの隣で Ethereum フルノードを実行し、レプリカから ICP コンセンサスを通じてこれらのサブネットと通信することで実現される。

[About extending ETH](./Extend%20Bitcoin%2C%20Ethereum%20and%20other%20blockchains.md)

## Chain-key ETH & ERC-20

2023 年 6 月、USDC と USDT の平均取引手数料はそれぞれ 4.21 ドルと 5.46 ドルであり、一定額以下の DEX での取引は完全に非現実的なものとなっている。イーサリアムの統合により、ckUSDC や ckUSDT など、インターネットコンピュータ上で ckETH や ckERC-20 などのチェーンキー（ck）トークンを使用できるようになり、ICP DEX の機能が大幅に拡張され、ユーザーは 1 ～ 2 秒の確定性でトークンを交換したり、セントで送金したりできるようになりました。

[Follow the community discussion](https://forum.dfinity.org/t/long-term-r-d-integration-with-the-ethereum-network/9382)

## EVM on the Internet Computer

インターネットコンピュータ上で動作する Bitfinity の EVM は、開発者が既存の solidity コードや dapps をインターネットコンピュータ上で動作させるためのターンキーソリューションを提供します。使い慣れた EVM 環境と ICP を組み合わせることで、1 秒のブロックタイム、1-2 秒のファイナリティ、0.02 ドルの Tx コストを実現します。

https://bitfinity.network/

## コミュニティの共同研究

2023 年 2 月、ICP DeFi コミュニティは、チューリッヒの DFINITY 本社で開催された DeFi に焦点を当てた ICP.Lab に招集され、イーサリアムの統合について議論した。その結果プロトコル・レベルの統合には時間がかかるため、コミュニティはイーサリアムからインターネット・コンピュータに流動性をもたらす短期的なソリューション、つまり IC の DEX、レンディング・プロトコル、マーケットプレイスなどのユースケースで ETH と ERC-20 トークンを使用できるソリューションを考え出した。

イーサリアム統合のフェーズ 1 は ICP コミュニティによって開始された。オンチェーン Ethereum RPC API を提供するオープンソースのキャニスターが公開されており、Ethereum ネットワークへのゲートウェイとして Dapps にデプロイすることができます。

フェーズ 1 では、API をさらに洗練させ、Dapps の構築を簡素化する機能を追加していきます。フェーズ 1 の完了には、キャニスターの改良、セキュリティチェック、IPv4 サポートを追加するためのシステムサブネットへのキャニスターのデプロイも含まれます。

---

$USDT and $ETH on ICP  
https://twitter.com/icprobot/status/1678431566362443777

---

icRouter 90%開発完了  
https://twitter.com/icp_ns/status/1678411777434963969
