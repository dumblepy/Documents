# ICP のトークン規格について

## ICP のアカウントの仕組みについて

### Principal ID

### Address

## ICRC

### ICRC-1

https://internetcomputer.org/docs/current/developer-docs/integrations/icrc-1/

https://github.com/dfinity/ICRC-1

ckBTC の実装
送金に approve がいらない
FT の実装

### ICRC-2

https://github.com/dfinity/ICRC-1/tree/main/standards/ICRC-2

approve が必要になった
トークンの送信に approve を必要にするための実装

第三者（キャニスター）に送信処理を委任することができる？
サブスクができるようになる？

ERC20 を真似した、同じようなインターフェイスを備えることを目指した
allowance は address に紐づく

### ICRC-3

トランザクションログを取得するための規格

### ICRC-4

支払いのバッチ処理をするための規格

## DIP

### DIP20

https://github.com/rocklabs-io/ic-token

https://github.com/Psychedelic/DIP20

Psychedelic が作った
Psychedelic は Plug Wallet を開発していた
現在は [luke.icp](https://twitter.com/luke_icp)が [ICP 版クラウドファンディングの funded](https://funded.app/)で調達をして開発を続けている
Psychedelic は消えてしまった

principal id だけで管理している
address を使っていない

### DIP721

https://github.com/Psychedelic/DIP721

## DRC

### DRC20

https://github.com/iclighthouse/DRC_standards

ckETH の実装

## EXT Standard

ICP上でのNFTのスタンダード

https://github.com/Toniq-Labs/extendable-token

## ckETH

https://github.com/iclighthouse/icETH

テスト用トークンの可能性あり

### Phase1

HTTP Out Call を使って価格をオラクルから取得する

### Phase2

コミュニティ主導でやっていくが Dfinity もサポートする

キャニスターに EVM を乗せるのは Dfinity が主体でやっていく

ETH との完全統合を目指す
