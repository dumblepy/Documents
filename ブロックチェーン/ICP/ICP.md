ICP
===

[Rust版CDK](https://github.com/dfinity/cdk-rs)  
[Python版CDK](https://github.com/demergent-labs/kybra)  
[C版CDK](https://github.com/ICPorts-labs/chico)  

---

An Introduction to Dfinity and the Internet Computer  
https://messari.io/report/an-introduction-to-dfinity-and-the-internet-computer

---

## グラント
- ライブラリ
  - 
- コミュニティ拡大のイベント
  - Hokosugi
- 既にあるサービスのための資金調達
  - sloth

## ノード要件
[Node Operator Requirements](https://internetcomputer.academy/nodes/node-operator-requirements/)

[Wiki Node Provider Onboarding](https://wiki.internetcomputer.org/wiki/Node_Provider_Onboarding)

[How To Become An Internet Computer Node Operator: Beginner's Guide](https://www.coinhustle.com/how-to-become-node-operators/)

[Node Machine Hardware](https://wiki.internetcomputer.org/wiki/Node_Machine_Hardware)

Gen2 Node Machineの一般的な構成
|部位|パーツ|個数|単価|
|---|---|---|---|
|CPU|Dual Socket AMD EPYC 7313 Milan 16C/32T 3 Ghz, 32K/512K/128M optionally 7343, 7373, 73F3|2|242,647円|[Amazon](https://www.amazon.co.jp/AMD-100-000000342-Epyc-7443P-%E3%83%88%E3%83%AC%E3%82%A4/dp/B09GRL6NN1/ref=sr_1_1?__mk_ja_JP=%E3%82%AB%E3%82%BF%E3%82%AB%E3%83%8A&keywords=dual+socket+and+epyc7313&s=computers&sr=1-1)|
|メモリ|32GB RDIMM, 3200MT/s, Dual Rank|16|20,980円|[Amazon](https://www.amazon.co.jp/3200Mhz-PC4-25600-32GBx2%E6%9E%9A%EF%BC%8864GBkit%EF%BC%89%E3%83%87%E3%82%B9%E3%82%AF%E3%83%88%E3%83%83%E3%83%97%E7%94%A8-%E6%97%A5%E6%9C%AC%E5%9B%BD%E5%86%85%E7%84%A1%E6%9C%9F%E9%99%90%E4%BF%9D%E8%A8%BC-%E6%B0%B8%E4%B9%85%E4%BF%9D%E8%A8%BC%EF%BC%89%E6%AD%A3%E8%A6%8F%E5%93%81/dp/B084RDPTPZ/ref=sr_1_1_sspa?__mk_ja_JP=%E3%82%AB%E3%82%BF%E3%82%AB%E3%83%8A&keywords=32gb%2Bdimm&rnid=2127209051&s=computers&sr=1-1-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFLNk82TkRTN1M2VVEmZW5jcnlwdGVkSWQ9QTA0Mjg0MjIyTlczVDlaUllWUjY4JmVuY3J5cHRlZEFkSWQ9QTEwTjhJRFNSNURTMEcmd2lkZ2V0TmFtZT1zcF9hdGYmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl&th=1)|
|ストレージ|6.4TB NVMe Mixed Mode (DWPD >= 3)|5||
|ネットワークアダプタ|Dual Port 10G SFP or BASE-T|2||
|暗号化用アルゴリズムエンジン|TPM 2.0|1||

---

## Justin Bonsの批判とその回答
https://twitter.com/Justin_Bons/status/1628814692180283393

- トークノミクス
  - 70%が大口投資家が保有
  - 個人投資家はロックしてたのにVC、機関投資家が数十億ドルを売ったから1ヶ月で3000億以上の暴落
  - トレジャリーの20%が3日間で売却
  - FTXでの釣り上げられた価格
- 無限のスケーラビリティの嘘
  - DOTとAVAXと似てるけどそれらは「無限のスケーラビリティ」とは言ってない
  - ノードが増えるとブロック合意のメッセージングに時間がかかるから速度は落ちる
- ネイティブBTCの嘘
  - キャニスターと同じ安全性であってBTCネイティブではない
  - ckBTCのセキュリティはICPのセキュリティ同等であってBTCのセキュリティ同等ではない
- バリデータがステーキングに参加しない
  - バリデータはノードに投票する
  - ノードを信頼する必要がある
- オラクル不要の嘘
  - 中央集権型APIに依存することは、オラクル問題の解決策ではない

回答  
https://twitter.com/lastmjs/status/1629108662101766145

- トークノミクス
  - 現在では当初より小さな個人投資家たちによって多くのトークンが保有され分散化している
  - 



---

Internet Computerの概要 ～クラウドコンピューティング基盤を目指すブロックチェーン～ 日本総研
===
https://www.jri.co.jp/MediaLibrary/file/column/opinion/pdf/13450.pdf

---

ビットコインと連携予定である「ICP」について調べてみた【期待です】
===
マナブ  
Oct 4

こんにちは、マナブです。  
本日は、下記のニュースを取り上げます。

> Manabu | Crypto Investor  
> @manabubannai  
> ICP、ビットコインへの接続予定を発表
> 「ICPのスマートコントラクトは、BTCの流動性にアクセスできるようになります。そしてBTCは、面倒なブリッジサービスを必要とせずに、新しいスマートコントラクト機能を獲得します」

https://neweconomy.jp/posts/152578  
これは要チェックですね。市場が盛り上がりそう

> DFINITY財団「Internet Computer（ICP）」、ビットコインへの接続予定を発表 | あたらしい経済  
> ICP、ビットコインへの接続予定を発表 ディフィニティ財団（DFINITY Foundation）が、分散型クラウドコンピューティングプラットフォームであるインターネット・コンピューター（Internet Computer）をビットコインに接続することを9月21日に発表した。 リリースによればビットコインネットワーク接続の提案に関するコミュニティ投票がDFINITY財団内で行われ、9月17日に96  

上記を深堀りしつつ、勉強していきます。  
知識ゼロでも分かるように、順番に解説します。
　　
✓メルマガを読むメリットとは  
ICP、スマートコントラクト、DeFi市場の理解が深まります  
場合によっては「投資アイデアの発見」にも繋がるかもです

メリットは上記のとおりです。  
それでは、本日も学習していきましょう(｀･ω･´)ゞ
✓メルマガの目次  
１．ICPとは【名称：インターネット・コンピューター】  
２．ICP上にあるアプリを使ってみた【完成度も高い】  
３．ICPが「ビットコイン」に接続する件【バズの起点】  
４．ICPに投資すべきなのか【ネガティブニュースあり】  

※ちょいお知らせ：メルマガにて「質問コーナー」を始めてみます。初心者質問でもOKなので、気になることを投稿してみてください。投稿方法は「メルマガの最後」に記載します。
　　

## ■１．ICPとは【名称：インターネット・コンピューター】
結論は「ブロックチェーン・シンギュラリティ」を目指す組織です。
　　
### ブロックチェーン・シンギュラリティとは
横文字が多く、よく分からないですよね。  
要するに「ブロックチェーン技術を使いつつ、あらゆるシステムやサービスを”再構築”したい」といった感じです。  
うーむ、これでもよく分からんですね。 なので大雑把に説明すると、ブロックチェーン技術を使い、世の中を「更に最適化していきたい」といった感じです。
　　
### 公式サイトから、ビジョンを引用する
下記のとおりに引用しつつ、翻訳もします。

The DFINITY Foundation is a not-for-profit organization developing technology for the Internet Computer blockchain.   
（DFINITY協会は、ICP技術を開発している非営利団体です。）  
We are headquartered in Zürich, Switzerland, and have major research centers in Switzerland and California, and remote teams in places such as Germany, the UK and US.   
（スイスに本部があり、カリフォルニアには研究センターがあります。またドイツ、イギリス、アメリカ等にリモートチームが存在します。）  
We aim to create a "blockchain singularity" in which every system and service is rebuilt and reimagined using smart contracts and runs entirely from infinite public blockchain without need for traditional IT.  
（私達は、ブロックチェーンの”シンギュラリティ”を目指しています。世界観としては、あらゆるシステムやサービスが”スマートコントラクト”で最適化される状態です。）  
引用：DFINITY Foundation | Internet Computer

上記のとおり。  
かなり壮大なビジョンなので、難易度が高いと思います。
　　
### どういった課題を解決するのか
課題解決の部分に関して、もう少し深堀りします。

**✓キーワード：ネットインフラの民主化**
創業者のインタビューを読む限りだと、インターネットインフラの「民主化」を目指しています。どうゆうことかと言うと、例えば次のような状態です。

検索インフラ：Googleが、ほぼ独占している  
SNSインフラ：Facebook、インスタ、Twitterなどが独占  
サーバー：AmazonのAWSが、ほぼ独占している

上記のとおりで、要するに「インフラの独占」です。  
シリコンバレーでは、新しい企業を作りつつ、最終的には「Facebookに売却する」という目標を持つ人が増えています。  
要するにFacebookなどが市場独占をしており、資金も潤沢なので、皆さん売却を狙うわけですね。  
しかしこれって、本当に正しいことですか？  
もっとオープンな競争があり、多様化する世界もありだと思います。

✓大手の独占は、デメリットもある  
ここまで読んだ方は「ふむふむ、、たしかにそうだけど、、でもGoogleは便利だし、Amazonも便利だし、、別に良くないか？」と思うかもです。  
しかしインフラが独占されることで、分かりやすいデメリットがあります。  
それが「AWSサーバーが故障すると、他サービスに波及する」ということ。  
AWSじゃなくでも同じですが、皆さんも経験済みのはず。  
Facebookが落ちてメッセージが送れなくなったり、AWSサーバーの障害により複数サービスが動かなくなったり、Slackが落ちて仕事が進まくなったり、、など。  
こういった「インフラの独占」という問題を、ICPのプロジェクトは解決しようと動いている訳ですね。 素晴らしいビジョンですが、敵は強いです。ある意味で、巨大テクノロジー企業が対戦相手になりますよね。  
というわけで、以上がプロジェクト概要です。  
いったん次の項目では、時価総額などのデータをまとめます。

### ICPに関する各種データ（※2021年10月4日現在）
下記のとおりで、箇条書きします。
　　
ランキング：23位  
価格：5,392円  
時価総額：8,980億円  
最大供給：規定なし

上記は「CoinMarketCap」からのデータです。

✓時価総額の補足  
時価総額が「8,980億円」と書かれても、イメージが沸かないですよね。  
なので、他のコインや企業の時価総額も、参考までに記載します。  

ビットコイン ：100兆円  
イーサリアム ：43兆円  
トヨタ自動車 ：31兆円  
ソフトバンク ：10兆円  
三菱UFJ銀行 ：8.5兆円  
バイナンスコイン ：7兆円  
サイバーエージェント ：１兆円  
ヤクルト ：9,750億円

上記のとおり。  
要するに「ICPは、ヤクルトくらい」ですかね笑。

### 補足：資金調達に関するデータ
資金調達のデータも重要です。  
調達額が大きいなら、それだけ期待されてる証拠ですからね。

・2018年2月7日：6100万ドル（調達元：アンドリーセン、ポリチェーン）  
・2018年8月28日：1億200万ドル（調達元：アンドリーセン、ポリチェーン）

※上記データは「CrunchBase」から引用をしています。  
なお「アンドリーセン＝アンドリーセン・ホロウィッツ」で、世界的に有名な投ファンドです。仮想通貨の領域では、たぶん最も有名なはず。 そして「ポリチェーン＝ポリチェーンキャピタル」です。こちらも超有名。  
参考までに、他の有名プロジェクトの調達額も記載します。

✓OpenSea（引用元：CrunchBase）  
2019年11月19日：210万ドル（調達元：gumi）  
2021年3月18日：2300万ドル（調達元：アンドリーセン）  
2021年7月20日：1億ドル（調達元：アンドリーセン）  
✓Compound（引用元：CrunchBase）  
2018年5月16日：820万ドル（調達元：アンドリーセン、ベイン、ポリチェーン）  
2019年11月14日：2,500万ドル（調達元：アンドリーセン）  
✓Airbnb（引用元：CrunchBase）  
2011年7月25日：1億1,200万ドル（調達元：アンドリーセン）

上記のとおり。  
ICPの調達額は、わりと「大きめ」であることが分かります。  

## ■２．ICP上にあるアプリを使ってみた【完成度も高い】
2021年6月15日に、ICPの開発環境が公開されました。  
つまり誰でも、ICPのチェーン上にアプリを作れます。

### TikTokのクローンアプリがバズった
下記の記事が出たときに、多少のバズが発生しました。

※引用元：[Dfinity demonstrates its TikTok clone, opens up its ‘Internet Computer’ to outside developers](https://techcrunch.com/2020/06/30/dfinity-demos-its-tiktok-clone-opens-up-its-internet-computer-to-outside-developers/)

詳しくは上記も見て欲しいですが、ICPチェーンの上に「TikTokのクローンアプリ」が開発され、そして公開されました。  
TikTokは「約1,500万行」のコードで動いているみたいですが、これをICPで開発っすると、なんと「1,000行以下のコード」で作れるみたいです。

### ICP上のアプリは、続々と増えています
2021年6月15日に開発環境が公開されたばかりにも関わらず、現時点（2021年10月）で複数のアプリが、ICP上に構築されています。

Entrepot：NFTマーケットプレイス  
Uniswap：Uniswapのクローンサイト  
HEXGL：レーシングゲーム  
METAVERSE：メタバースのゲーム  
ICME：Webサイトビルダー

気になる方は触ってみてください。僕は全て触ってみたのですが、普通にサクサク動くと思います。  
かつ「Uniswapのクローンサイト」とかは、ぶっちゃけ、ほとんどコードを書かなくても作れたりします。なのでICPが伸びてくれば、DeFi系のプロジェクトも移動してきそうです。

### 余談：ICPunksを買ってみた件
ここはネタという感じですが、、参考までに。


> Manabu | Crypto Investor  
> @manabubannai  
> 新しく「NFT」を買いました  
> Internet Computerというブロックチェーン上にあるNFTで、とりあえず記念に購入です。ICPプロジェクトは有望だと思ったので、リサーチしつつ、買いつつ、検証しています。１枚目も２枚目も、たしか５万円くらいでした  
> #icp #icppunks #cronics

> September 29th 2021  
> 5 Retweets227 Likes

上記のとおりで、いくつかNFTを買いました。  
ぶっちゃけデザインは微妙ですが、もし仮に「ICPが世界を変える」といったプロジェクトになった場合は、大きな価値が生まれそうです。  
僕はCryptoPunksを買いましたが、これと同じ理由です。  
イーサリアムは、間違いなく世界を変えました。なのでイーサリアム上にあった「初期NFT」として、CryptoPunksの価格が上がった訳ですよね。  
というわけで、ICPのパンクスは、果たしてどうなるか。

## ■３．ICPが「ビットコイン」に接続する件【バズの起点】
ここからの話題は、すこし面白くなるはず。  
メルマガ冒頭に掲載したニュースを、再度貼ります。

> Manabu | Crypto Investor  
> @manabubannai  
> ICP、ビットコインへの接続予定を発表  
> 「ICPのスマートコントラクトは、BTCの流動性にアクセスできるようになります。そしてBTCは、面倒なブリッジサービスを必要とせずに、新しいスマートコントラクト機能を獲得します」

https://neweconomy.jp/posts/152578  
これは要チェックですね。市場が盛り上がりそう

> DFINITY財団「Internet Computer（ICP）」、ビットコインへの接続予定を発表 | あたらしい経済  
> ICP、ビットコインへの接続予定を発表 ディフィニティ財団（DFINITY Foundation）が、分散型クラウドコンピューティングプラットフォームであるインターネット・コンピューター（Internet Computer）をビットコインに接続することを9月21日に発表した。 リリースによればビットコインネットワーク接続の提案に関するコミュニティ投票がDFINITY財団内で行われ、9月17日に96  
> September 23rd 2021  
> 8 Retweets125 Likes

メルマガをここまで読んだ上で「上記のニュース」を読むと、かなり理解しやすいはずです。国民の99%は理解できないはずなので、優位性ですね。  
とはいえ日本語ニュースだと情報が少ないので、公式を見てみます。

### なぜ、ビットコインに接続するのか？
公式ブログから引用しつつ、翻訳します。

Bitcoin is a payment network without support for smart contracts. The Internet Computer will provide smart contracts that can natively hold, send, and receive Bitcoin (without the need for bridges or private keys) through an application of Chain Key cryptography, which will directly integrate the networks.  
（ビットコインには、スマートコントラクト機能がありません。しかしICPを使うことで、ビットコインを直接操作して、保有や送金が可能になります。）  
The scope of this feature is the integration of the Internet Computer with the Bitcoin blockchain with the following goals: (1) Making powerful smart contract functionality available for Bitcoin transactions; (2) enabling Bitcoin transactions with fast finality and low transaction cost.  
ビットコインへの接続では、次の２つの目標があります。１つ目は「協力なスマートコントラクト機能を、ビットコインに適用する」です。２つ目は「高速かつ、安い手数料で、ビットコイン取引を可能にする」です。  
引用：[Motion Proposal to Integrate Bitcoin With the Internet Computer Adopted by Community](https://medium.com/dfinity/motion-proposal-to-integrate-bitcoin-with-the-internet-computer-is-approved-95b00e1dcc9c)

上記のとおりで、これは凄いですよね。  
もし上記が実現したら、間違いなく注目を浴びそうです。

### 具体的に、どういったメリットがあるのか？
とはいえですが、、、なぜビットコインと繋げるのか？  
具体的なメリットについて、もう少し公式から深堀りします。

An example class of applications that will become possible with this integration are decentralized finance (DeFi) applications,   
（例えばですが、DeFiのアプリケーションを改善できます。）  
which can currently be implemented only with wrapped bitcoin and additional trusted entities on blockchains such as Ethereum.   
（現状のDeFIでは、WBTC等が主流です。しかしICPがビットコインに接続することで、ビットコインを直接DeFiに流すことができます。）  
Moreover, bitcoin could be used for paying for any kind of services on the Internet Computer, opening up an endless number of application scenarios.  
（さらに言うと、ビットコインをインターネット上のあらゆるサービスの支払いに使えるようになるので、とても便利になります。）  
引用：[Motion Proposal to Integrate Bitcoin With the Internet Computer Adopted by Community](https://medium.com/dfinity/motion-proposal-to-integrate-bitcoin-with-the-internet-computer-is-approved-95b00e1dcc9c)

上記のとおり。  
最初の注目は「DeFiとの連携」ですね。  
現状のDeFiだと、WBTCといったコインしか使えません。  
しかしWBTCは中央集権型なので、リスクが高いとも言えます。  
ICPを通して直接ビットコインを動かせるなら、便利ですよね。  
しかしICPのバグもあり得るので、、果たしてどこまで普及するか謎です。  

✓メタマスクが不要になる未来  
ICPを使うことで、HTTP接続でコイン操作が出来るようになります。  
現状だとメタマスクを使い、ガチャガチャといじらないといけないですよね。  
しかしICPを使うことで、まるで「ネットサーフィンをしている感覚」にて、DeFiなどのアプリケーションを使えます。  
ブラウザでビットコインのウォレットを読み込み、指紋認証にて送金するようなイメージです。ここは創業者が動画で解説しています。

https://www.youtube.com/watch?v=TtVo3krjARI

上記の動画をみるか、もしくは「ICPチェーン上のアプリ」を使うことで、実際に体験できます。 使うと気付きますが、普通に便利ですね。

## ■４．ICPに投資すべきなのか【ネガティブニュースあり】
というわけで、最後に「投資」について触れておきます。

### □結論：わかりません
僕がメルマガに取り上げるプロジェクトは、個人的には注目しています。  
とはいえICPに関しては、、どうなんでしょう。判断は難しいです。  
まだまだ「道のり半ば」ですし、かつ開発環境が公開されたのは「2021年6月」です。この記事の執筆は「2021年10月」なので、最近の出来事です。

### □参考までに、３つの情報を記載する
投資判断において、すこし情報を掲載します。

その①：ビットコインとの接続  
その②：フロントエンドの分散  
その③：ヴィタリック氏の言及

上記のとおりで、サクッと順番に解説です。  
まず「その①」に関しては、これは説明不要ですね。ビットコインとの接続が完了したら、わりと大きめなニュースになりそうです。  
続いて「その②：フロントエンドの分散」ですが、ここも重要。最近のDeFiは「アメリカからの規制」が進みつつあります。下記のとおり。

次なる仮想通貨トレンドは「フロントエンドの分散」なのかな、、と思っています。  
アメリカの規制が強まってくると、DeFiアプリケーションへの風当たりが強くなる。すると「地下に潜るプロジェクト」と「規制に従うプロジェクト」に別れていく。変化はチャンスなので、着目中
 
https://thedefiant.io/1inch-geofences-usa/

> New 1inch Feature Seeks to Keep US Users Away - The Defiant  
> 1inch, a decentralized exchange aggregator, introduced a feature which pushes its users to verify whether they are US > residents.  
> thedefiant.io  
> October 2nd 2021  
> 6 Retweets130 Likes

要するに「1inchという有名なプロジェクトが、アメリカ市民だと使えなくなった」というニュースです。  
多くのDeFiプロジェクトはAWSなどで運用されているので、表向きは匿名でも、Amazon社は「運営者の情報」を持っているはずです。  
この情報を警察に渡されたら、DeFi運営者は逮捕リスクもあります。  
こういった規制から逃げるために、ICPが活用されるかもです。  
ICPは非営利団体で運営されており、オープンソースです。 ICP上のDeFiプロジェクトなら、完全匿名を維持できるはずです。僕は実際に「ICP上にWebサイト」を作ってみましたが、これは完全匿名で実現しています。  

ブロックチェーン上に「自分のWebサイト」を作ってみました  
こちら「ICP」のブロックチェーンでホスティングされており、１行をコードを書かずに実現しました。手数料だけかかっており、たしか2,000円くらい。使っているツールは「 @icme_app 」です。とりあえずの実験です

続いて、３つ目の情報として「ヴィタリック氏の言及」です。  
2019年のインタビューにて、次のように答えています。  

質問者「最も見込みのある競合は、誰だと思いますか？」  
ヴィタリック氏「DFINITYが技術的には競合かな」

上記のとおりで、ヴィタリック氏も技術力を認めています。  
下記動画の「7:53」から、該当箇所が見れます。
https://www.youtube.com/watch?t=473&v=Cy8Fa5T2gYE&feature=youtu.be
