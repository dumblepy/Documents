https://www.youtube.com/watch?v=QuYY0NYTeJk

## 環境構築
```yaml
version: '3'
services:
  cosmos:
    build: ./docker/cosmos
    tty: true
    volumes:
      - .:/application

  nodejs:
    build: ./docker/nodejs
    tty: true
    volumes:
      - .:/application
    ports:
      - 3000:3000
```

docker/cosmos/Dockerfile
```dockerfile
FROM golang:1.19.1-bullseye

RUN apt update --fix-missing && \
    apt upgrade -y
RUN apt install -y --fix-missing \
        gcc \
        xz-utils \
        ca-certificates \
        vim \
        wget \
        git
WORKDIR /application
```

docker/nodejs/Dockerfile
```dockerfile
FROM node:16.17.1-alpine3.15

RUN echo http://dl-cdn.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories
RUN apk update && \
    apk upgrade --no-cache && \
    apk add --no-cache \
        git \
        yarn
WORKDIR /application
```

## Goの環境にIgnite CLIをインストール
https://docs.ignite.com/

```sh
curl https://get.ignite.com/cli! | bash
```
