## 25日
9:30AM - 9:35AM　WebX主催者挨拶  
国際ステージ　B棟7階  
https://webx-asia.com/ja/speaker/makoto-aoki/

9:45AM - 9:50AM　基調講演：岸田文雄 内閣総理大臣  
国際ステージ　B棟7階  
https://webx-asia.com/ja/speaker/fumio-kishida/

10:00AM - 10:30AM　大企業におけるweb3マーケティングとウォレットソリューション  
日本ステージ　B棟7階  
https://webx-asia.com/ja/speaker/yuya-higuchi/

11:20AM - 11:35AM　安心して利用できるweb3ビジネスインフラを。　Japan Open Chain  
ピッチステージ Eホール地下2階  
https://webx-asia.com/ja/speaker/hidekazu-kondo/

11:30AM - 12:00PM　トップアナリスト３人が語るクリプト市場のゆくえ　仮想西  
日本ステージ　B棟7階  
https://webx-asia.com/ja/speaker/nishi-crypto/

12:15PM - 1:15PM　ビットコイン・Web3は世の中に必要か？　ゆーぞー、ひろゆき  
日本ステージ　B棟7階  
https://webx-asia.com/ja/speaker/hiroyuki-nishimura/

3:30PM - 3:45PM　Astar Network　発表　そーた  
日本ステージ　B棟7階  
https://webx-asia.com/ja/speaker/sota-watanabe/

4:30PM - 5:00PM　元ゴールドマン・サックス機関投資家が教える「プロの投資」  アミン・アズムデ
スポットライトステージ　B棟5階  
https://webx-asia.com/ja/speaker/amin-azmoudeh/

5:15PM - 5:45PM　日本のWeb3はココがズレている　岡部、shinさん  
日本ステージ　B棟7階  
https://webx-asia.com/ja/speaker/noritaka-okabe/



## 26日
9:15AM - 9:20AM　経済産業大臣 基調講演  
日本ステージ　B棟7階  
https://webx-asia.com/ja/speaker/yasutoshi-nishimura/

10:00AM - 10:30AM　日本でステーブルコイン解禁 暗号資産・金融業界はどう変わる？　斉藤さん、白石さん  
日本ステージ　B棟7階  
https://webx-asia.com/ja/speaker/tatsuya-saito/

11:15AM - 11:50AM　冬は感じない？レイヤー1プロジェクトの挑戦　ロイ  
スポットライトステージ　B棟5階  
https://webx-asia.com/ja/speaker/roi-hirata/

1:40PM - 1:55PM　Pitch: JPYC  
ピッチステージ　Eホール地下2階  
https://webx-asia.com/ja/speaker/noritaka-okabe/

2:45PM - 3:15PM　デジタル金融時代 最先端技術がもたらす影響 斉藤さん  
日本ステージ　B棟7階  
https://webx-asia.com/ja/speaker/tatsuya-saito/

26日15:00-「岡部VSJPYCアンチ」  
JPYCブース  
https://twitter.com/jcam_official/status/1683311214729003008

4:20PM - 4:35PM　Pitch: Slash
ピッチステージ　Eホール地下2階
https://webx-asia.com/ja/speaker/tokio-nakamoto/

5:00PM - 5:30PM　日本におけるコミュニタリアニズム的DAOの発生とその可能性　落合さん
スポットライトステージ　B棟5階
https://webx-asia.com/ja/speaker/shogo-ochiai/
