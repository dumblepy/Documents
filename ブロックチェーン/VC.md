https://docs.google.com/spreadsheets/d/195PAQA1Up9r_sak0bK49_ITtMW2I1zrQ4pYcPBVRB-Q/edit#gid=0

|Name|公式Twit|個人Twitter①|個人Twitter②|個人Twitter③|Memo|
|---|---|---|---|---|---|
|Arriba Studio|@arriba_studio|@takashisato|@mch_tamaya|@miyasaka2020|
|B Dash Ventures|@BDashVentures|@nishida||
|Coincheck Labs|@coincheck_labs|@yusuke_56|@kaiyuu_eth||
|double jump.ventures|@doublejumptokyo|@cryptoangler96|
|East Ventures|@EastVentures_JP|@evkaneko|
|Emoote	@emoote_|@yujikumagai_|@sys1221|
|Flickshot|@FlickshotCrypto|@fukkai_vc|@aki_vc_fs|
|Fventures|@Fventures_jp	@ryokado	@makiwophoto|
|Genesia Ventures|@yutodx|@kantakimura_|||genesiaventures.com|
|GMO Web3|||||https://web3.gmo/
|gumi Cryptos|@GumiCryptos|@hkunimitsu|
|Hyperithm|@hyperithm|@wonjunlloydlee|@abeairb|
|Incuba Alpha|@Incuba_Alpha|@Melody_Taira|@0xBFRuby|
|Incubate Fund|@IncubateFund|@kirina___|
|Infinity Ventures Crypto|@ivcryptofund|@whiplus|
|mint|@keysket||||https://t.co/8eio2p8oeV
|MZ Web3ファンド|@yousuck2020||||https://web3.mzfund.co.jp/
|Next Web Capital|@NextWebCapital|@9dai_5|@tolehico|@Sota_Web3|https://nextweb.capital/
|Skyland Ventures|@skylandvc|@kinoshitay|@TakumiOokoshi|@murakamyista|
|Tané|@tanelabs|@ikuma|
|Z Venture Capital|@lucheng_li||||https://zvc.vc/
|レアゾンホールディングス|@SatoshiKuriga||||https://t.co/zb9PkoQkAO
|電通ベンチャーズ|||||https://dentsu-v.com/